package com.xiaobaqi.xbqdemo.wxapi

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.LogUtils
import com.tencent.mm.opensdk.constants.ConstantsAPI
import com.tencent.mm.opensdk.modelbase.BaseReq
import com.tencent.mm.opensdk.modelbase.BaseResp
import com.tencent.mm.opensdk.modelpay.PayResp
import com.tencent.mm.opensdk.openapi.IWXAPI
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.xbq.xbqsdk.core.pay.WxPayResultEvent
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.net.constants.SysConfigEnum
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

@AndroidEntryPoint
class WXPayEntryActivity : AppCompatActivity(), IWXAPIEventHandler {
    companion object {
        private const val TAG = "WXPayEntryActivity"
    }

    @Inject
    lateinit var userCache: UserCache

    private lateinit var api: IWXAPI
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        api = WXAPIFactory.createWXAPI(this, userCache.getConfig(SysConfigEnum.WX_APPID))
        api.handleIntent(intent, this)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        api.handleIntent(intent, this)
    }

    override fun onReq(req: BaseReq) {}
    override fun onResp(resp: BaseResp) {
        Log.d(TAG, "onPayFinish, errCode = " + resp.errCode)
        if (resp.type == ConstantsAPI.COMMAND_PAY_BY_WX) {
            if (resp is PayResp) {
                val orderNo = resp.extData
                if (resp.errCode == 0) {
                    LogUtils.d("WXPayEntryActivity，onResp， 支付成功")
                    EventBus.getDefault().post(WxPayResultEvent(true, "", orderNo))
                } else if (resp.errCode == -1) {
                    toast("支付失败")
                    EventBus.getDefault().post(WxPayResultEvent(false, "支付失败", orderNo))
                } else if (resp.errCode == -2) {
                    toast("用户取消")
                    EventBus.getDefault().post(WxPayResultEvent(false, "用户取消", orderNo))
                } else {
                    toast("支付失败")
                    EventBus.getDefault().post(WxPayResultEvent(false, "支付失败", orderNo))
                }
            }
        }
        finish()
    }

    private fun toast(msg: String) {
        Toast.makeText(applicationContext, msg, LENGTH_SHORT).show()
    }

}