package com.xbq.xbqsdk

import android.app.Application
import android.os.Build
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import com.blankj.utilcode.util.LogUtils
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.scwang.smart.refresh.layout.SmartRefreshLayout
import com.xbq.xbqsdk.core.umeng.UMengUtils

/**
 * Author: liaohaiping
 * Time: 2021-10-27
 * Description:
 */
open class XbqApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        LogUtils.getConfig().setGlobalTag("lhp")
        //友盟初始化
        UMengUtils.preinit(this)
        closeFileUriExposure()
        setRefreshLayoutHeaderAndFooter()
    }
    private fun closeFileUriExposure() {
        try {
            //关闭FileUriExposure检测：
            val builder = VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {  //18--4.3
                builder.detectFileUriExposure()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    private fun setRefreshLayoutHeaderAndFooter() {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, layout ->
            ClassicsHeader(context)//.setTimeFormat(new DynamicTimeFormat("更新于 %s"));//指定为经典Header，默认是 贝塞尔雷达Header
        }

        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, layout -> //指定为经典Footer，默认是 BallPulseFooter
            ClassicsFooter(context)
        }
    }
}