package com.xbq.xbqsdk

import com.google.gson.Gson
import com.xbq.xbqsdk.net.officeeditor.OfficeEditorApi
import com.xbq.xbqsdk.net.*
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.vo.FeatureDescriber
import com.xbq.xbqsdk.net.mapmark.MapMarkApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.sql.Timestamp
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Author: liaohaiping
 * Time: 2022-03-08
 * Description:
 */
@Module
@InstallIn(SingletonComponent::class)
class XbqSdkModule {
    val DEFAULT_TIMEOUT: Long = 3000L


    fun createRetrofit(userCache: UserCache): Retrofit {
        val clientBuilder = OkHttpClient.Builder()
            .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
        clientBuilder.interceptors().clear()
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        clientBuilder.addInterceptor(AddHeaderInterceptor(userCache))
        clientBuilder.addInterceptor(logger)
        val gson = Gson()
        val newGson = gson.newBuilder()
            .registerTypeAdapter(Timestamp::class.java, TimestampTypeAdapter())
            .create()
        return Retrofit.Builder()
            .client(clientBuilder.build())
            .baseUrl(XbqSdk.getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create(newGson))
            .build()
    }


    @Singleton
    @Provides
    fun provideCommonApi(userCache: UserCache): CommonApi {
        return createRetrofit(userCache).create(CommonApi::class.java)
    }

    @Singleton
    @Provides
    fun provideMapMarkApi(userCache: UserCache): MapMarkApi {
        return createRetrofit(userCache).create(MapMarkApi::class.java)
    }

    @Singleton
    @Provides
    fun provideOfficeEditorApi(userCache: UserCache): OfficeEditorApi {
        return createRetrofit(userCache).create(OfficeEditorApi::class.java)
    }


    @Singleton
    @Provides
    fun provideUserRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository {
        return userRepositoryImpl
    }

    @Singleton
    @Provides
    fun provideUserCache(userCacheImpl: UserCacheImpl): UserCache {
        return userCacheImpl
    }

    @Singleton
    @Provides
    fun provideFeatureDescriber(): FeatureDescriber {
        return XbqSdk.getFeatureDescriber();
    }
}