package com.xbq.xbqsdk.net

import android.text.TextUtils
import android.util.Base64
import android.util.Log
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.SPUtils
import com.xbq.xbqsdk.net.base.ApiResponse
import com.xbq.xbqsdk.net.base.BaseDto
import com.xbq.xbqsdk.net.base.DataResponse
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.dto.DeleteUserBySelfDto
import com.xbq.xbqsdk.net.common.dto.RegisterUserDto
import com.xbq.xbqsdk.net.common.vo.LoginVO
import com.xbq.xbqsdk.util.TimeUtils
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import org.json.JSONException
import org.json.JSONObject
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.text.DateFormat
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Author: liaohaiping
 * Time: 2021-10-27
 * Description:
 */
@Singleton
class UserRepositoryImpl @Inject constructor(val commonApi: CommonApi, val userCache: UserCache) : UserRepository {


    override suspend fun login(userName: String, password: String): DataResponse<LoginVO?> {
        val res = commonApi.login(RegisterUserDto(userName, password))
        if (res.success()) {
            userCache.saveLoginData(res.data!!)
            userCache.markLogined()
            userCache.setUserPassword(userName,password)
        }
        return res
    }

    override suspend fun register(userName: String, password: String): ApiResponse {
        return commonApi.register(RegisterUserDto(userName, password))
    }

    override suspend fun registerAndLogin(
        userName: String,
        password: String
    ): DataResponse<LoginVO?> {
        val res = commonApi.register(RegisterUserDto(userName, password))
        if (res.success()) {
            return login(userName, password)
        }
        return DataResponse.fail(res.code, res.message)
    }

    override suspend fun deleteSelfAccount(password: String): ApiResponse {
        var res = commonApi.deleteUserBySelf(DeleteUserBySelfDto(password))
        if (res.success()) {
            userCache.exitLogin()
        }
        return res
    }

    override suspend fun configs(): DataResponse<Map<String, String>?> {
        val res = commonApi.configs(BaseDto())
        if(res.success()){
            userCache.saveConfigs(res.data!!)
        }
        return res
    }


}