package com.xbq.xbqsdk.net

import android.text.TextUtils
import android.util.Base64
import android.util.Log
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.SPUtils
import com.xbq.xbqsdk.net.common.vo.LoginVO
import com.xbq.xbqsdk.net.common.vo.UserFeatureVO
import com.xbq.xbqsdk.net.constants.SysConfigEnum
import com.xbq.xbqsdk.util.TimeUtils
import com.xbq.xbqsdk.util.ext.genericType
import org.json.JSONException
import org.json.JSONObject
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.text.DateFormat
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Author: liaohaiping
 * Time: 2021-10-27
 * Description:
 */
@Singleton
class UserCacheImpl @Inject constructor() : UserCache {
    val isLogined: AtomicBoolean = AtomicBoolean(false)
    val SP_NAME = "user_data"
    val KEY_LOGIN_DATA = "LOGIN_DATA"
    val KEY_USER_FEATURE = "USER_FEATURE"
    val KEY_CONFIGS = "CONFIGS"
    val KEY_REMEMBER_USER_NAME = "remember_username"
    val KEY_REMEMBER_PASSWORD = "remember_password"

    override fun getConfigs(): Map<String, String?> {
        var str = SPUtils.getInstance(SP_NAME).getString(KEY_CONFIGS, null)
        if (str != null) {
            val configs =
                GsonUtils.fromJson<Map<String, String?>>(str, genericType<Map<String, String?>>())
            return configs
        }
        return emptyMap()
    }

    override fun getConfig(configName: String, defaultValue: String): String {
        var str = SPUtils.getInstance(SP_NAME).getString(KEY_CONFIGS, null)
        if (str != null) {
            val configs =
                GsonUtils.fromJson<Map<String, String?>>(str, genericType<Map<String, String?>>())
            if (configs != null) {
                if (configs.containsKey(configName)) {
                    return configs.get(configName) ?: defaultValue
                }
            }
        }
        return defaultValue
    }

    override fun getConfig(configName: String): String {
        return getConfig(configName, "")
    }

    override fun getConfig(sysConfigEnum: SysConfigEnum): String {
        return getConfig(sysConfigEnum.keyName, sysConfigEnum.value ?: "")
    }

    override fun getConfigBoolean(sysConfigEnum: SysConfigEnum): Boolean {
        return getConfig(sysConfigEnum).toBoolean()
    }

    override fun getConfigInt(sysConfigEnum: SysConfigEnum): Int {
        return getConfig(sysConfigEnum).toInt()
    }

    override fun saveConfigs(configs: Map<String, String>) {
        SPUtils.getInstance(SP_NAME).put(KEY_CONFIGS, GsonUtils.toJson(configs),true)
    }

    private fun clearConfigs() {
        SPUtils.getInstance(SP_NAME).remove(KEY_CONFIGS)
    }

    override fun getToken(): String {
        return getLoginData()?.token ?: ""
    }

    override fun getLoginData(): LoginVO? {
        return SPUtils.getInstance(SP_NAME)
            .getString(KEY_LOGIN_DATA, null)?.let {
                return@let GsonUtils.fromJson(it, LoginVO::class.java)
            }
    }

    override fun getUserFeatures(): List<UserFeatureVO>? {
        return SPUtils.getInstance(SP_NAME).getString(KEY_USER_FEATURE, null)?.let {
            return GsonUtils.fromJson(it, genericType<List<UserFeatureVO>>())
        }
    }

    override fun saveUserFeatures(userFeatures: List<UserFeatureVO>) {
        SPUtils.getInstance(SP_NAME).put(KEY_USER_FEATURE, GsonUtils.toJson(userFeatures),true)
    }

    override fun getUserName(): String {
        return getLoginData()?.userName ?: ""
    }

    override fun getUserId(): Long {
        return getLoginData()?.id ?: 0
    }

    override fun isLogin(): Boolean {
        return isLogined.get() && isTokenValid()
    }

    override fun canUse(feature: String): Boolean {
        return getUserFeatures()?.any { it.feature.equals(feature) && it.isValid }
            ?: false
    }

    override fun isFree(): Boolean {
        return !getConfigBoolean(SysConfigEnum.IS_CHARGE)
    }

    override fun isFreeOrCanUse(feature: String): Boolean {
        return isFree() or canUse(feature)
    }


    override fun exitLogin() {
        isLogined.set(false)
        SPUtils.getInstance(SP_NAME).clear()
    }

    override fun saveLoginData(data: LoginVO) {
        SPUtils.getInstance(SP_NAME).put(KEY_LOGIN_DATA, GsonUtils.toJson(data),true)
        saveConfigs(data.configs)
        saveUserFeatures(data.userFeatures)
    }

    override fun markLogined() {
        isLogined.set(true)
    }

    override fun setUserPassword(userName: String, password: String) {
        SPUtils.getInstance(SP_NAME).put(KEY_REMEMBER_USER_NAME, userName,true)
        SPUtils.getInstance(SP_NAME).put(KEY_REMEMBER_PASSWORD, password,true)
    }

    override fun getUserPassword(): UserPasswordBean? {
        SPUtils.getInstance(SP_NAME).let {
            val username = it.getString(KEY_REMEMBER_USER_NAME, "")
            val password = it.getString(KEY_REMEMBER_PASSWORD, "")
            if (username.isNotBlank() && password.isNotEmpty()) {
                return UserPasswordBean(username,password)
            }
        }
        return null
    }

    private fun isTokenValid(): Boolean {
        val token: String = getToken()
        if (TextUtils.isEmpty(token)) {
            return false
        }
        val arr = token.split(".").toTypedArray()
        if (arr.size != 3) {
            return false
        }
        val bytes = Base64.decode(arr[1], Base64.URL_SAFE)
        //        {"jti":"lhp","sub":"TEXT2VOICE","iss":"com.xbq.webapi","iat":1560650388,"exp":1560657588}
        val json = Charset.forName("utf-8").decode(ByteBuffer.wrap(bytes)).toString()
        return try {
            Log.d("lhp", "token payload: $json")
            val jsonObject = JSONObject(json)
            val expireTime =
                jsonObject.optLong("exp", TimeUtils.getTimeAfterNow(1, TimeUnit.DAYS).getTime())
            expireTime * 1000 > System.currentTimeMillis()
        } catch (e: JSONException) {
            e.printStackTrace()
            false
        }
    }

}