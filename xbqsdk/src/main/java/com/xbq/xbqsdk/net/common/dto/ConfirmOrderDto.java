package com.xbq.xbqsdk.net.common.dto;

import com.xbq.xbqsdk.net.base.BaseDto;
import com.xbq.xbqsdk.net.constants.PayTypeEnum;

import java.math.BigDecimal;

public class ConfirmOrderDto extends BaseDto {
    public String sku;
    public PayTypeEnum payType;  //支付类型
    public String contactPhone;
    public String payDesc; //有支付宝无微信
    public BigDecimal userPrice; //用户指定的金额
    //订单属性
    public String orderAttr;

    public ConfirmOrderDto(String sku, PayTypeEnum payType, String contactPhone, String payDesc, BigDecimal userPrice, String orderAttr) {
        this.sku = sku;
        this.payType = payType;
        this.contactPhone = contactPhone;
        this.payDesc = payDesc;
        this.userPrice = userPrice;
        this.orderAttr = orderAttr;
    }
}
