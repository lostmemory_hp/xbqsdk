package com.xbq.xbqsdk.net.officeeditor.dto;


import com.xbq.xbqsdk.net.base.BaseDto;

public class CollectionVideosDto extends BaseDto {
    public long collectionId;

    public CollectionVideosDto(long collectionId) {
        this.collectionId = collectionId;
    }
}
