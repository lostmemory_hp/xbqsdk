package com.xbq.xbqsdk.net

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.Keep

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
@Keep
class UserPasswordBean(val userName:String,val password:String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userName)
        parcel.writeString(password)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserPasswordBean> {
        override fun createFromParcel(parcel: Parcel): UserPasswordBean {
            return UserPasswordBean(parcel)
        }

        override fun newArray(size: Int): Array<UserPasswordBean?> {
            return arrayOfNulls(size)
        }
    }
}