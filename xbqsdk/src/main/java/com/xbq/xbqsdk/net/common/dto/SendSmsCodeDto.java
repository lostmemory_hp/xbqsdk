package com.xbq.xbqsdk.net.common.dto;

import com.xbq.xbqsdk.net.base.BaseDto;

public class SendSmsCodeDto extends BaseDto {
    public String phoneNumber;

    public SendSmsCodeDto(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
