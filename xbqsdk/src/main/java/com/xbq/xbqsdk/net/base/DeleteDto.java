package com.xbq.xbqsdk.net.base;

public class DeleteDto extends BaseDto {
  public long id;

  public DeleteDto(long id) {
    this.id = id;
  }
}
