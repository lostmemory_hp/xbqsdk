package com.xbq.xbqsdk.net.constants;

public enum SysConfigEnum {
  KEFU_QQ("kefuqq", "123456"),
  APP_DOWNLOAD_URL("app_download_url", ""),
  WORK_TIME("worktime","9:00-17:00"),
  IS_CHARGE("ischarge","true"),
  DISABLE_ALIPAY("disableAlipay","false"),
  WX_APPID("wxappId",""),
  VIP_DES("vipDes",""),
  NEED_SMS_VERIFICATION_CODE("need_sms_verification_code","false"),
  FREE_TRIALS("free_trials","0"),
  IS_CITY_FREE("is_city_free","false"),
  KEFU_PHONE("kefu_phone",""),
  ;

  private String keyName;
  private String value;

  SysConfigEnum(String keyName, String value) {
    this.keyName = keyName;
    this.value = value;
  }

  public String getKeyName() {
    return keyName;
  }

  public String getValue() {
    return value;
  }


  public int getValueInt() {
    return Integer.valueOf(getValue());
  }

  public float getValueFloat() {
    return Float.valueOf(getValue());
  }

  public boolean getValueBoolean() {
    return Boolean.valueOf(getValue());
  }

}