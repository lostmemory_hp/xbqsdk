package com.xbq.xbqsdk.net.common.dto;

import androidx.annotation.NonNull;

import com.xbq.xbqsdk.net.base.BaseDto;

/**
 * @Author: liaohaiping
 * @Description:
 * @Date: Created in 2019/6/11 0011 18:09
 */
public class ProductListDto extends BaseDto {
//    public FeatureEnum feature;
//    public ProductListDto( FeatureEnum feature) {
//        this.feature = feature;
//    }
    @NonNull
    public String feature;

    public ProductListDto(@NonNull String feature) {
        this.feature = feature;
    }
}
