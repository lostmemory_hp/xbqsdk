package com.xbq.xbqsdk.net.mapmark.constants;

/**
 * @Author: liaohaiping
 * @Description: 时间长度单位
 * @Date: Created in 2019/3/18 0018 15:57
 */
public enum MarkOrderStatusEnum {
  PENDING("待支付"),
  PAID("已支付"),
  REFUNDED("已退款"),
  CLOSED("交易已关闭(未支付)"),
  DATA_UPLOADED("资料已上传,等待平台审核"),
  AUDIT_PASS("资料审核通过，标注已完成"),
  AUDIT_FAILED("平台审核未通过，请重新提供资料")
  ;

  private String desc;

  MarkOrderStatusEnum(String desc) {
    this.desc = desc;
  }


  public String getDesc() {
    return desc;
  }

  public MarkOrderStatusEnum setDesc(String desc) {
    this.desc = desc;
    return this;
  }
}
