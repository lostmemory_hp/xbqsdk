package com.xbq.xbqsdk.net.base;

import androidx.annotation.Keep;

@Keep
public class ApiResponse {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean success() {
        return code == 0;
    }

    /**
     * token已过期
     * @return
     */
    public boolean tokenExpired() {
        return code == 900;
    }
    /**
     * 账号在其他地方登录
     * @return
     */
    public boolean loginInOtherPlace() {
        return code == 901;
    }

    public static ApiResponse ok(){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setCode(0);
        return apiResponse;
    }
    public static ApiResponse fail(String message){
        return fail(505,message);
    }
    public static ApiResponse fail(int code, String message){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setCode(code);
        apiResponse.setMessage(message);
        return apiResponse;
    }


    @Override
    public String toString() {
        return "ApiResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}