package com.xbq.xbqsdk.net.common.dto;


import com.xbq.xbqsdk.net.base.BaseDto;

public class ChangePasswordDto extends BaseDto {
    public String password;
    public String newPassword;

    public ChangePasswordDto(String password, String newPassword) {
        this.password = password;
        this.newPassword = newPassword;
    }
}
