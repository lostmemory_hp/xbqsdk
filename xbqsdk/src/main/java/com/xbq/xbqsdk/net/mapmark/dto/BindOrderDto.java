
package com.xbq.xbqsdk.net.mapmark.dto;

import com.xbq.xbqsdk.net.base.BaseDto;

public class BindOrderDto extends BaseDto {
    private long markOrderId;
    private String orderNo;

    public BindOrderDto(long markOrderId, String orderNo) {
        this.markOrderId = markOrderId;
        this.orderNo = orderNo;
    }

    public long getMarkOrderId() {
        return markOrderId;
    }

    public void setMarkOrderId(long markOrderId) {
        this.markOrderId = markOrderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
