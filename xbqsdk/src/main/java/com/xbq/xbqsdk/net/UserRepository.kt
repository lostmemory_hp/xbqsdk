package com.xbq.xbqsdk.net

import com.xbq.xbqsdk.net.base.ApiResponse
import com.xbq.xbqsdk.net.base.DataResponse
import com.xbq.xbqsdk.net.common.vo.LoginVO

/**
 * Author: liaohaiping
 * Time: 2021-10-27
 * Description:
 */
interface UserRepository {
    suspend fun login(userName: String, password: String): DataResponse<LoginVO?>
    suspend fun register(userName: String, password: String): ApiResponse
    suspend fun registerAndLogin(userName: String, password: String): DataResponse<LoginVO?>
    /**
     * 注销帐号
     */
    suspend fun deleteSelfAccount(password: String):ApiResponse

    suspend fun configs(): DataResponse<Map<String, String>?>
}