package com.xbq.xbqsdk.net.common.vo;

import android.util.Log;

import com.xbq.xbqsdk.util.collection.Linq;

import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class LoginVO {
    private long id;
    private String token;
    private String userName;
    private String userId;
    private Timestamp createTime;
    private int goldCoin;
    private List<UserFeatureVO> userFeatures;
    private Map<String, String> configs;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getGoldCoin() {
        return goldCoin;
    }

    public LoginVO setGoldCoin(int goldCoin) {
        this.goldCoin = goldCoin;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<UserFeatureVO> getUserFeatures() {
        return userFeatures;
    }

    public void setUserFeatures(List<UserFeatureVO> userFeatures) {
        this.userFeatures = userFeatures;
    }

    public Map<String, String> getConfigs() {
        return configs;
    }

    public void setConfigs(Map<String, String> configs) {
        this.configs = configs;
    }


    public String getConfig(String configName, String defaultValue) {
        if (configs.containsKey(configName)) {
            return configs.get(configName);
        }
        return defaultValue;
    }

    public int getConfigInt(String configName, int defaultValue) {
        try {
            return Integer.valueOf(getConfig(configName, String.valueOf(defaultValue)));
        } catch (Exception e) {
            Log.e("lhp", e.getMessage(), e);
            return defaultValue;
        }
    }

    public long getConfigLong(String configName, long defaultValue) {
        try {
            return Long.valueOf(getConfig(configName, String.valueOf(defaultValue)));
        } catch (Exception e) {
            Log.e("lhp", e.getMessage(), e);
            return defaultValue;
        }
    }

    public double getConfigDouble(String configName, double defaultValue) {
        try {
            return Double.valueOf(getConfig(configName, String.valueOf(defaultValue)));
        } catch (Exception e) {
            Log.e("lhp", e.getMessage(), e);
            return defaultValue;
        }
    }

    public boolean getConfigBoolean(String configName, boolean defaultValue) {
        try {
            return Boolean.valueOf(getConfig(configName, String.valueOf(defaultValue)));
        } catch (Exception e) {
            Log.e("lhp", e.getMessage(), e);
            return defaultValue;
        }
    }

    public String formatFeatures(@NotNull FeatureDescriber featureDescriber) {
        if (userFeatures != null && userFeatures.size() > 0) {
            if(userFeatures.size()==1){
                return userFeatures.get(0).formatSimpleFeature();
            }else {
                return Linq.of(userFeatures).stringJoin("\n", c -> c.formatFeature(featureDescriber));
            }
        }
        return "";
    }

    public boolean canUse(String feature){
        UserFeatureVO vo = Linq.of(userFeatures).first(f -> f.getFeature().equals(feature));
        if (vo != null && vo.isValid()) {
            return true;
        }
        return false;
    }
}
