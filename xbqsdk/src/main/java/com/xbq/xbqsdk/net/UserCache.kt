package com.xbq.xbqsdk.net

import com.xbq.xbqsdk.net.common.vo.LoginVO
import com.xbq.xbqsdk.net.common.vo.UserFeatureVO
import com.xbq.xbqsdk.net.constants.SysConfigEnum

/**
 * Author: liaohaiping
 * Time: 2021-11-04
 * Description:
 */
interface UserCache {
    fun getConfigs():Map<String,String?>
    fun getConfig(configName: String, defaultValue: String): String
    fun getConfig(configName: String): String
    fun getConfig(sysConfigEnum: SysConfigEnum): String
    fun getConfigBoolean(sysConfigEnum: SysConfigEnum): Boolean
    fun getConfigInt(sysConfigEnum: SysConfigEnum): Int
    fun saveConfigs(configs: Map<String, String>)
    fun getToken(): String
    fun getLoginData(): LoginVO?
    fun saveLoginData(loginVO: LoginVO)
    fun getUserFeatures(): List<UserFeatureVO>?
    fun saveUserFeatures(userFeatures: List<UserFeatureVO>)
    fun getUserName(): String
    fun getUserId(): Long
    fun isLogin(): Boolean
    fun canUse(feature: String): Boolean
    fun isFree(): Boolean
    fun isFreeOrCanUse(feature: String): Boolean
    fun exitLogin()
    fun markLogined() //标记已登录
    fun setUserPassword(userName:String, password:String)
    fun getUserPassword():UserPasswordBean?
}