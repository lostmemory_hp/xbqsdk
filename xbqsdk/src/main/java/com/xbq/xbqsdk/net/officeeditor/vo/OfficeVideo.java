package com.xbq.xbqsdk.net.officeeditor.vo;

import android.os.Parcel;
import android.os.Parcelable;

import com.xbq.xbqsdk.net.officeeditor.OfficeTypeEnum;


public final class OfficeVideo implements Parcelable {
    private long id;
    private String title;

    private String thumbnail;  //缩略图
    private String url;   //视频地址

    private int index;

    private long collectionId;

    private OfficeTypeEnum officeType;

    private boolean needVip;


    protected OfficeVideo(Parcel in) {
        id = in.readLong();
        title = in.readString();
        thumbnail = in.readString();
        url = in.readString();
        index = in.readInt();
        collectionId = in.readLong();
        needVip = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(thumbnail);
        dest.writeString(url);
        dest.writeInt(index);
        dest.writeLong(collectionId);
        dest.writeByte((byte) (needVip ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OfficeVideo> CREATOR = new Creator<OfficeVideo>() {
        @Override
        public OfficeVideo createFromParcel(Parcel in) {
            return new OfficeVideo(in);
        }

        @Override
        public OfficeVideo[] newArray(int size) {
            return new OfficeVideo[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(long collectionId) {
        this.collectionId = collectionId;
    }

    public OfficeTypeEnum getOfficeType() {
        return officeType;
    }

    public void setOfficeType(OfficeTypeEnum officeType) {
        this.officeType = officeType;
    }

    public boolean isNeedVip() {
        return needVip;
    }

    public void setNeedVip(boolean needVip) {
        this.needVip = needVip;
    }
}
