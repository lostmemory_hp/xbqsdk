package com.xbq.xbqsdk.net.officeeditor.dto;

import com.xbq.xbqsdk.net.base.BaseDto;
import com.xbq.xbqsdk.net.officeeditor.OfficeTypeEnum;

public class CollectionsByTypeDto extends BaseDto {
    public OfficeTypeEnum officeType;
    public String type;
    public int requestCount=10;  //返回数据量


    public CollectionsByTypeDto(OfficeTypeEnum officeType, String type, int requestCount) {
        this.officeType = officeType;
        this.type = type;
        this.requestCount = requestCount;
    }
}
