package com.xbq.xbqsdk.net.common.vo;

/**
 * Author: liaohaiping
 * Time: 2022-03-09
 * Description: feature
 */
public interface FeatureDescriber {
    default String getDescription(String feature){
        return feature;
    }
}
