package com.xbq.xbqsdk.net.officeeditor.vo;

import android.os.Parcel;
import android.os.Parcelable;

import com.xbq.xbqsdk.net.officeeditor.OfficeTypeEnum;


public final class OfficeVideoCollection implements Parcelable {
    private long id;
    private String title;
    private OfficeTypeEnum officeType;
    private String cover;  //封面
    private boolean published;

    protected OfficeVideoCollection(Parcel in) {
        id = in.readLong();
        title = in.readString();
        cover = in.readString();
        published = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(cover);
        dest.writeByte((byte) (published ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OfficeVideoCollection> CREATOR = new Creator<OfficeVideoCollection>() {
        @Override
        public OfficeVideoCollection createFromParcel(Parcel in) {
            return new OfficeVideoCollection(in);
        }

        @Override
        public OfficeVideoCollection[] newArray(int size) {
            return new OfficeVideoCollection[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public OfficeTypeEnum getOfficeType() {
        return officeType;
    }

    public void setOfficeType(OfficeTypeEnum officeType) {
        this.officeType = officeType;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }
}
