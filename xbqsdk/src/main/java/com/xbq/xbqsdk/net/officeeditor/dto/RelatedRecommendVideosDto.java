package com.xbq.xbqsdk.net.officeeditor.dto;


import com.xbq.xbqsdk.net.base.BaseDto;
import com.xbq.xbqsdk.net.officeeditor.OfficeTypeEnum;

public class RelatedRecommendVideosDto extends BaseDto {
    private OfficeTypeEnum officeType;
    private long videoId;
    private int videoCount=10; //返回的视频数量

    public RelatedRecommendVideosDto(OfficeTypeEnum officeType, long videoId, int videoCount) {
        this.officeType = officeType;
        this.videoId = videoId;
        this.videoCount = videoCount;
    }
}
