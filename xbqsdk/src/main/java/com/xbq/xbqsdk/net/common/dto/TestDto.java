package com.xbq.xbqsdk.net.common.dto;

import com.xbq.xbqsdk.net.base.BaseDto;

import java.util.List;

public class TestDto extends BaseDto {
    public String userName;
    public String password;
    public List<String> field_values;
    public List<List<String>> images;

    public TestDto() {
    }

    public TestDto(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
}