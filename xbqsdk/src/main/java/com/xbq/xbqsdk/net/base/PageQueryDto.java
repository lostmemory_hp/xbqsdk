package com.xbq.xbqsdk.net.base;

import androidx.annotation.Keep;

/**
 * Author: liaohaiping
 * Time: 2019-06-24
 * Description:
 */
@Keep
public class PageQueryDto extends BaseDto {
    public int pageIndex;
    public int pageSize;

    public PageQueryDto(int pageIndex, int pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }
}
