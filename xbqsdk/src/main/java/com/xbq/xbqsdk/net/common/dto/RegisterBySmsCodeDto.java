package com.xbq.xbqsdk.net.common.dto;

import com.xbq.xbqsdk.net.base.BaseDto;

public class RegisterBySmsCodeDto extends BaseDto {
    public String userName;
    public String phoneNumber;
    public String password;
    public String verificationCode;

    public RegisterBySmsCodeDto(String userName, String phoneNumber, String password, String verificationCode) {
        this.userName = userName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.verificationCode = verificationCode;
    }
}
