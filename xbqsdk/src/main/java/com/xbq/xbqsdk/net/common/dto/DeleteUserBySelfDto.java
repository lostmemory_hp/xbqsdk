package com.xbq.xbqsdk.net.common.dto;

import com.xbq.xbqsdk.net.base.BaseDto;

/**
 * @Author: liaohaiping
 * @Description:
 * @Date: Created in 2019/6/10 0010 17:56
 */
public class DeleteUserBySelfDto extends BaseDto {
  public String password;

  public DeleteUserBySelfDto(String password) {
    this.password = password;
  }
}
