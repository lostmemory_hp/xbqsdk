package com.xbq.xbqsdk.net.mapmark.vo;

import androidx.annotation.Keep;

import com.xbq.xbqsdk.net.mapmark.constants.MarkOrderStatusEnum;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Keep
public class MarkOrderVO  {
    private long id;
    private long uId;
    private String feature; //feature
    private String storeName;
    private String storeCategory;
    private String phone; //联系电话
    private String businessPhone; //营业电话
    private String businessHours; //营业时间
    private String address;  //商户详细地址
    private String houseNumber;  //街道门牌号
    private double longitude; //经度
    private double latitude;  //纬度
    private String maps; //标注哪些地图
    private String description;  //详细描述， 加速审核
    private String orderNo; //订单号
    private MarkOrderStatusEnum status;
    private Timestamp createTime;
    private Timestamp paytime;
    private Timestamp dealTime; //处理时间
    private Timestamp successTime; //处理时间

    private String productSku;
    private BigDecimal price;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreCategory() {
        return storeCategory;
    }

    public void setStoreCategory(String storeCategory) {
        this.storeCategory = storeCategory;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getMaps() {
        return maps;
    }

    public void setMaps(String maps) {
        this.maps = maps;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public MarkOrderStatusEnum getStatus() {
        return status;
    }

    public void setStatus(MarkOrderStatusEnum status) {
        this.status = status;
    }

    public Timestamp getPaytime() {
        return paytime;
    }

    public void setPaytime(Timestamp paytime) {
        this.paytime = paytime;
    }

    public Timestamp getDealTime() {
        return dealTime;
    }

    public void setDealTime(Timestamp dealTime) {
        this.dealTime = dealTime;
    }

    public Timestamp getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(Timestamp successTime) {
        this.successTime = successTime;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public MarkOrderVO setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getProductSku() {
        return productSku;
    }

    public MarkOrderVO setProductSku(String productSku) {
        this.productSku = productSku;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public MarkOrderVO setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }
}
