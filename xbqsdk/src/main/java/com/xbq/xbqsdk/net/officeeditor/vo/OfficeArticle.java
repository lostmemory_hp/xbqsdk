package com.xbq.xbqsdk.net.officeeditor.vo;

import com.xbq.xbqsdk.net.officeeditor.OfficeTypeEnum;

public final class OfficeArticle  {
    private long id;
    private String title;
    private String thumbnail;  //缩略图
    private String url;   //视频地址
    private String tagName;  //标签
    private OfficeTypeEnum officeType;
    private boolean needVip;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public OfficeTypeEnum getOfficeType() {
        return officeType;
    }

    public void setOfficeType(OfficeTypeEnum officeType) {
        this.officeType = officeType;
    }

    public boolean isNeedVip() {
        return needVip;
    }

    public void setNeedVip(boolean needVip) {
        this.needVip = needVip;
    }
}
