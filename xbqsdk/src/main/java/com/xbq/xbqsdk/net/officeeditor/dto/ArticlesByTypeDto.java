package com.xbq.xbqsdk.net.officeeditor.dto;

import com.xbq.xbqsdk.net.base.BaseDto;
import com.xbq.xbqsdk.net.officeeditor.OfficeTypeEnum;

public class ArticlesByTypeDto extends BaseDto {
    private OfficeTypeEnum officeType;
    private String type;
    private int articleCount=20; //返回视频数量

    public ArticlesByTypeDto(OfficeTypeEnum officeType, String type, int articleCount) {
        this.officeType = officeType;
        this.type = type;
        this.articleCount = articleCount;
    }
}
