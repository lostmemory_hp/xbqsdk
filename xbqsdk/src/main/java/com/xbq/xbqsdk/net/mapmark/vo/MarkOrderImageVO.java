package com.xbq.xbqsdk.net.mapmark.vo;

import androidx.annotation.Keep;

@Keep
public class MarkOrderImageVO {
    private long markOrderId;
    private long imageId;
    private String tag; // 图片标签

    public long getMarkOrderId() {
        return markOrderId;
    }

    public void setMarkOrderId(long markOrderId) {
        this.markOrderId = markOrderId;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
