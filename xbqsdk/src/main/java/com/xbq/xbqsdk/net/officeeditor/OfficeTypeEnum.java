package com.xbq.xbqsdk.net.officeeditor;

public enum OfficeTypeEnum {
  WORD("WORD"),
  EXCEL("EXCEL"),
  PPT("PPT"),
  ;

  private String desc;

  OfficeTypeEnum(String desc) {
    this.desc = desc;
  }


  public String getDesc() {
    return desc;
  }

  public OfficeTypeEnum setDesc(String desc) {
    this.desc = desc;
    return this;
  }
}
