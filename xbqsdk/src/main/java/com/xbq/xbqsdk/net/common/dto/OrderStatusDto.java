package com.xbq.xbqsdk.net.common.dto;


import com.xbq.xbqsdk.net.base.BaseDto;

public class OrderStatusDto extends BaseDto {
    public String orderNo;

    public OrderStatusDto(String orderNo) {
        this.orderNo = orderNo;
    }
}
