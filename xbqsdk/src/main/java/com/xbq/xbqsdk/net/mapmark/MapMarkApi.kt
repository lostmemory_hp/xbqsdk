package com.xbq.xbqsdk.net.mapmark

import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.net.base.*
import com.xbq.xbqsdk.net.mapmark.dto.BindOrderDto
import com.xbq.xbqsdk.net.mapmark.dto.RequestMapMarkDto
import com.xbq.xbqsdk.net.mapmark.vo.MarkOrderDetailVO
import com.xbq.xbqsdk.net.mapmark.vo.MarkOrderVO
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Author: liaohaiping
 * Time: 2021-12-10
 * Description:
 */


interface MapMarkApi {

    /**
     * 申请地图标注
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "mapmark/request_mapmark")
    suspend fun request_mapmark(@Body dto: RequestMapMarkDto): DataResponse<MarkOrderVO?>

    /**
     * 标注订单详情
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "mapmark/mark_order_detail")
    suspend fun mark_order_detail(@Body dto: DeleteDto): DataResponse<MarkOrderDetailVO?>

    /**
     * 我的订单列表
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "mapmark/my_mark_orders")
    suspend fun my_mark_orders(@Body dto: PageQueryDto): DataResponse<PagedList<MarkOrderVO>?>



    /**
     * 绑定支付订单
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "mapmark/bind_order")
    suspend fun bind_order(@Body dto: BindOrderDto): ApiResponse
}