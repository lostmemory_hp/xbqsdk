package com.xbq.xbqsdk.net

import com.xbq.xbqsdk.XbqSdk
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

/**
 * 网络请求拦截器
 * Created by cuiyan on 16/6/8 12:07.
 */
class AddHeaderInterceptor(val userCache: UserCache) : Interceptor {

    @Synchronized
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        var url = request.url.toString()
        if (url.contains(XbqSdk.API_PREFIX_PLACEHOLDER)) {
            url = url.replace(XbqSdk.API_PREFIX_PLACEHOLDER, XbqSdk.getApiPrefix());
        }
        val newRequest = request.newBuilder()
            .url(url)
            .addHeader("Authorization", "Bearer " + userCache.getToken())
            .build()
        return chain.proceed(newRequest)
    }
}