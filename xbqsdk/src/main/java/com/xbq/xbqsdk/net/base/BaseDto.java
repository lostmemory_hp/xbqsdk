package com.xbq.xbqsdk.net.base;

import android.os.Build;

import androidx.annotation.Keep;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.MetaDataUtils;
import com.xbq.xbqsdk.XbqSdk;


@Keep
public class BaseDto {
    public String agencyChannel = MetaDataUtils.getMetaDataInApp("AGENCY_CHANNEL"); //代理渠道
    public String appMarket = MetaDataUtils.getMetaDataInApp("UMENG_CHANNEL"); //应用市场
    public String appPackage = AppUtils.getAppPackageName();  //应用包名
    public String appName = AppUtils.getAppName(); //应用名称
    public String appVersion = AppUtils.getAppVersionName(); //应用版本
    public int appVersionCode = AppUtils.getAppVersionCode(); //应用版本号
    public String deviceName = Build.MODEL;  //设备名称
    public String deviceBrand = Build.BRAND; //品牌
    public String deviceManufacturer = Build.MANUFACTURER; //设备制造商
    public String devicePlatform = "ANDROID";   //设备平台
    public String application = XbqSdk.getApplication();
    public String innerVersion = XbqSdk.getInnerVersion();
}