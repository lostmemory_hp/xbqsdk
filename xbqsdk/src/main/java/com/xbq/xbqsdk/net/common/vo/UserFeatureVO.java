package com.xbq.xbqsdk.net.common.vo;



import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;


/**
 * 用户功能
 */
public class UserFeatureVO {
    private long id;
    private String application; //应用程序
    private String userName; //用户名
    private String feature; //功能
    private boolean limitAmount;
    private int amount;
    private boolean limitExpireTime;
    private Timestamp expireTime;
    private Timestamp createTime;  //创建时间


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public boolean isLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(boolean limitAmount) {
        this.limitAmount = limitAmount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isLimitExpireTime() {
        return limitExpireTime;
    }

    public void setLimitExpireTime(boolean limitExpireTime) {
        this.limitExpireTime = limitExpireTime;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public boolean isValid() {
        return (!limitAmount || amount > 0)
                && (!limitExpireTime || (expireTime != null && expireTime.getTime() > System
                .currentTimeMillis()));
    }


    public String formatFeature(@NotNull FeatureDescriber featureDescriber) {
        StringBuilder sb = new StringBuilder();
        sb.append(featureDescriber.getDescription(feature));
        if (limitAmount) {
            sb.append(",  剩余：" + amount);
        }
        if (limitExpireTime) {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if(expireTime.getTime()>System.currentTimeMillis()) {
                sb.append("[" + sdf.format(expireTime)+" 到期]");
            }else{
                sb.append("[已过期]");
            }
        }else{
            sb.append("[永久有效]");
        }
        return sb.toString();
    }

    public String formatSimpleFeature() {
        StringBuilder sb = new StringBuilder();
        if (limitAmount) {
            sb.append("剩余：" + amount+"");
        }
        if (limitExpireTime) {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if(expireTime.getTime()>System.currentTimeMillis()) {
                sb.append(sdf.format(expireTime)+" 到期");
            }else{
                sb.append("已过期");
            }
        }else{
            sb.append("永久有效");
        }
        return sb.toString();
    }




}
