package com.xbq.xbqsdk.net.common.dto;


import com.xbq.xbqsdk.net.base.BaseDto;

/**
 * @Author: liaohaiping
 * @Description:
 * @Date: Created in 2019/6/11 0011 18:09
 */
public class AddFeedbackDto extends BaseDto {
  public String title;
  public String content;
  public String contactPhone;


  public AddFeedbackDto(String title, String content, String contactPhone) {
    this.title = title;
    this.content = content;
    this.contactPhone = contactPhone;
  }
}
