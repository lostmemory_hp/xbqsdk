package com.xbq.xbqsdk.net.common.dto;

import com.xbq.xbqsdk.net.base.BaseDto;

public class IsCityFreeDto extends BaseDto {
  public String city;

  public IsCityFreeDto(String city) {
    this.city = city;
  }
}
