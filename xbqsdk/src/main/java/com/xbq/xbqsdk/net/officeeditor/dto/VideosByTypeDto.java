package com.xbq.xbqsdk.net.officeeditor.dto;

import com.xbq.xbqsdk.net.base.BaseDto;
import com.xbq.xbqsdk.net.officeeditor.OfficeTypeEnum;

public class VideosByTypeDto extends BaseDto {
    public OfficeTypeEnum officeType;
    public String type;
    public int videoCount=20; //返回视频数量

    public VideosByTypeDto(OfficeTypeEnum officeType, String type, int videoCount) {
        this.officeType = officeType;
        this.type = type;
        this.videoCount = videoCount;
    }
}
