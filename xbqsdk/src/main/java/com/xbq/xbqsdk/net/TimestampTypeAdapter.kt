package com.xbq.xbqsdk.net

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.sql.Timestamp

/**
 * Author: liaohaiping
 * Time: 2020-11-19
 * Description:
 */
class TimestampTypeAdapter: TypeAdapter<Timestamp>() {
    private val dateAdapter = com.google.gson.internal.bind.DateTypeAdapter()
    override fun write(out: JsonWriter?, value: Timestamp?) {
        out?.value(value?.time)
    }

    override fun read(`in`: JsonReader?): Timestamp? {
        val read = dateAdapter.read(`in`)
        if(read==null){
            return null
        }
        return Timestamp(read.time)
    }
}