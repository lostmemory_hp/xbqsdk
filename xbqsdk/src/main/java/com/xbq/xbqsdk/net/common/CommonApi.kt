package com.xbq.xbqsdk.net.common

import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.net.base.ApiResponse
import com.xbq.xbqsdk.net.base.BaseDto
import com.xbq.xbqsdk.net.base.DataResponse
import com.xbq.xbqsdk.net.common.dto.*
import com.xbq.xbqsdk.net.common.vo.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Author: liaohaiping
 * Time: 2020-11-09
 * Description:
 */
interface CommonApi {

    /**
     * 获取配置
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "config/configs")
    suspend fun configs(@Body dto: BaseDto): DataResponse<Map<String, String>?>

    /**
     * 城市是否免费
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "config/is_city_free")
    suspend fun is_city_free(@Body dto: IsCityFreeDto):DataResponse<Boolean?>

    /**
     * 注册
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/register")
    suspend fun register(@Body dto: RegisterUserDto): ApiResponse

    /**
     * 发送注册验证码
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/send_register_sms_code")
    suspend fun send_register_sms_code(@Body dto: SendSmsCodeDto): ApiResponse

    /**
     * 通过验证码注册
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/register_by_sms_code")
    suspend fun register_by_sms_code(@Body dto: RegisterBySmsCodeDto): ApiResponse

    /**
     * 用户名密码登录
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/login")
    suspend fun login(@Body dto: RegisterUserDto): DataResponse<LoginVO?>


    /**
     * 注册用户并同时登录，如果用户已存在则直接登录
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/register_login")
    suspend fun registerLogin(@Body dto: RegisterUserDto): DataResponse<LoginVO?>

    /**
     * 修改用户密码
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/change_password")
    suspend fun changePassword(@Body dto: ChangePasswordDto): ApiResponse

    /**
     * 注销账号
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/delete_user_by_self")
    suspend fun deleteUserBySelf(@Body dto: DeleteUserBySelfDto): ApiResponse


    /**
     * 获取用户购买的功能
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/user_features")
    suspend fun userFeatures(@Body dto: BaseDto): DataResponse<List<UserFeatureVO>?>

    /**
     * 获取用户的金币
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "user/user_gold_coin")
    suspend fun userGoldCoin(@Body dto: BaseDto): DataResponse<Int?>

    /**
     * 商品列表
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "product/list")
    suspend fun productList(@Body dto: ProductListDto): DataResponse<List<ProductVO>?>

    /**
     * 金币列表
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "product/list_gold_coin")
    suspend fun listGoldCoin(@Body dto: BaseDto): DataResponse<List<ProductVO>?>

    /**
     * 打赏列表
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "product/list_rewards")
    suspend fun listRewards(@Body dto: BaseDto): DataResponse<List<ProductVO>?>

    /**
     * 下单
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "order/confirm_order")
    suspend fun confirmOrder(@Body dto: ConfirmOrderDto): DataResponse<ConfirmOrderVO?>

    /**
     * 订单状态
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "order/order_status")
    suspend fun orderStatus(@Body dto: OrderStatusDto): DataResponse<OrderVO?>

    /**
     * 意见反馈
     * @param dto
     * @return
     */
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "feedback/addfeedback")
    suspend fun addFeedback(@Body dto: AddFeedbackDto): ApiResponse


    /**
     * 上传文件
     * @param file
     * @return
     */
    @Multipart
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "file/upload")
    suspend fun uploadFile(
        @Part("dto") dto: RequestBody,
        @Part file: MultipartBody.Part,
    ): DataResponse<Long?>


    /**
     * 上传文件
     * @param file
     * @return
     */
    @Multipart
    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "file/upload_forever")
    suspend fun uploadFileForever(
        @Part("dto") dto: RequestBody,
        @Part file: MultipartBody.Part,
    ): DataResponse<Long?>




    /**
     * 下载文件
     * @param file
     * @return
     */
    @Streaming
    @GET(XbqSdk.API_PREFIX_PLACEHOLDER + "file/download")
    suspend fun download(@Query("id") id: Long, @Query("token") token: String): Call<ResponseBody>







}