package com.xbq.xbqsdk.net.officeeditor



import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.net.base.DataResponse
import com.xbq.xbqsdk.net.officeeditor.dto.*
import com.xbq.xbqsdk.net.officeeditor.vo.OfficeArticle
import com.xbq.xbqsdk.net.officeeditor.vo.OfficeVideo
import com.xbq.xbqsdk.net.officeeditor.vo.OfficeVideoCollection
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Author: liaohaiping
 * Time: 2020-11-09
 * Description:
 */
interface OfficeEditorApi {

    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "officeeditor/collections_by_type")
    suspend fun collections_by_type(@Body dto: CollectionsByTypeDto): DataResponse<List<OfficeVideoCollection>?>


    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "officeeditor/collection_videos")
    suspend fun collection_videos(@Body dto: CollectionVideosDto): DataResponse<List<OfficeVideo>?>


    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "officeeditor/videos_by_type")
    suspend fun videos_by_type(@Body dto: VideosByTypeDto): DataResponse<List<OfficeVideo>?>


    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "officeeditor/related_recommend_videos")
    suspend fun related_recommend_videos(@Body dto: RelatedRecommendVideosDto): DataResponse<List<OfficeVideo>?>


    @POST(XbqSdk.API_PREFIX_PLACEHOLDER + "officeeditor/articles_by_type")
    suspend fun articles_by_type(@Body dto: ArticlesByTypeDto): DataResponse<List<OfficeArticle>?>




}