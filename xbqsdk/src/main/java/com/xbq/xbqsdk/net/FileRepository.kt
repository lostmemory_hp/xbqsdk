package com.xbq.xbqsdk.net

import android.webkit.MimeTypeMap
import com.blankj.utilcode.util.GsonUtils
import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.net.base.BaseDto
import com.xbq.xbqsdk.net.base.DataResponse
import com.xbq.xbqsdk.net.common.CommonApi
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2020-11-09
 * Description:
 */
class FileRepository @Inject constructor(val userCache: UserCache, val commonApi: CommonApi) {
    suspend fun uploadFile(file: File): DataResponse<Long?> {
        val dto =
            GsonUtils.toJson(BaseDto()).toRequestBody("application/json".toMediaTypeOrNull())
        val mediaType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.extension)?.toMediaTypeOrNull()
        val requestFile = file.asRequestBody(mediaType)
        return commonApi.uploadFile(dto, MultipartBody.Part.createFormData(
                "file",
                file.name,
                requestFile
            )
        )
    }

    suspend fun uploadFileForever(file: File): DataResponse<Long?> {
        val dto =
            GsonUtils.toJson(BaseDto()).toRequestBody("application/json".toMediaTypeOrNull())

        val mediaType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.extension)?.toMediaTypeOrNull()
        val requestFile = file.asRequestBody(mediaType)
        return commonApi.uploadFileForever(dto, MultipartBody.Part.createFormData(
                "file",
                file.name,
                requestFile
            )
        )
    }

    fun getDownloadUrl(fileId: Long): String {
        val sperator1 = if(XbqSdk.getBaseUrl().endsWith("/") || XbqSdk.getApiPrefix().startsWith("/")) "" else "/"
        val seperator2 = if(XbqSdk.getApiPrefix().endsWith("/")) "" else "/"
        return "${XbqSdk.getBaseUrl()}${sperator1}${XbqSdk.getApiPrefix()}${seperator2}file/download?id=${fileId}&token=${userCache.getToken()}"

    }


}