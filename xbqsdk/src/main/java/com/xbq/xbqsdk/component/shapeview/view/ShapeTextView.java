package com.xbq.xbqsdk.component.shapeview.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.xbq.xbqsdk.R;
import com.xbq.xbqsdk.component.shapeview.builder.ShapeDrawableBuilder;
import com.xbq.xbqsdk.component.shapeview.builder.TextColorBuilder;
import com.xbq.xbqsdk.component.shapeview.builder.TextViewDrawableSizeBuilder;
import com.xbq.xbqsdk.component.shapeview.styleable.ShapeTextViewStyleableTextView;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/ShapeView
 *    time   : 2021/07/17
 *    desc   : 支持直接定义 Shape 背景的 TextView
 */
public class ShapeTextView extends AppCompatTextView {

    private static final ShapeTextViewStyleableTextView STYLEABLE = new ShapeTextViewStyleableTextView();

    private final ShapeDrawableBuilder mShapeDrawableBuilder;
    private final TextColorBuilder mTextColorBuilder;
    private final TextViewDrawableSizeBuilder mTextViewDrawableSizeBuilder;

    public ShapeTextView(Context context) {
        this(context, null);
    }

    public ShapeTextView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public ShapeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ShapeTextView);
        mShapeDrawableBuilder = new ShapeDrawableBuilder(this, typedArray, STYLEABLE);
        mTextColorBuilder = new TextColorBuilder(this, typedArray, STYLEABLE);
        mTextViewDrawableSizeBuilder = new TextViewDrawableSizeBuilder(this, typedArray, STYLEABLE);
        typedArray.recycle();

        mShapeDrawableBuilder.intoBackground();

        if (mTextColorBuilder.isTextGradientColors()) {
            setText(getText());
        } else {
            mTextColorBuilder.intoTextColor();
        }
        mTextViewDrawableSizeBuilder.setDrawablesSize();
    }

    @Override
    public void setCompoundDrawables(@Nullable Drawable left, @Nullable Drawable top, @Nullable Drawable right, @Nullable Drawable bottom) {
        if(mTextViewDrawableSizeBuilder!=null) {
            mTextViewDrawableSizeBuilder.setDrawableBounds(left, 0);
            mTextViewDrawableSizeBuilder.setDrawableBounds(top, 1);
            mTextViewDrawableSizeBuilder.setDrawableBounds(right, 2);
            mTextViewDrawableSizeBuilder.setDrawableBounds(bottom, 3);
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public void setTextColor(int color) {
        super.setTextColor(color);
        if (mTextColorBuilder == null) {
            return;
        }
        mTextColorBuilder.setTextColor(color);
        mTextColorBuilder.clearTextGradientColors();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (mTextColorBuilder != null && mTextColorBuilder.isTextGradientColors()) {
            super.setText(mTextColorBuilder.buildLinearGradientSpannable(text), type);
        } else {
            super.setText(text, type);
        }
    }

    public ShapeDrawableBuilder getShapeDrawableBuilder() {
        return mShapeDrawableBuilder;
    }

    public TextColorBuilder getTextColorBuilder() {
        return mTextColorBuilder;
    }
}