package com.xbq.xbqsdk.component.shapeview.builder;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.View;

import com.xbq.xbqsdk.component.shapeview.styleable.IDimensionRatioStyleable;


/**
 * author : Android 轮子哥
 * github : https://github.com/getActivity/ShapeView
 * time   : 2021/08/28
 * desc   : ShapeDrawable 构建类
 */
public final class DimensionRatioBuilder {

    private static final int NO_COLOR = Color.TRANSPARENT;

    private final View mView;

    private Integer width;
    private Integer height;


    public DimensionRatioBuilder(View view, TypedArray typedArray, IDimensionRatioStyleable styleable) {
        mView = view;
        String dimRatio = typedArray.getString(styleable.getDimensionRatioStyleable());
        if (dimRatio != null) {
            String[] arr = dimRatio.split(":");
            if (arr.length == 2) {
                try {
                    width = Integer.parseInt(arr[0]);
                    height = Integer.parseInt(arr[1]);
                } catch (Exception ex) {
                    width = null;
                    height = null;
                }
            }
        }
    }

    public boolean isSetRatio(){
        return width!=null && height!=null;
    }

    public int getHeightMeasureSpec(int widthMeasureSpec, int heightMeasureSpec) {
        //获取宽度的模式和尺寸
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int widthSize2 = mView.getMeasuredWidth();
        if (isSetRatio()) {
            //根据宽高比ratio和模式创建一个测量值
            heightMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) (widthSize * height/width), View.MeasureSpec.EXACTLY);
        }
        return heightMeasureSpec;
    }

}