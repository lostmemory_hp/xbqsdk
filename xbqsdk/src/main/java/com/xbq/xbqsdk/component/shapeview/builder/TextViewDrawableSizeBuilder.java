package com.xbq.xbqsdk.component.shapeview.builder;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

import com.xbq.xbqsdk.component.shapeview.styleable.ITextViewDrawableSizeStyleable;


/**
 * author : Android 轮子哥
 * github : https://github.com/getActivity/ShapeView
 * time   : 2021/08/28
 * desc   : ShapeDrawable 构建类
 */
public final class TextViewDrawableSizeBuilder {

    private static final int NO_COLOR = Color.TRANSPARENT;

    private final TextView mView;


    private int mDrawableLeftSize;
    private int mDrawableRightSize;
    private int mDrawableTopSize;
    private int mDrawableBottomSize;

    private int mLeftWidth;
    private int mLeftHeight;
    private int mTopWidth;
    private int mTopHeight;
    private int mRightWidth;
    private int mRightHeight;
    private int mBottomWidth;
    private int mBottomHeight;


    public TextViewDrawableSizeBuilder(TextView view, TypedArray typedArray, ITextViewDrawableSizeStyleable styleable) {
        mView = view;

        mDrawableLeftSize = typedArray.getDimensionPixelSize(styleable.getDrawableLeftSizeStyleable(), 0);
        mDrawableRightSize = typedArray.getDimensionPixelSize(styleable.getDrawableRightSizeStyleable(), 0);
        mDrawableTopSize = typedArray.getDimensionPixelSize(styleable.getDrawableTopSizeStyleable(), 0);
        mDrawableBottomSize = typedArray.getDimensionPixelSize(styleable.getDrawableBottomSizeStyleable(), 0);

    }


    public void setDrawablesSize() {
        Drawable[] compoundDrawables = mView.getCompoundDrawables();
//        for (int i = 0; i < compoundDrawables.length; i++) {
//            switch (i) {
//                case 0:
//                    setDrawableBounds(compoundDrawables[0], mDrawableLeftSize, mDrawableLeftSize);
//                    break;
//                case 1:
//                    setDrawableBounds(compoundDrawables[1], mDrawableTopSize, mDrawableTopSize);
//                    break;
//                case 2:
//                    setDrawableBounds(compoundDrawables[2], mDrawableRightSize, mDrawableRightSize);
//                    break;
//                case 3:
//                    setDrawableBounds(compoundDrawables[3], mDrawableBottomSize, mDrawableBottomSize);
//                    break;
//                default:
//                    break;
//            }
//        }
        mView.setCompoundDrawables(compoundDrawables[0], compoundDrawables[1], compoundDrawables[2], compoundDrawables[3]);
    }


    private void setDrawableBounds(Drawable drawable, int width, int height) {
        if (drawable != null && (width != 0 || height != 0)) {
            double scale = (double) drawable.getIntrinsicHeight() / drawable.getIntrinsicWidth();
            drawable.setBounds(0, 0, width, height);
            Rect bounds = drawable.getBounds();
            //高宽只给一个值时，自适应
            if (bounds.right != 0 || bounds.bottom != 0) {
                if (bounds.right == 0) {
                    bounds.right = (int) (bounds.bottom / scale);
                    drawable.setBounds(bounds);
                }
                if (bounds.bottom == 0) {
                    bounds.bottom = (int) (bounds.right * scale);
                    drawable.setBounds(bounds);
                }
            }
        }
    }

    /**
     * @param drawable
     * @param direction 方位, 0:left, 1:top, 2:right, 3:bottom
     */
    public void setDrawableBounds(Drawable drawable, int direction) {
        switch (direction) {
            case 0:
                setDrawableBounds(drawable, mDrawableLeftSize, mDrawableLeftSize);
                break;
            case 1:
                setDrawableBounds(drawable, mDrawableTopSize, mDrawableTopSize);
                break;
            case 2:
                setDrawableBounds(drawable, mDrawableRightSize, mDrawableRightSize);
                break;
            case 3:
                setDrawableBounds(drawable, mDrawableBottomSize, mDrawableBottomSize);
                break;
            default:
                break;
        }
    }


}