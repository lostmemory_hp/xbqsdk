package com.xbq.xbqsdk.component.activity

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.gyf.immersionbar.ktx.immersionBar
import com.xbq.xbqsdk.R
import java.lang.reflect.ParameterizedType

/**
 * Author: liaohaiping
 * Time: 2021-10-26
 * Description:
 */
open class VBActivity<T : ViewBinding> : AppCompatActivity(), IFragmentActivity {
    lateinit var binding: T
    lateinit var context: Context
    var lastFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        val clz =
            (this::class.java.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>
        val inflate = clz.getMethod("inflate", LayoutInflater::class.java)
        binding = inflate.invoke(null, layoutInflater) as T
        setContentView(binding.root)
        val titleBar = findViewById<View>(R.id.titlebar)
        titleBar?.let {
            immersionBar { titleBar(R.id.titlebar) }
        }
    }


    override fun showFragment(@IdRes containerViewId: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .apply {
                lastFragment?.let { hide(it) }
                if (!fragment.isAdded) {
                    add(containerViewId, fragment)
                }
            }
            .show(fragment)
            .commitAllowingStateLoss()
        lastFragment = fragment
    }


    override fun <T:Fragment> showFragment(@IdRes containerViewId: Int, clazz: Class<T>) {
        var fragment = supportFragmentManager.findFragmentByTag(clazz.name)
        if(fragment==null) {
            fragment = clazz.newInstance()
        }
        supportFragmentManager.beginTransaction()
            .apply {
                lastFragment?.let { hide(it) }
                if (!fragment!!.isAdded) {
                    add(containerViewId, fragment,clazz.name)
                }
            }
            .show(fragment!!)
            .commitAllowingStateLoss()
        lastFragment = fragment
    }
    override fun <T:Fragment> showFragment(@IdRes containerViewId: Int, clazz: Class<T>, tag:String) {
        var fragment = supportFragmentManager.findFragmentByTag(tag)
        if(fragment==null) {
            fragment = clazz.newInstance()
        }
        supportFragmentManager.beginTransaction()
            .apply {
                lastFragment?.let { hide(it) }
                if (!fragment!!.isAdded) {
                    add(containerViewId, fragment,tag)
                }
            }
            .show(fragment!!)
            .commitAllowingStateLoss()
        lastFragment = fragment
    }


}