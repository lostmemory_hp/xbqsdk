package com.xbq.xbqsdk.component.layout

/**
 * Author: liaohaiping
 * Time: 2020-10-14
 * Description:
 */
class XbqGridLayoutData<T>(val arr: Iterable<T>, val columnCount: Int) {
    var rowCount = 0
    var totalCount = 0

    init {
        totalCount = arr.count()
        rowCount = Math.ceil(totalCount.toDouble() / columnCount).toInt()
    }

    /**
     * 获取某一行的数据
     * @param row 从0开始
     */
    fun getRowDatas(row: Int): List<T> {
        val rowStartIndex = row * columnCount
        val rowEndIndex = Math.min(totalCount, rowStartIndex + columnCount)-1
        return arr.filterIndexed { index, t -> index >= rowStartIndex && index <= rowEndIndex }
    }

    /**
     * 获取某一列的数据
     * @param column 从0开始
     */
    fun getColumnDatas(column: Int): List<T> {
        return arr.filterIndexed { index, t -> index % columnCount == column }
    }

    /**
     * 获取同一行中序号小于当前位置的数据
     * @param curIndex 当前位置
     */
    fun getPreRowDatas(curIndex: Int): List<T> {
        val row = curIndex / columnCount
        val rowStartIndex = row * columnCount
        val rowEndIndex = Math.min(totalCount, rowStartIndex + columnCount)-1
        return arr.filterIndexed { index, t -> index < curIndex && index >= rowStartIndex && index <= rowEndIndex }
    }

    /**
     * 获取同一行中序号小于当前位置的数据
     * @param curIndex 当前位置
     */
    fun getPreOrEqualRowDatas(curIndex: Int): List<T> {
        val row = curIndex / columnCount
        val rowStartIndex = row * columnCount
        val rowEndIndex = Math.min(totalCount, rowStartIndex + columnCount)-1
        return arr.filterIndexed { index, t -> index <= curIndex && index >= rowStartIndex && index <= rowEndIndex }
    }

    /**
     * 获取同一列中序号小于当前位置的数据
     * @param curIndex 当前位置
     */
    fun getPreColumnDatas(curIndex: Int): List<T> {
        var column = curIndex % columnCount
        return arr.filterIndexed { index, t -> index < curIndex && index % columnCount == column }
    }

    /**
     * 获取同一列中序号小于等于当前位置的数据
     * @param curIndex 当前位置
     */
    fun getPreOrEqualColumnDatas(curIndex: Int): List<T> {
        var column = curIndex % columnCount
        return arr.filterIndexed { index, t -> index <= curIndex && index % columnCount == column }
    }

}