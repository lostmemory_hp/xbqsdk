package com.xbq.xbqsdk.component.activity

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
interface IFragmentActivity {
    fun showFragment(@IdRes containerViewId: Int, fragment: Fragment)
    fun <T: Fragment> showFragment(@IdRes containerViewId: Int, clazz: Class<T>, tag:String)
    fun <T:Fragment> showFragment(@IdRes containerViewId: Int, clazz: Class<T>)

}