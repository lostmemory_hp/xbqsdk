package com.xbq.xbqsdk.component.layout

import android.content.Context
import android.content.res.TypedArray
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.view.children
import com.xbq.xbqsdk.component.layout.XbqGridLayoutData
import com.xbq.xbqsdk.R


/**
 * Author: liaohaiping
 * Time: 2020-10-13
 * Description:
 */
class XbqGridLayout : ViewGroup {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }


    fun init(attrs: AttributeSet?) {
        val typedArray: TypedArray =
            context.obtainStyledAttributes(attrs, R.styleable.XbqGridLayout)
        columnCount = typedArray.getInt(R.styleable.XbqGridLayout_column_count, 1)
        itemHeightMode =
            typedArray.getInt(R.styleable.XbqGridLayout_item_height_mode, ITEM_HEIGHT_MODE_ROW_SAME)
        sameItemWidth =
            typedArray.getBoolean(R.styleable.XbqGridLayout_same_item_width, true)
        itemGravity = typedArray.getInt(
            R.styleable.XbqGridLayout_item_gravity,
            ITEM_GRAVITY_TOP or ITEM_GRAVITY_LEFT
        )
        rowSpace = typedArray.getDimensionPixelSize(R.styleable.XbqGridLayout_row_space, 0)
        columnSpace = typedArray.getDimensionPixelSize(R.styleable.XbqGridLayout_column_space, 0)
        typedArray.recycle()
    }

    var columnCount: Int = 1

    //单元格高度计算模式
    var itemHeightMode: Int = ITEM_HEIGHT_MODE_ROW_SAME

    //单元格宽度是否保持一致
    var sameItemWidth: Boolean = true

    //单元格内空间排列方式
    var itemGravity: Int = ITEM_GRAVITY_LEFT or ITEM_GRAVITY_TOP

    //单元格垂直方向上的间距
    var columnSpace: Int = 0

    //单元格水平方向上的间距
    var rowSpace: Int = 0

    var maxCellWidth = 0;
    var maxCellHeight = 0;
    var rowCount = 0;

    var rowHeightMap = mutableMapOf<Int, Int>()

    companion object {
        val TAG = XbqGridLayout::class.java.simpleName
        const val ITEM_GRAVITY_LEFT = 0x10
        const val ITEM_GRAVITY_RIGHT = 0x20
        const val ITEM_GRAVITY_CENTER_HORIZONTAL = 0x30
        const val ITEM_GRAVITY_TOP = 0x01
        const val ITEM_GRAVITY_BOTTOM = 0x02
        const val ITEM_GRAVITY_CENTER_VERTICAL = 0x03
        const val ITEM_GRAVITY_HORIZONTAL_MASK = 0xF0 //横向排列掩码
        const val ITEM_GRAVITY_VERTICAL_MASK = 0x0F //横向排列掩码

        //同一行的高度相同
        const val ITEM_HEIGHT_MODE_ROW_SAME = 1

        //所有单元格的高度都相同
        const val ITEM_HEIGHT_MODE_ALL_SAME = 2

        //使用单元格内控件自身的高度
        const val ITEM_HEIGHT_MODE_CONTENT_HEIGHT = 3
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val selfWidthMode = MeasureSpec.getMode(widthMeasureSpec)
        val selfWidthSize = MeasureSpec.getSize(widthMeasureSpec)
        val selfHeightMode = MeasureSpec.getMode(heightMeasureSpec)
        val selfHeightSize = MeasureSpec.getSize(heightMeasureSpec)
        val selfAvailableWidth =
            selfWidthSize - columnSpace * (columnCount - 1) - paddingLeft - paddingRight
        val selfAvailableHeight =
            selfHeightSize - rowSpace * (rowCount - 1) - paddingTop - paddingBottom

        Log.d(
            TAG,
            "self spec: width mode=${selfWidthMode}, width size=${selfWidthSize},  height mode=${selfHeightMode}, height size=${selfHeightSize}"
        )
        rowCount = Math.ceil(childCount.toDouble() / columnCount).toInt()
        val tableData = XbqGridLayoutData(children.asIterable(), columnCount)
        for (i in 0 until childCount) {
            measureChild(
                getChildAt(i).apply { tag = i },
                selfWidthMode,
                selfAvailableWidth,
                selfHeightMode,
                selfAvailableHeight
            )
        }
        maxCellWidth = children.maxOf { it.measuredWidth }
        maxCellHeight = children.maxOf { it.measuredHeight }
        val mw = if (sameItemWidth) maxCellWidth * columnCount else {
            var maxRowWidth = 0;
            for (row in 0 until rowCount) {
                var rowWidth = tableData.getRowDatas(row).sumOf { it.measuredWidth }
                if (rowWidth > maxRowWidth) {
                    maxRowWidth = rowWidth
                }
            }
            maxRowWidth
        }
        val mh = when (itemHeightMode) {
            ITEM_HEIGHT_MODE_ALL_SAME -> maxCellHeight * rowCount
            ITEM_HEIGHT_MODE_ROW_SAME -> {
                var totalHeight = 0
                for (row in 0 until rowCount) {
                    val rowHeight = tableData.getRowDatas(row).maxOf { it.measuredHeight }
                    rowHeightMap.put(row, rowHeight)
                    totalHeight += rowHeight
                }
                totalHeight
            }
            else -> {
                var maxColumnHeight = 0;
                for (i in 0 until columnCount) {
                    var columnHeight = tableData.getColumnDatas(i).sumOf { it.measuredHeight }
                    if (columnHeight > maxColumnHeight) {
                        maxColumnHeight = columnHeight
                    }
                }
                maxColumnHeight
            }
        }
        setMeasuredDimension(
            mw + (columnCount - 1) * columnSpace + paddingLeft + paddingRight,
            mh + (rowCount - 1) * rowSpace + paddingTop + paddingBottom
        )

    }

    private fun measureChild(
        child: View,
        selfWidthMode: Int,
        selfWidthSize: Int,
        selfHeightMode: Int,
        selfHeightSize: Int
    ) {
        val lp = child.layoutParams
        val wSpec = when (lp.width) {
            LayoutParams.MATCH_PARENT -> {
                when (selfWidthMode) {
                    MeasureSpec.EXACTLY -> MeasureSpec.makeMeasureSpec(
                        selfWidthSize / columnCount,
                        MeasureSpec.EXACTLY
                    )
                    MeasureSpec.AT_MOST -> MeasureSpec.makeMeasureSpec(
                        selfWidthSize / columnCount,
                        MeasureSpec.AT_MOST
                    )
                    else -> MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                }
            }
            LayoutParams.WRAP_CONTENT -> {
                when (selfWidthMode) {
                    MeasureSpec.EXACTLY,
                    MeasureSpec.AT_MOST -> MeasureSpec.makeMeasureSpec(
                        selfWidthSize / columnCount,
                        MeasureSpec.AT_MOST
                    )
                    else -> MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                }

            }
            else -> MeasureSpec.makeMeasureSpec(lp.width, MeasureSpec.EXACTLY)
        }
        val hSpec = when (lp.height) {
            LayoutParams.MATCH_PARENT -> {
                when (selfHeightMode) {
                    MeasureSpec.EXACTLY,
                    MeasureSpec.AT_MOST -> MeasureSpec.makeMeasureSpec(
                        selfHeightSize / rowCount,
                        MeasureSpec.EXACTLY
                    )
                    else -> MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                }
            }
            LayoutParams.WRAP_CONTENT -> {
                when (selfHeightMode) {
                    MeasureSpec.EXACTLY, MeasureSpec.AT_MOST -> MeasureSpec.makeMeasureSpec(
                        selfHeightSize / rowCount,
                        MeasureSpec.AT_MOST
                    )
                    else -> MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                }
            }
            else -> MeasureSpec.makeMeasureSpec(lp.height, MeasureSpec.EXACTLY)
        }
        child.measure(wSpec, hSpec)
        Log.d(TAG,"child_${child.tag}: selfWidthMode=${selfWidthMode}, selfWidthSize=${selfWidthSize},layoutParam.width=${lp.width}, wSpec=${wSpec}, width=${child.measuredWidth}")
    }


    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val tableData = XbqGridLayoutData(children.asIterable(), columnCount)
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            //当前列数，从0开始
            val column = i % columnCount
            //当前行数，从0 开始
            val row = Math.ceil((i + 1).toDouble() / columnCount).toInt() - 1
            val cellLeft =
                paddingLeft + columnSpace * column + if (sameItemWidth) maxCellWidth * column else {
                    tableData.getPreRowDatas(i).sumOf { it.measuredWidth }
                }

            val cellTop = paddingTop + rowSpace * row + when (itemHeightMode) {
                ITEM_HEIGHT_MODE_ALL_SAME -> maxCellHeight * row
                ITEM_HEIGHT_MODE_ROW_SAME -> {
                    (0 until row).sumOf { rowHeightMap.get(it) ?: 0 }
                }
                else -> {
                    tableData.getPreColumnDatas(i).sumOf { it.measuredHeight }
                }
            }
            val cellWidth = if (sameItemWidth) maxCellWidth else child.measuredWidth
            val cellHeight = when (itemHeightMode) {
                ITEM_HEIGHT_MODE_ALL_SAME -> maxCellHeight
                ITEM_HEIGHT_MODE_ROW_SAME -> rowHeightMap.get(row) ?: 0
                else -> child.measuredHeight
            }

            var childLeft = when (itemGravity and ITEM_GRAVITY_HORIZONTAL_MASK) {
                ITEM_GRAVITY_CENTER_HORIZONTAL -> (cellWidth - child.measuredWidth) / 2 + cellLeft
                ITEM_GRAVITY_LEFT -> cellLeft
                ITEM_GRAVITY_RIGHT -> cellLeft + cellWidth - child.measuredWidth
                else -> cellLeft
            }
            var childTop = when (itemGravity and ITEM_GRAVITY_VERTICAL_MASK) {
                ITEM_GRAVITY_CENTER_VERTICAL -> cellTop + (cellHeight - child.measuredHeight) / 2
                ITEM_GRAVITY_TOP -> cellTop
                ITEM_GRAVITY_BOTTOM -> cellTop + cellHeight - child.measuredHeight
                else -> cellTop
            }

            val childRight = childLeft + child.measuredWidth
            val childBottom = childTop + child.measuredHeight
            child.layout(childLeft, childTop, childRight, childBottom)
        }
    }


}