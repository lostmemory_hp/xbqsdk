package com.xbq.xbqsdk.component.recyleview;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;


public class LinearSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int spaceInPixel;  //位移间距,单位像素
    private int orientation;

    public LinearSpaceItemDecoration(int spacePx, @RecyclerView.Orientation int orientation) {
        this.spaceInPixel = spacePx;
        this.orientation = orientation;
    }
    public LinearSpaceItemDecoration(int spacePx) {
        this.spaceInPixel = spacePx;
        this.orientation = RecyclerView.VERTICAL;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position > 0) {
            if (orientation == RecyclerView.HORIZONTAL) {
                outRect.left = spaceInPixel;
            } else if (orientation == RecyclerView.VERTICAL) {
                outRect.top = spaceInPixel;
            }
        }

    }

}