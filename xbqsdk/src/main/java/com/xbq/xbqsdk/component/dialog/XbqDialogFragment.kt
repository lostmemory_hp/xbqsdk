package com.xbq.xbqsdk.component.dialog

import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.viewbinding.ViewBinding
import java.lang.reflect.ParameterizedType

/**
 * Author: liaohaiping
 * Time: 2021-10-27
 * Description:
 */
abstract class XbqDialogFragment<T : ViewBinding> : DialogFragment() {
    var marginHorizontal: Int = 24
    var tranparentBackground: Boolean = false

    lateinit var binding: T
    private var width: Int = 0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val clz =
            (this::class.java.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>
        val inflate = clz.getMethod("inflate", LayoutInflater::class.java)
        binding = inflate.invoke(null, layoutInflater) as T
        return binding.root
    }

    override fun onStart() {
        resources.displayMetrics.widthPixels?.let {
            dialog?.window?.attributes?.width = it - dp2px(marginHorizontal)  //12dp的边距
        }
        setStyle(STYLE_NO_TITLE, theme)
        dialog?.window?.setGravity(Gravity.CENTER)
        if (tranparentBackground) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        super.onStart()

    }

    fun dp2px(dp: Int): Int {
        val scale = Resources.getSystem().displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

}