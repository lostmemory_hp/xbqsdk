package com.xbq.xbqsdk.component.shapeview.styleable;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/ShapeView
 *    time   : 2021/08/28
 *    desc   : 文本颜色 View 属性收集接口
 */
public interface ITextViewDrawableSizeStyleable {

    int getDrawableLeftSizeStyleable();

    int getDrawableRightSizeStyleable();

    int getDrawableTopSizeStyleable();

    int getDrawableBottomSizeStyleable();

}