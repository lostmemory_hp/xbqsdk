package com.xbq.xbqsdk.component.shapeview.styleable;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/ShapeView
 *    time   : 2021/08/28
 *    desc   : shape的宽高比
 */
public interface IDimensionRatioStyleable {

    int getDimensionRatioStyleable();

}