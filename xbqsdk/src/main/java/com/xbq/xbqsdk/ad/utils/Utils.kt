package com.xbq.xbqsdk.ad.utils

import android.content.Context
import android.graphics.Point
import android.os.Build
import android.view.WindowManager

/**
 * Author: liaohaiping
 * Time: 2021-03-31
 * Description:
 */
fun Context.px2dp(pxValue: Int): Int {
    val scale = this.resources.displayMetrics.density;
    return (pxValue / scale + 0.5f).toInt()
}

fun Context.dp2px(dp: Int): Int {
    val scale = resources.displayMetrics.density
    return (dp * scale + 0.5f).toInt()
}

fun Context.screenWidthDp(): Int {
    return px2dp(screenWidthPx())
}

fun Context.screenWidthPx(): Int {
    val systemService = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val size = Point()
    systemService.defaultDisplay.getSize(size)
    return size.x
}

fun Context.screenSizePx(): Point {
    val systemService = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val size = Point()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        systemService.currentWindowMetrics.bounds.let {
            size.x = it.width()
            size.y = it.height()
        }
    }else {
        systemService.defaultDisplay.getSize(size)
    }
    return size
}

fun Context.screenSizeDp(): Point {
    val systemService = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val size = Point()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        systemService.currentWindowMetrics.bounds.let {
            size.x = it.width()
            size.y = it.height()
        }
    } else {
        systemService.defaultDisplay.getSize(size)
    }
    return Point(px2dp(size.x), px2dp(size.y))
}