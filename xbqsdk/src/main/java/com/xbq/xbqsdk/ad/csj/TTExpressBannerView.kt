package com.xbq.xbqsdk.ad.csj

import android.app.Activity
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import com.bytedance.sdk.openadsdk.*
import com.xbq.xbqsdk.ad.utils.XbqAdConfig
import com.xbq.xbqsdk.ad.utils.px2dp
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean


/**
 * Author: liaohaiping
 * Time: 2021-03-31
 * Description: 模版banner广告view
 */
class TTExpressBannerView : FrameLayout {
    companion object {
        const val TAG = "TTExpressBannerView"
    }

    private var mTTAdNative: TTAdNative? = null
        private get() {
            if (field == null) {
                field = CsjAd.get()?.createAdNative(context)
            }
            return field
        }

    var mTTAd: TTNativeExpressAd? = null

    private var codeId: String = ""
    private var activityRef: WeakReference<Activity>? = null
    private var loading: AtomicBoolean = AtomicBoolean(false)

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        loadAd()
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        when (visibility) {
            View.VISIBLE -> loadAd()
            View.GONE, View.INVISIBLE -> {
                mTTAd?.destroy()
                mTTAd = null
            }
        }
    }


    fun init(activity: Activity) {
        activityRef = WeakReference(activity)
        if (XbqAdConfig.isCsj() && XbqAdConfig.showBanner() && TTAdSdk.isInitSuccess()) {
            this.codeId = XbqAdConfig.bannerId
            loadAd()
        }
    }

    fun loadAd() {
        if (loading.get()) {
            return
        }
        if (codeId.isEmpty() || width == 0) {
            return
        }
        if (mTTAd != null) {
            return
        }

        loading.set(true)

        val adSlot = AdSlot.Builder()
            .setCodeId(codeId) //广告位id
            .setSupportDeepLink(true)
            .setAdCount(1) //请求广告数量为1到3条
            .setExpressViewAcceptedSize(context.px2dp(width).toFloat(), 45f) //期望模板广告view的size,单位dp
            .setAdLoadType(TTAdLoadType.LOAD) //推荐使用，用于标注此次的广告请求用途为预加载（当做缓存）还是实时加载，方便后续为开发者优化相关策略
            .build()


        mTTAdNative?.loadBannerExpressAd(adSlot, object : TTAdNative.NativeExpressAdListener {
            //请求失败回调
            override fun onError(code: Int, message: String) {
                Log.e(TAG, "onError: $message")
                loading.set(false)
            }

            //请求成功回调
            override fun onNativeExpressAdLoad(ads: List<TTNativeExpressAd>) {
                if (ads.isEmpty()) {
                    return
                }
                mTTAd = ads[0]
//                mTTAd!!.setSlideIntervalTime(30 * 1000)
                bindAdListener(mTTAd!!)
                mTTAd?.render()
                loading.set(false)
                Log.d(TAG, "onNativeExpressAdLoad: load success!")
            }

        })

    }


    private fun bindAdListener(ad: TTNativeExpressAd) {
        ad.setExpressInteractionListener(object : TTNativeExpressAd.ExpressAdInteractionListener {
            override fun onAdClicked(view: View, type: Int) {
                Log.d(TAG, "onAdClicked: 广告被点击")
            }

            override fun onAdShow(view: View, type: Int) {
                Log.d(TAG, "onAdShow: 广告展示")
            }

            override fun onRenderFail(view: View, msg: String, code: Int) {
                Log.d(TAG, "onRenderFail: $msg code:$code")
            }

            override fun onRenderSuccess(view: View, width: Float, height: Float) {
                Log.e(TAG, "onRenderSuccess: 渲染成功: width=$width, height=$height")
                //返回view的宽高 单位 dp
                removeAllViews()
                addView(view)

            }
        })
        //dislike设置
        bindDislike(ad, false)
    }

    /**
     * 设置广告的不喜欢, 注意：强烈建议设置该逻辑，如果不设置dislike处理逻辑，则模板广告中的 dislike区域不响应dislike事件。
     *
     * @param ad
     * @param customStyle 是否自定义样式，true:样式自定义
     */
    private fun bindDislike(ad: TTNativeExpressAd, customStyle: Boolean) {
        if (customStyle) {
            //使用自定义样式
            val dislikeInfo = ad.dislikeInfo
            if (dislikeInfo == null || dislikeInfo.filterWords == null || dislikeInfo.filterWords.isEmpty()) {
                return
            }
            val dislikeDialog = DislikeDialog(context, dislikeInfo)
            dislikeDialog.setOnDislikeItemClick { filterWord -> //屏蔽广告
                Log.d(TAG, "onItemClick: 点击 " + filterWord.name)
                //用户选择不喜欢原因后，移除广告展示
                removeAllViews()
            }
            dislikeDialog.setOnPersonalizationPromptClick {
                Log.d(TAG, "onClick: 点击了为什么看到此广告")
            }
            ad.setDislikeDialog(dislikeDialog)
            return
        }
        activityRef?.get()?.let {
            //        //使用默认模板中默认dislike弹出样式
            ad.setDislikeCallback(it, object : TTAdDislike.DislikeInteractionCallback {
                override fun onShow() {}
                override fun onSelected(p0: Int, p1: String?, p2: Boolean) {
                    //用户选择不喜欢原因后，移除广告展示
                    removeAllViews()
                }

                override fun onCancel() {
                    Log.d(TAG, "onCancel: 点击取消 ")
                }

            })
        }
    }


}