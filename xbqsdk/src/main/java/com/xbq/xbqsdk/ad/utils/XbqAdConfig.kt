package com.xbq.xbqsdk.ad.utils

/**
 * Author: liaohaiping
 * Time: 2021-03-31
 * Description:
 */
object XbqAdConfig {
    var type: String = "csj"
    var appId: String = ""
    var kpId: String = ""
    var bannerId: String = ""
    var tpId: String = ""
    var cpId: String = ""
    var jlspId: String = ""
    var xxlId: String = ""
    private val adMap: MutableMap<String, String> = mutableMapOf()

    private var showKp: Boolean = false
    private var showBanner: Boolean = false
    private var showTp: Boolean = false
    private var showCp: Boolean = false
    private var showJlsp: Boolean = false
    private var showXxl: Boolean = false
    private var showHaoping: Boolean = false
    private val adShowMap: MutableMap<String, Boolean> = mutableMapOf()


    fun setConfigData(map: Map<String, String?>) {
        // ad config
        var adConfig = map["ad_config"] ?: ""
        adConfig.split(",", "，")
            .map { it.split("=") }
            .filter { it.size == 2 }
            .forEach { adMap.put(it[0], it[1]) }
        type = adMap["type"] ?: "csj"
        appId = adMap["app"] ?: ""
        kpId = adMap["kp"] ?: ""
        bannerId = adMap["banner"] ?: ""
        tpId = adMap["tp"] ?: ""
        cpId = adMap["cp"] ?: ""
        jlspId = adMap["jlsp"] ?: ""
        xxlId = adMap["xxl"] ?: ""


        //ad show config
        var adShowConfig = map["ad_show_config"] ?: ""
        adShowConfig.split(",", "，")
            .map { it.split("=") }
            .filter { it.size == 2 }
            .forEach { adShowMap[it[0]] = ("1" == it[1]) }
        showKp = adShowMap["kp"] ?: false
        showBanner = adShowMap["banner"] ?: false
        showTp = adShowMap["tp"] ?: false
        showCp = adShowMap["cp"] ?: false
        showJlsp = adShowMap["jlsp"] ?: false
        showXxl = adShowMap["xxl"] ?: false
        showHaoping = adShowMap["hp"] ?: false
    }

    /**
     * 是否显示开屏广告
     */
    fun showKaiping(): Boolean =
        showKp && appId.isNotEmpty() && kpId.isNotEmpty()

    /**
     * 是否显示banner广告
     */
    fun showBanner(): Boolean =
        showBanner && appId.isNotEmpty() && bannerId.isNotEmpty()

    /**
     * 是否显示激励视屏广告
     */
    fun showJlsp(): Boolean =
        showJlsp && appId.isNotEmpty() && jlspId.isNotEmpty()

    /**
     * 是否显示信息流广告
     */
    fun showXxl(): Boolean =
        showXxl && appId.isNotEmpty() && xxlId.isNotEmpty()

    /**
     * 是否显示插屏广告
     */
    fun showCp(): Boolean =
        showCp && appId.isNotEmpty() && cpId.isNotEmpty()

    /**
     * 是否显示退屏广告
     */
    fun showTp(): Boolean =
        showTp && appId.isNotEmpty() && tpId.isNotEmpty()

    /**
     * 是否显示好评弹窗
     */
    fun showHaoping(): Boolean = showHaoping


    /**
     * 是否是穿山甲广告
     */
    fun isCsj(): Boolean = type == "csj"
    /**
     * 是否是广点通广告
     */
    fun isGdt(): Boolean = type == "gdt"
    /**
     * 是否是百度广告
     */
    fun isBaidu(): Boolean = type == "baidu"

}
