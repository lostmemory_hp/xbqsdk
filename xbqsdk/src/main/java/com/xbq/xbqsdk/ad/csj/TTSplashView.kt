package com.xbq.xbqsdk.ad.csj

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.MainThread
import androidx.annotation.RequiresApi
import com.bytedance.sdk.openadsdk.*
import com.bytedance.sdk.openadsdk.TTAdNative.SplashAdListener
import com.xbq.xbqsdk.ad.utils.XbqAdConfig
import com.xbq.xbqsdk.ad.utils.UIUtils

/**
 * Author: liaohaiping
 * Time: 2021-04-01
 * Description: 开屏广告view
 */
class TTSplashView : FrameLayout {
    companion object{
        const val TAG = "TTSplashView"
    }

    private var mTTAdNative: TTAdNative? = null
        private get() {
            if (field == null) {
                field = CsjAd.get()?.createAdNative(context)
            }
            return field
        }

    //开屏广告加载超时时间,建议大于3000,这里为了冷启动第一次加载到广告并且展示,示例设置了3000ms
    private val AD_TIME_OUT = 3000
    private var mCodeId = ""
    private var adView: View? = null
    var onAdComplete: (() -> Unit)? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)


    fun init(onAdCompleteListener: (() -> Unit)?) {
        this.onAdComplete = onAdCompleteListener

        if(XbqAdConfig.showKaiping() && TTAdSdk.isInitSuccess()){
            this.mCodeId = XbqAdConfig.kpId
            //加载开屏广告
            loadSplashAd()
        }else{
            fireAdComplete()
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w>0 && h>0) {
            loadSplashAd()
        }
    }

    /**
     * 加载开屏广告
     */
    private fun loadSplashAd() {
        if (mCodeId.isEmpty() || width == 0) {
            return
        }
        //step3:创建开屏广告请求参数AdSlot,具体参数含义参考文档
        var adSlot: AdSlot? = null

        //step3:创建开屏广告请求参数AdSlot,具体参数含义参考文档
        val splashWidthDp: Float = UIUtils.getScreenWidthDp(context)
        val splashWidthPx: Int = UIUtils.getScreenWidthInPx(context)
        val screenHeightPx: Int = UIUtils.getScreenHeight(context)
        val screenHeightDp: Float = UIUtils.px2dip(context, screenHeightPx.toFloat()).toFloat()
        var splashHeightDp: Float
        var splashHeightPx: Int
//        if (mIsHalfSize) {
//            // 开屏高度 = 屏幕高度 - 下方预留的高度，demo中是预留了屏幕高度的1/5，因此开屏高度传入 屏幕高度*4/5
//            splashHeightDp = screenHeightDp * 4 / 5f
//            splashHeightPx = (screenHeightPx * 4 / 5f).toInt()
//        } else {
//            splashHeightDp = screenHeightDp
//            splashHeightPx = screenHeightPx
//        }
        splashHeightDp = screenHeightDp
        splashHeightPx = screenHeightPx

        adSlot = AdSlot.Builder()
            .setCodeId(mCodeId) //模板广告需要设置期望个性化模板广告的大小,单位dp,代码位是否属于个性化模板广告，请在穿山甲平台查看
            .setExpressViewAcceptedSize(splashWidthDp,splashHeightDp)
            .setImageAcceptedSize(splashWidthPx,splashHeightPx)
            .setAdLoadType(TTAdLoadType.PRELOAD)
            .build()


        //step4:请求广告，调用开屏广告异步请求接口，对请求回调的广告作渲染处理
        mTTAdNative?.loadSplashAd(adSlot, object : SplashAdListener {
            @MainThread
            override fun onError(code: Int, message: String) {
                Log.d(TAG, "onError: $message")
                fireAdComplete()
            }

            @MainThread
            override fun onTimeout() {
                Log.d(TAG, "onAdClicked: 开屏广告加载超时")
                fireAdComplete()
            }

            @MainThread
            override fun onSplashAdLoad(ad: TTSplashAd) {
                Log.d(TAG, "开屏广告请求成功")
                //获取SplashView
                adView = ad.splashView
                removeAllViews()
                addView(adView)
                Log.d(TAG, "onSplashAdLoad: thread : ${Thread.currentThread().id}")
                //设置SplashView的交互监听器
                ad.setSplashInteractionListener(object : TTSplashAd.AdInteractionListener {
                    override fun onAdClicked(view: View, type: Int) {
                        Log.d(TAG, "onAdClicked: 开屏广告点击")
                    }

                    override fun onAdShow(view: View, type: Int) {
                        Log.d(TAG, "onAdShow: 开屏广告展示")
                    }

                    override fun onAdSkip() {
                        Log.d(TAG, "onAdSkip: 开屏广告跳过")
                        fireAdComplete()
                    }

                    override fun onAdTimeOver() {
                        Log.d(TAG, "onAdTimeOver: 开屏广告倒计时结束")
                        fireAdComplete()
                    }
                })
            }
        }, AD_TIME_OUT)
    }


    private fun fireAdComplete() {
        onAdComplete?.invoke()
    }

}