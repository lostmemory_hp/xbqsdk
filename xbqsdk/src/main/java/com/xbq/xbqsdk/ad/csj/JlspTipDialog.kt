package com.xbq.xbqad.csj

import android.app.AlertDialog
import android.content.Context
import com.xbq.xbqsdk.ad.utils.XbqAdConfig

/**
 * Author: liaohaiping
 * Time: 2021-04-06
 * Description:
 */
class JlspTipDialog(val context: Context) {
    var onBuyVip: (() -> Unit)? = null
    var onLookVideo: (() -> Unit)? = null
    var onCancel: (() -> Unit)? = null


    fun showTip(msg: String) {
        if(XbqAdConfig.showJlsp()) {
            AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton("购买会员") { dlg, w -> onBuyVip?.invoke() }
                .setNegativeButton("观看广告") { dlg, w -> onLookVideo?.invoke() }
                .setNeutralButton("取消") { dlg, w -> onCancel?.invoke() }
                .setCancelable(false)
                .show()
        }else{
            onBuyVip?.invoke()
        }

    }


}