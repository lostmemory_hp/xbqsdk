package com.xbq.xbqsdk.ad.csj

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import com.bytedance.sdk.openadsdk.*
import com.bytedance.sdk.openadsdk.TTAdSdk.InitCallback
import com.xbq.xbqsdk.ad.utils.XbqAdConfig.appId
import com.xbq.xbqsdk.ad.utils.XbqAdConfig.isCsj
import kotlin.coroutines.suspendCoroutine

/**
 * 可以用一个单例来保存TTAdManager实例，在需要初始化sdk的时候调用
 */
object CsjAd {
    private const val TAG = "TTAdManagerHolder"
    var globalTTAdNative: TTAdNative? = null
        private set

    fun get(): TTAdManager? {
        return if (TTAdSdk.isInitSuccess()) TTAdSdk.getAdManager() else null
    }

    /**
     * 要在AdConfig.setConfigData()之后调用
     * @param context
     */
    suspend fun init(context: Context) {
        if (isCsj() && appId.length > 0) {
            doInit(context.applicationContext, appId)
        }
    }

    //step1:接入网盟广告sdk的初始化操作，详情见接入文档和穿山甲平台说明
    private suspend fun doInit(context: Context, appId: String): Boolean {
        return suspendCoroutine<Boolean> {
            TTAdSdk.init(context, buildConfig(context, appId), object : InitCallback {
                override fun success() {
                    globalTTAdNative = TTAdSdk.getAdManager().createAdNative(context.applicationContext)
                    it.resumeWith(Result.success(true))
                }

                override fun fail(i: Int, s: String) {
                    it.resumeWith(Result.success(false))
                }
            })
        }
    }

    private fun buildConfig(context: Context, appId: String): TTAdConfig {
        val appName =  try {
            val pm = context.packageManager
            val pi: PackageInfo = pm.getPackageInfo(context.packageName, 0)
            pi.applicationInfo.loadLabel(pm).toString()
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            ""
        }
        return TTAdConfig.Builder()
            .appId(appId) //
            .appName(appName)
//            .useTextureView(true) //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
            .titleBarTheme(TTAdConstant.TITLE_BAR_THEME_NO_TITLE_BAR) //落地页主题
            .allowShowNotify(true) //是否允许sdk展示通知栏提示,若设置为false则会导致通知栏不显示下载进度
            .debug(BuildConfig.DEBUG) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
            .directDownloadNetworkType() //允许直接下载的网络状态集合 ,不传递任何参数即可在所有网络下均有下载的二次弹窗。
            .supportMultiProcess(true) //是否支持多进程
            .build()
    }

}