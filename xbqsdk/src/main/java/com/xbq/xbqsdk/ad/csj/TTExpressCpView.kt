package com.xbq.xbqsdk.ad.csj

import android.app.Activity
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import com.bytedance.sdk.openadsdk.*
import com.xbq.xbqsdk.ad.utils.XbqAdConfig
import com.xbq.xbqsdk.ad.utils.px2dp
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean


/**
 * Author: liaohaiping
 * Time: 2021-04-13
 * Description:
 */
class TTExpressCpView : FrameLayout {
    companion object {
        const val TAG = "TTExpressCpView"
    }

    var mTTAd: TTNativeExpressAd? = null
    private var codeId: String = ""
    private var activityRef: WeakReference<Activity>? = null
    private var loading: AtomicBoolean = AtomicBoolean(false)

    private var mTTAdNative: TTAdNative? = null
        get() {
            if (field == null) {
                field = CsjAd.get()?.createAdNative(context)
            }
            return field
        }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context,
        attrs,
        defStyleAttr)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int,
    ) : super(context, attrs, defStyleAttr, defStyleRes)


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        loadAd()
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        when (visibility) {
            View.VISIBLE -> loadAd()
            View.GONE, View.INVISIBLE -> {
                mTTAd?.destroy()
                mTTAd = null
            }
        }
    }

    fun init(activity: Activity) {
        activityRef = WeakReference(activity)
        if (XbqAdConfig.isCsj() && XbqAdConfig.showBanner() && TTAdSdk.isInitSuccess()) {
            this.codeId = XbqAdConfig.bannerId
            loadAd()
        }
    }

    fun loadAd() {
        if (loading.get()) {
            return
        }
        if (mTTAd != null) {
            return
        }

        loading.set(true)

        val adSlot = AdSlot.Builder()
            .setCodeId(XbqAdConfig.tpId) //广告位id
            .setSupportDeepLink(true)
            .setAdCount(1) //请求广告数量为1到3条
            .setExpressViewAcceptedSize(600f, 600f) //期望模板广告view的size,单位dp
            .setAdLoadType(TTAdLoadType.LOAD) //推荐使用，用于标注此次的广告请求用途为预加载（当做缓存）还是实时加载，方便后续为开发者优化相关策略
            .build()

        mTTAdNative?.loadInteractionExpressAd(adSlot, object : TTAdNative.NativeExpressAdListener {
            //请求广告失败
            override fun onError(code: Int, message: String) {
                Log.d(TpDialog.TAG, "onError: $message")
            }

            //请求广告成功
            override fun onNativeExpressAdLoad(ads: List<TTNativeExpressAd>) {
                Log.d(TpDialog.TAG, "onNativeExpressAdLoad: ")
                if (ads.size > 0) {
                    mTTAd = ads[0]
                    bindAdListener(mTTAd!!)
                    mTTAd?.render()
                }
            }
        })
    }


    fun bindAdListener(ad: TTNativeExpressAd) {
        ad.setExpressInteractionListener(object : TTNativeExpressAd.AdInteractionListener {
            //广告关闭回调
            override fun onAdDismiss() {
                Log.d(TpDialog.TAG, "onAdDismiss: ")
            }

            //广告点击回调
            override fun onAdClicked(view: View?, type: Int) {
                Log.d(TpDialog.TAG, "onAdClicked: type=$type")
            }

            //广告展示回调
            override fun onAdShow(view: View?, type: Int) {
                Log.d(TpDialog.TAG, "onAdShow: type=$type")
            }

            //广告渲染失败回调
            override fun onRenderFail(view: View?, msg: String?, code: Int) {
                Log.d(TpDialog.TAG, "onRenderFail: msg=$msg, code=$code")
            }

            //广告渲染成功回调
            override fun onRenderSuccess(view: View?, width: Float, height: Float) {
                Log.d(TpDialog.TAG, "onRenderSuccess: ")
                //在渲染成功回调时展示广告，提升体验
//                mTTAd?.showInteractionExpressAd(activity)
//                fl.removeAllViews()
//                fl.addView(view)
                removeAllViews()
                addView(view)
            }
        })
    }


}