package com.xbq.xbqsdk.ad.csj

import android.app.Activity
import android.util.Log
import androidx.annotation.IntDef
import com.bytedance.sdk.openadsdk.AdSlot
import com.bytedance.sdk.openadsdk.TTAdConstant
import com.bytedance.sdk.openadsdk.TTAdLoadType
import com.bytedance.sdk.openadsdk.TTAdNative.RewardVideoAdListener
import com.bytedance.sdk.openadsdk.TTRewardVideoAd
import com.bytedance.sdk.openadsdk.TTRewardVideoAd.RewardAdInteractionListener
import com.xbq.xbqsdk.ad.utils.XbqAdConfig
import java.lang.ref.WeakReference

/**
 * Author: liaohaiping
 * Time: 2021-04-02
 * Description: 模版激励视频广告
 */

class VideoConstant{
    companion object{
        const val VERTICAL= TTAdConstant.VERTICAL
        const val HORIZONTAL= TTAdConstant.HORIZONTAL
    }
}

@IntDef(VideoConstant.VERTICAL, VideoConstant.HORIZONTAL)
@Retention(AnnotationRetention.SOURCE)
annotation class VideoOrientation

class TTRewardVideoHolder(@VideoOrientation val orientation: Int= VideoConstant.VERTICAL) {
    private var mttRewardVideoAd: TTRewardVideoAd? = null
//    private var codeId: String = ""
//    private var mOrientation: Int = TTAdConstant.VERTICAL
    private var mIsLoaded = false

    var onReward: (()->Unit)? = null

    private var activityRef: WeakReference<Activity>? = null

    companion object {
        const val TAG = "TTRewardVideoHolder"
    }


    fun loadAndShowAd(activity: Activity,onReward:(()->Unit)?) {
        activityRef = WeakReference(activity)
        this.onReward = onReward
        if(XbqAdConfig.isCsj() && XbqAdConfig.showJlsp()) {
            loadAd(XbqAdConfig.jlspId, this.orientation)
        }
    }


    private fun loadAd(codeId: String, orientation: Int) {
        //step4:创建广告请求参数AdSlot,具体参数含义参考文档
        val adSlot = AdSlot.Builder()
            .setCodeId(codeId)
//            .setRewardName("金币") //奖励的名称 选填
//            .setRewardAmount(3)  //奖励的数量 选填
//        模板广告需要设置期望个性化模板广告的大小,单位dp,激励视频场景，只要设置的值大于0即可
//        且仅是模板渲染的代码位ID使用，非模板渲染代码位切勿使用
            .setExpressViewAcceptedSize(500f, 500f)
            .setUserID("user1")//tag_id
//            .setMediaExtra("media_extra") //附加参数
            .setOrientation(orientation) //必填参数，期望视频的播放方向：TTAdConstant.HORIZONTAL 或 TTAdConstant.VERTICAL
            .setAdLoadType(TTAdLoadType.LOAD)
            .build();
        //step5:请求广告
        CsjAd.globalTTAdNative?.loadRewardVideoAd(adSlot, object : RewardVideoAdListener {
                var canReward = false  //是否该激励
                override fun onError(code: Int, message: String) {
                    Log.e(TAG, "Callback --> onError: $code, $message")
                }

                //视频广告加载后，视频资源缓存到本地的回调，在此回调后，播放本地视频，流畅不阻塞。
                override fun onRewardVideoCached() {
                    Log.e(TAG, "Callback --> onRewardVideoCached")
                    mIsLoaded = true
                    activityRef?.get()?.let {
                        showAd(it)
                    }
                }

                override fun onRewardVideoCached(p0: TTRewardVideoAd?) {
                    Log.e(TAG, "Callback --> onRewardVideoCached")
                    mIsLoaded = true
                    activityRef?.get()?.let {
                        showAd(it)
                    }
                }

                //视频广告的素材加载完毕，比如视频url等，在此回调后，可以播放在线视频，网络不好可能出现加载缓冲，影响体验。
                override fun onRewardVideoAdLoad(ad: TTRewardVideoAd) {
                    Log.e(
                        TAG,
                        "Callback --> onRewardVideoAdLoad 广告类型：${getAdType(ad.rewardVideoAdType)}"
                    )
                    mIsLoaded = false
                    mttRewardVideoAd = ad
                    mttRewardVideoAd!!.setRewardAdInteractionListener(object :
                        RewardAdInteractionListener {
                        override fun onAdShow() {
                            Log.d(TAG, "Callback --> rewardVideoAd show")
                        }

                        override fun onAdVideoBarClick() {
                            Log.d(TAG, "Callback --> rewardVideoAd bar click")
                        }

                        override fun onAdClose() {
                            Log.d(TAG, "Callback --> rewardVideoAd close")
                            mttRewardVideoAd=null
                            if(canReward){
                                onReward?.invoke()
                            }
                        }

                        //视频播放完成回调
                        override fun onVideoComplete() {
                            Log.d(TAG, "Callback --> rewardVideoAd complete")

                        }

                        override fun onVideoError() {
                            Log.e(TAG, "Callback --> rewardVideoAd error")
                        }

                        //视频播放完成后，奖励验证回调，rewardVerify：是否有效，rewardAmount：奖励数量，rewardName：奖励名称
                        override fun onRewardVerify(
                            rewardVerify: Boolean,
                            rewardAmount: Int,
                            rewardName: String,
                            errorCode: Int,
                            errorMsg: String
                        ) {
                            val logString = "verify:" + rewardVerify + " amount:" + rewardAmount +
                                    " name:" + rewardName + " errorCode:" + errorCode + " errorMsg:" + errorMsg
                            Log.e(TAG, "Callback --> $logString")
                            canReward =true
                        }

                        override fun onSkippedVideo() {
                            Log.e(TAG, "Callback --> rewardVideoAd has onSkippedVideo")
                        }
                    })
                }
            })
    }

    private fun showAd(activity: Activity) {
        if (mIsLoaded) {
            mttRewardVideoAd?.showRewardVideoAd(
                activity,
                TTAdConstant.RitScenes.CUSTOMIZE_SCENES,
                "scenes_test"
            )
            mttRewardVideoAd = null
        }
    }

    fun showToast(msg: String) {
        Log.d(TAG, msg)
    }

    private fun getAdType(type: Int): String? {
        when (type) {
            TTAdConstant.AD_TYPE_COMMON_VIDEO -> return "普通激励视频，type=$type"
            TTAdConstant.AD_TYPE_PLAYABLE_VIDEO -> return "Playable激励视频，type=$type"
            TTAdConstant.AD_TYPE_PLAYABLE -> return "纯Playable，type=$type"
        }
        return "未知类型+type=$type"
    }
}