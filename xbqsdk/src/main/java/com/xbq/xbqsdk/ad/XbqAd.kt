package com.xbq.xbqsdk.ad

import android.content.Context
import com.xbq.xbqsdk.ad.csj.CsjAd
import com.xbq.xbqsdk.ad.utils.XbqAdConfig

/**
 * Author: liaohaiping
 * Time: 2021-11-22
 * Description:
 */
object XbqAd {
    suspend fun init(context: Context, configs: Map<String, String?>){
        XbqAdConfig.setConfigData(configs)
        CsjAd.init(context.applicationContext)
    }
}