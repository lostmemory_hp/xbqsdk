package com.xbq.xbqsdk.ad.csj

import android.app.Activity
import android.app.AlertDialog
import android.view.LayoutInflater
import com.xbq.xbqsdk.R


/**
 * Author: liaohaiping
 * Time: 2021-04-08
 * Description: 退屏对话框
 */
class TpDialog {
    companion object {
        const val TAG = "TpDialog"
        fun show(activity: Activity, onExitApp: (() -> Unit)?) {
            val layoutInflater = LayoutInflater.from(activity)
            val rootView = layoutInflater.inflate(R.layout.xbqad_dlg_tp, null, false)
            rootView.findViewById<TTExpressCpView>(R.id.adview).init(activity)
            AlertDialog.Builder(activity)
                .setView(rootView)
                .setNegativeButton("取消") { dlg, which -> }
                .setPositiveButton("退出应用") { dlg, which -> onExitApp?.invoke() }
                .setCancelable(false)
                .show()
        }
    }


}