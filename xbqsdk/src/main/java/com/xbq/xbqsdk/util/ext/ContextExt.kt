package com.xbq.xbqsdk.util.ext

import android.content.Context

/**
 * Author: liaohaiping
 * Time: 2019-11-22
 * Description:
 */


/** Calls [Context.getSystemService] and casts the return value to [T]. */
inline fun <reified T> Context.systemService(name: String): T {
    return getSystemService(name) as T
}
