/**
 * Designed and developed by Aidan Follestad (@afollestad)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xbq.xbqsdk.util.ext

import com.google.gson.reflect.TypeToken


/**
 * Applies a block to the receiver if [condition] is true.
 */
fun <T> T.applyIf(
  condition: Boolean,
  block: T.() -> Unit
): T {
  if (condition) {
    return apply(block)
  }
  return this
}

fun <T> T.applyIf(
  condition: T.()->Boolean,
  block: T.() -> Unit
): T {
  if (condition()) {
    return apply(block)
  }
  return this
}

fun <T,R> T.letIf(
  condition: T.()->Boolean,
  block:  (T) -> R
): R? {
  if (condition()) {
    return let(block)
  }
  return null
}
fun <T,R> T.letIf(
  condition: Boolean,
  block:  (T) -> R
): R? {
  if (condition) {
    return let(block)
  }
  return null
}

inline fun <reified T> genericType() = object: TypeToken<T>() {}.type

