package com.xbq.xbqsdk.util.format;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatUtils {

    /**
     * 将秒数格式化为00:00:00的形式
     *
     * @param timeSeconds
     * @return
     */
    public static String formatTime(int timeSeconds) {
        int hour = timeSeconds / 3600;
        int minute = timeSeconds % 3600 / 60;
        int second = timeSeconds % 3600 % 60;
        DecimalFormat df = new DecimalFormat("00");
        return df.format(hour) + ":" + df.format(minute) + ":" + df.format(second);
    }

    /**
     * 将秒数格式化为xx小时xx分xx秒的形式
     *
     * @param timeSeconds
     * @return
     */
    public static String formatTimeChinses(int timeSeconds) {
        int hour = timeSeconds / 3600;
        int minute = timeSeconds % 3600 / 60;
        int second = timeSeconds % 3600 % 60;
        StringBuilder sb = new StringBuilder();
        if (hour > 0) {
            sb.append(hour + "小时");
        }
        if (minute > 0 || (second > 0 && hour > 0)) {
            sb.append(minute + "分");
        }
        if (second > 0) {
            sb.append(second + "秒");
        }
        return sb.toString();
    }


    /**
     * 将毫秒数格式化为00:00:00或00:00的形式
     *
     * @param timeMillis
     * @return
     */
    public static String formatDuration(long timeMillis) {
        int i = ((int) (timeMillis / 1000)) % 60;
        int i2 = (int) ((timeMillis / 60000) % 60);
        int i3 = (int) ((timeMillis / 3600000) % 24);
        if (i3 > 0) {
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", new Object[]{Integer.valueOf(i3), Integer.valueOf(i2), Integer.valueOf(i)});
        }
        return String.format(Locale.getDefault(), "%02d:%02d", new Object[]{Integer.valueOf(i2), Integer.valueOf(i)});
    }

    /**
     * @param fileSize
     * @return
     */
    public static String formatFileSize(long fileSize) {
        if (fileSize == 0) {
            return "0 MB";
        }
        DecimalFormat df = new DecimalFormat("0.00");
        if (fileSize > (1024 * 1024 * 1024)) {
            double gb = fileSize * 1.0 / (1024 * 1024 * 1024);
            return df.format(gb) + " GB";
        } else if (fileSize > (1024 * 1024)) {
            double mb = fileSize * 1.0 / (1024 * 1024);
            return df.format(mb) + " MB";
        } else if (fileSize > 1024) {
            double kb = fileSize * 1.0 / 1024;
            return df.format(kb) + " KB";
        }
        return fileSize + " Bytes";
    }


    /* renamed from: a */
    public static String formatFileSize(long recordDate, boolean z) {
        int i = z ? 1000 : 1024;
        if (recordDate < ((long) i)) {
            StringBuilder sb = new StringBuilder();
            sb.append(recordDate);
            sb.append(" B");
            return sb.toString();
        }
        double d = (double) recordDate;
        double d2 = (double) i;
        int log = (int) (Math.log(d) / Math.log(d2));
        StringBuilder sb2 = new StringBuilder();
        sb2.append((z ? "kMGTPE" : "KMGTPE").charAt(log - 1));
        sb2.append(z ? "" : "i");
        return String.format(Locale.getDefault(), "%.1f %sB", new Object[]{Double.valueOf(d / Math.pow(d2, (double) log)), sb2.toString()});
    }

    public static String formatYMDHM(long timeMills) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(new Date(timeMills));
    }

    public static String formatYMD(long timeMills) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date(timeMills));
    }

    public static String formatId(long id) {
        DecimalFormat df = new DecimalFormat("0000");
        return df.format(id);
    }

    public static String formatDownloadSpeed(long size) {
        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;

        if (size >= gb) {
            return String.format("%.1f GB", (float) size / gb);
        } else if (size >= mb) {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        } else if (size >= kb) {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        } else
            return String.format("%d B", size);
    }

    public static String formatRate(float rate) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        String format = decimalFormat.format(rate * 100);
        return format + "%";
    }

    public static String formatCount(int count) {
        if (count < 10000) {
            return count + "";
        } else {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            return decimalFormat.format(count / 10000.0);
        }
    }
    public static String formatCount(long count) {
        if (count < 10000) {
            return count + "";
        } else {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            return decimalFormat.format(count / 10000.0)+"万";
        }
    }
}
