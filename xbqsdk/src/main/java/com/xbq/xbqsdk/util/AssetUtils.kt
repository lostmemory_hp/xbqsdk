package com.xbq.xbqsdk.util

/**
 * Author: liaohaiping
 * Time: 2021-11-25
 * Description:
 */
fun getAssetFullPath(path:String):String{
    return if(path.startsWith("/")) {
         "file:////android_asset$path"
    }else{
        "file:////android_asset/$path"
    }
}