package com.xbq.xbqsdk.util.shell;

import com.blankj.utilcode.util.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShellCommandUtils {
    public static Map<String,String> getPropMap() {
        ShellUtils.CommandResult commandResult = ShellUtils.execCommand("/system/bin/getprop", false);
        Map<String,String> props = new HashMap<>();
        if (commandResult.result == 0) {
            String reg   ="\\[([^\\]]*)]\\:\\s*\\[([^\\]]*)]";
            Matcher matcher = Pattern.compile(reg).matcher(commandResult.successMsg);
            while (matcher.find()){
//                String group = matcher.group();
                props.put(matcher.group(1),matcher.group(2));
            }
            return props;
        } else {
            return new HashMap<>();
        }
    }

    public static List<String> getPropList() {
        ShellUtils.CommandResult commandResult = ShellUtils.execCommand("/system/bin/getprop", false);
        List<String> props = new ArrayList<>();
        if (commandResult.result == 0) {
            String reg   ="\\[([^\\]]*)]\\:\\s*\\[([^\\]]*)]";
            Matcher matcher = Pattern.compile(reg).matcher(commandResult.successMsg);
            while (matcher.find()){
                props.add(matcher.group());
            }
        }
        return props;
    }

    public static String getProp(String prop) {
        ShellUtils.CommandResult commandResult = ShellUtils.execCommand(String.format("/system/bin/getprop %s", prop), false);
        if (commandResult.result == 0) {
            return commandResult.successMsg;
        } else {
            return "";
        }
    }

    public static String getOsKernel(){
        ShellUtils.CommandResult commandResult = ShellUtils.execCommand("cat /proc/version", false);
        if (commandResult.result==0){
            String[] s = commandResult.successMsg.split(" ");
            if(s.length>2){
                return s[2];
            }
        }
        return "Unvailable";
    }

    /* renamed from: a */
    public static String formatKernel(String str) {
        Matcher matcher = Pattern.compile("Linux version (\\S+) \\((\\S+?)\\) (?:\\(gcc.+? \\)) (#\\d+) (?:.*?)?((Sun|Mon|Tue|Wed|Thu|Fri|Sat).+)").matcher(str);
        String str2 = "Unavailable";
        if (!matcher.matches() || matcher.groupCount() < 4) {
            return str2;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(matcher.group(1));
        String NEW_LINE = "\n";
        sb.append(NEW_LINE);
        sb.append(matcher.group(2));
        sb.append(" ");
        sb.append(matcher.group(3));
        sb.append(NEW_LINE);
        sb.append(matcher.group(4));
        return sb.toString();
    }


    public static List<String> getAllDangerPermissions() {
        String cmd = "pm list permissions -d -g";
        ShellUtils.CommandResult commandResult = ShellUtils.execCommand(cmd, false);
        if (commandResult.result == 0) {
            LogUtils.d(ShellCommandUtils.class.getName(), commandResult.successMsg);
            String pattern = "permission\\:(([a-z]+\\.)+permission\\.[A-Z_]+)";
            Pattern compile = Pattern.compile(pattern);
            Matcher matcher = compile.matcher(commandResult.successMsg);
            List<String> permissions = new ArrayList<>();
            while (matcher.find()) {
                permissions.add(matcher.group(1));
            }
            return permissions;
        } else {
            return new ArrayList<>();
        }
    }
    public static void screenshot(String filePath) {
        String cmd = "screencap -p "+filePath;
        ShellUtils.CommandResult commandResult = ShellUtils.execCommand(cmd, false);
        LogUtils.d("screenshot result:"+commandResult.result);
        LogUtils.d("screenshot success msg:"+commandResult.successMsg);
        LogUtils.d("screenshot error msg:"+commandResult.errorMsg);
    }

}