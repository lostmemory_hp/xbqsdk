package com.xbq.xbqsdk.util.activityresult

import android.util.Log
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.Semaphore

/**
 * Author: liaohaiping
 * Time: 2022-03-09
 * Description:
 */
class ActivityContractHelper<I, O>() : DefaultLifecycleObserver {
    var launcher: ActivityResultLauncher<I>? = null
    var data: O? = null
    var sem: Semaphore = Semaphore(0)

    fun register(activity: FragmentActivity, contract: ActivityResultContract<I, O>) {
        if(launcher==null) {
            launcher = activity.registerForActivityResult(contract) {
                transferData(it)
            }
        }
    }


    suspend fun startForResult(input: I, callback: ActivityResultCallback<O>) {
        launcher?.let {
            it.launch(input)
            val d = withContext(Dispatchers.IO) {
                sem.acquire()
                data
            }
            callback?.onActivityResult(d)
        }
    }


    suspend fun startForResult(
        activity: FragmentActivity,
        contract: ActivityResultContract<I, O>,
        input: I,
        callback: ActivityResultCallback<O>
    ) {
        register(activity,contract)
        launcher?.let {
            it.launch(input)
            val d = withContext(Dispatchers.IO) {
                sem.acquire()
                data
            }
            callback?.onActivityResult(d)
        }
    }


    private fun transferData(data: O) {
        this.data = data
        sem.release()
    }

    override fun onCreate(owner: LifecycleOwner) {
        Log.d("lhp", "onCreate: ")
        super.onCreate(owner)
    }
    override fun onDestroy(owner: LifecycleOwner) {
        launcher?.unregister()
        launcher = null
        Log.d("lhp", "onDestroy: ")
        super.onDestroy(owner)
    }
}