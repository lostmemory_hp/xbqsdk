package com.xbq.xbqsdk.util.permissions.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;

import static android.content.Context.POWER_SERVICE;

/**
 * 权限请求页适配，不同手机系统跳转到不同的权限请求页
 *
 * @author Donkor
 */
public class PermissionUtils {
    private static final String TAG = "Permission";


    /**
     * 是否有悬浮框权限
     *
     * @return
     */
    public static boolean canDrawOverlay() {
        return SettingsCompat.canDrawOverlays(Utils.getApp());
    }

    public static void requestDrawOverlayPermission(Activity activity, int requestCode) {
        //启动Activity让用户授权
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + AppUtils.getAppPackageName()));
//        activity.startActivityForResult(intent, requestCode);
        if (activity.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivityForResult(intent, requestCode);
        } else {
            LogUtils.e(TAG, "Intent is not available! " + intent);
        }
    }
    public static void requestDrawOverlayPermission(Fragment fragment, int requestCode) {
        //启动Activity让用户授权
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + AppUtils.getAppPackageName()));
//        fragment.startActivityForResult(intent, requestCode);
        if (fragment.getContext() != null) {
            if (fragment.getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                fragment.startActivityForResult(intent, requestCode);
            } else {
                LogUtils.e(TAG, "Intent is not available! " + intent);
            }
        } else {
            LogUtils.e(TAG, "Context is null! ");
        }
    }


    /**
     * 忽略电池优化
     */
    public static void ignoreBatteryOptimization(Context context,boolean showToast) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            PowerManager powerManager = (PowerManager) Utils.getApp().getSystemService(POWER_SERVICE);
            boolean hasIgnored = powerManager.isIgnoringBatteryOptimizations(AppUtils.getAppPackageName());
            //  判断当前APP是否有加入电池优化的白名单，如果没有，弹出加入电池优化的白名单的设置对话框。
            if (!hasIgnored) {
                Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("package:" + AppUtils.getAppPackageName()));
                context.startActivity(intent);
            }else{
                if(showToast) {
                    ToastUtils.showShort("已开启");
                }
            }
        }
    }

}