package com.xbq.xbqsdk.util.click

import android.view.View

/**
 * Author: liaohaiping
 * Time: 2020-11-06
 * Description:
 */
fun View.fiveClick(callback:(view: View)->Unit){
    var clickCount=0
    val CLICK_SPAN = 200 //200毫秒间隔
    var lastClickTime = System.currentTimeMillis()-60000
    this.setOnClickListener{
        clickCount++
        val now = System.currentTimeMillis()
        if(now-lastClickTime<CLICK_SPAN){
            if(clickCount>=5){
                callback(this)
                clickCount=0
            }
        }else{
            clickCount=1
        }
        lastClickTime=now
    }
}