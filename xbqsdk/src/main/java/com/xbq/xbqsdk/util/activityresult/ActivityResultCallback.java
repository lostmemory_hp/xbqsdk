package com.xbq.xbqsdk.util.activityresult;

import android.content.Intent;

/**
 * author: cs丶
 * date: 2019/4/3
 * description:
 */
@FunctionalInterface
public interface ActivityResultCallback {
  void onResult(int resultCode, Intent intent);
}