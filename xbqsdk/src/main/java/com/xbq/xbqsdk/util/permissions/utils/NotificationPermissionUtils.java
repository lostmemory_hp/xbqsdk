package com.xbq.xbqsdk.util.permissions.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.NotificationManagerCompat;


/**
 * Author: liaohaiping
 * Time: 2019-10-25
 * Description:
 */
public class NotificationPermissionUtils {

    public static void checkNotificationPermission(Context ctx) {
        Boolean isEnabled = NotificationManagerCompat.from(ctx).areNotificationsEnabled();
        if (!isEnabled) {
            //未打开通知
            new AlertDialog.Builder(ctx)
                    .setTitle("提示")
                    .setMessage("请在“通知”中打开通知权限")
                    .setPositiveButton("去设置", (dialog, which) -> {
                        dialog.dismiss();
                        goNotificationPermissionSetting(ctx);
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private static void goNotificationPermissionSetting(Context ctx) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("android.provider.extra.APP_PACKAGE", ctx.getPackageName());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {  //5.0
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", ctx.getPackageName());
            intent.putExtra("app_uid", ctx.getApplicationInfo().uid);
            ctx.startActivity(intent);
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {  //4.4
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + ctx.getPackageName()));
        } else if (Build.VERSION.SDK_INT >= 15) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", ctx.getPackageName(), null));
        }
        ctx.startActivity(intent);
    }
}
