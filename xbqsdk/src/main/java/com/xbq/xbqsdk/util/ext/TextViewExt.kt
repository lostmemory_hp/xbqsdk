package com.xbq.xbqsdk.util.ext

import android.graphics.Paint
import android.text.TextUtils
import android.widget.EditText
import android.widget.TextView
import java.math.BigDecimal
import java.sql.Timestamp
import java.text.SimpleDateFormat

/**
 * Author: liaohaiping
 * Time: 2021-11-26
 * Description:
 */


/**
 * 添加删除线
 * @receiver TextView
 * @return String
 */
fun TextView.addDeleteLine(){
    this.paintFlags =  this.paintFlags.or(Paint.STRIKE_THRU_TEXT_FLAG)
}

/**
 * 取消删除线
 * @receiver TextView
 */
fun TextView.removeDeleteLine(){
    this.paintFlags =  this.paintFlags.xor(Paint.STRIKE_THRU_TEXT_FLAG.inv())
}


fun TextView.value() = this.text.trim().toString()
fun TextView.intValue() =  Integer.valueOf(this.text.trim().toString())
fun TextView.decimalValue() =  BigDecimal(this.text.trim().toString())
fun TextView.timeValue() : Timestamp? {
    val value = this.value()
    if (value.isEmpty()) {
        return null
    }
    return SimpleDateFormat("yyyy-MM-dd HH:mm").parse(value)?.run { Timestamp(this.time) }
}
