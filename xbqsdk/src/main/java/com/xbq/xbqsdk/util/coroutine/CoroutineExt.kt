package com.xbq.xbqsdk.util.coroutine

import com.blankj.utilcode.util.LogUtils
import com.umeng.umcrash.UMCrash
import kotlinx.coroutines.*

/**
 * Author: liaohaiping
 * Time: 2021-10-09
 * Description:
 */


suspend fun <T> await(block: suspend CoroutineScope.() -> T): T {
    val runBlock: suspend CoroutineScope.() -> T = {
        try {
            block()
        } catch (e: Exception) {
            LogUtils.e(e)
            UMCrash.generateCustomLog(e,"await");
            throw Exception(e)
        }
    }
    return withContext(Dispatchers.IO, runBlock)
}

fun ui(block: suspend CoroutineScope.() -> Unit):Job {
    val runBlock: suspend CoroutineScope.() -> Unit = {
        try {
            block()
        } catch (e: Exception) {
            LogUtils.e(e)
            UMCrash.generateCustomLog(e,"ui");
        }
    }
    return GlobalScope.launch(Dispatchers.Main, CoroutineStart.DEFAULT, runBlock)
}

