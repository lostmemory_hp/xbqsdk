package com.xbq.xbqsdk.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimeUtils {

    public static final String[] DAY_OF_WEEKS = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};

    /**
     * 当前时间
     */
    public static Timestamp now() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     *
     * @param time
     * @param duration
     * @param field the calendar field.
     * @return
     */
    public static Date getTimeAfter(Date time, int duration, int field) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.add(field, duration);
        return calendar.getTime();
    }

    public static Date getTimeAfter(Date time, int duration, TimeUnit unit) {
        return new Date(time.getTime()+unit.toMillis(duration));
    }

    public static Date getTimeAfterNow(long duration, TimeUnit unit) {
        return new Date(System.currentTimeMillis() + unit.toMillis(duration));
    }

    public static Date getTimeAfterNow(int duration, int field) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.add(field, duration);
        return calendar.getTime();
    }


    /**
     * 得到今天的日期
     *
     * @return
     */
    public static Date today() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static String getDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int index = calendar.get(Calendar.DAY_OF_WEEK);
        return DAY_OF_WEEKS[index-1];
    }

    /**
     * @param time
     * @return
     */
    public static boolean isAfterNow(long time) {
        return System.currentTimeMillis() < time;
    }

    /**
     * @param time
     * @return
     */
    public static boolean isAfterNow(Timestamp time) {
        return System.currentTimeMillis() < time.getTime();
    }

    /**
     * @param time
     * @return
     */
    public static boolean isAfterNow(Date time) {
        return System.currentTimeMillis() < time.getTime();
    }

    /**
     * @param time
     * @return
     */
    public static boolean isBeforeNow(long time) {
        return System.currentTimeMillis() > time;
    }

    /**
     * @param time
     * @return
     */
    public static boolean isBeforeNow(Timestamp time) {
        return System.currentTimeMillis() > time.getTime();
    }

    /**
     * @param time
     * @return
     */
    public static boolean isBeforeNow(Date time) {
        return System.currentTimeMillis() > time.getTime();
    }

    public static boolean isToday(Timestamp time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        return simpleDateFormat.format(time)
                .equals(simpleDateFormat.format(new Date(System.currentTimeMillis())));
    }

    public static String parse(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }


    public static String todayYMD() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date(System.currentTimeMillis()));
    }
}
