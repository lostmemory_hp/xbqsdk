package com.xbq.xbqsdk.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * Author: liaohaiping
 * Time: 2020-11-04
 * Description:
 */
public class DpiUtils {
    /**
     * @return :
     * @Description:匹配得到手机的dpi
     * @author 作者 :likun
     * @date 创建时间：2016/6/24 16:07
     * @parameter :
     */
    public static String getDpi(Context context) {
        //获取x、y的dpi,得到的数据，基本上是一致的
        WindowManager wm = (WindowManager) (context.getSystemService(Context.WINDOW_SERVICE));
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        float xdpi = dm.xdpi;
//        float ydpi = context.getResources().getDisplayMetrics().xdpi;
        String dpi = "";

        if (xdpi >= 0 && xdpi < DisplayMetrics.DENSITY_LOW) {
            dpi = "ldpi";
        } else if (xdpi >= DisplayMetrics.DENSITY_LOW && xdpi < DisplayMetrics.DENSITY_MEDIUM) {
            dpi = "mdpi";
        } else if (xdpi >= DisplayMetrics.DENSITY_MEDIUM && xdpi < DisplayMetrics.DENSITY_HIGH) {
            dpi = "hdpi";
        } else if (xdpi >= DisplayMetrics.DENSITY_HIGH && xdpi < DisplayMetrics.DENSITY_XHIGH) {
            dpi = "xhdpi";
        } else if (xdpi >= DisplayMetrics.DENSITY_XHIGH && xdpi < DisplayMetrics.DENSITY_XXHIGH) {
            dpi = "xxhdpi";
        } else if (xdpi >= DisplayMetrics.DENSITY_XXHIGH && xdpi < DisplayMetrics.DENSITY_XXXHIGH) {
            dpi = "xxxhdpi";
        } else {
            dpi = "未知dpi";
        }
        return dpi;
    }

    public static String getScreenResolution(Context context){
        //获取x、y的dpi,得到的数据，基本上是一致的
        WindowManager wm = (WindowManager) (context.getSystemService(Context.WINDOW_SERVICE));
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels+"*"+dm.heightPixels;
    }

}
