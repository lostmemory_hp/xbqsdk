package com.xbq.xbqsdk.util.permissions.utils;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.RomUtils;
import com.blankj.utilcode.util.Utils;

/**
 * Author: liaohaiping
 * Time: 2020-11-06
 * Description: 自启动管理页面
 */
public class PermissionAutoStartUtils {

    /**
     * 跳转到自启动管理页面
     */
    public static void goAutoRunPage() {
        if (RomUtils.isHuawei()) {
            goHuawei();
        } else if (RomUtils.isXiaomi()) {
            goXiaomi();
        } else if (RomUtils.isSamsung()) {
            goSamsung();
        } else if (RomUtils.isMeizu()) {
            goMeizu();
        } else if (RomUtils.isOppo()) {
            goOppo();
        } else if (RomUtils.isLeeco()) {
            goLetv();
        } else if (RomUtils.isVivo()) {
            goViVO();
        } else if(RomUtils.isOneplus()){
            goOnePlus();
        }else if(RomUtils.isZte()){
            goZte();
        } else if(RomUtils.isGionee()){
            goGionee();
        } else if(RomUtils.isCoolpad()){
            goCoolpad();
        } else if(RomUtils.isSmartisan()){
            goSmartisan();
        }else if(RomUtils.is360()){
            go360();
        }
        else {
            jumpAPPInfo();
        }
    }

    //360
    private static void go360() {
        String[] compns=new String[]{
                "com.yulong.android.coolsafe/.ui.activity.autorun.AutoRunListActivity",
                "com.yulong.android.coolsafe"
        };
        startActivityByComponentNames(compns);
    }

    //锤子
    private static void goSmartisan() {
        String[] compns=new String[]{
                "com.smartisanos.security/.invokeHistory.InvokeHistoryActivity",
                "com.smartisanos.security"
        };
        startActivityByComponentNames(compns);
    }

    //酷派
    private static void goCoolpad() {
        String[] compns=new String[]{
                "com.yulong.android.security/com.yulong.android.seccenter.tabbarmain",
                "com.yulong.android.security"
        };
        startActivityByComponentNames(compns);
    }

    //金立
    private static void goGionee() {
        String[] compns=new String[]{
                "com.gionee.softmanager/.MainActivity",
                "com.gionee.softmanager"
        };
        startActivityByComponentNames(compns);
    }

    private static void goZte() {
        String[] compns = new String[]{
                "com.zte.heartyservice/.autorun.AppAutoRunManager",
                "com.zte.heartyservice"
        };
        startActivityByComponentNames(compns);
    }

    private static void goOnePlus() {
        String[] compNs = new String[]{
                "com.oneplus.security/.chainlaunch.view.ChainLaunchAppListActivity",
                "com.oneplus.security"
        };
        startActivityByComponentNames(compNs);
    }


    private static void goViVO() {
        String[] compNs = new String[]{
                "com.iqoo.secure/.ui.phoneoptimize.BgStartUpManager",
                "com.iqoo.secure/.safeguard.PurviewTabActivity",
                "com.vivo.permissionmanager/.activity.BgStartUpManagerActivity",
                "com.iqoo.secure",
                "com.vivo.permissionmanager"
        };
        startActivityByComponentNames(compNs);
    }

    private static void goLetv() {
        Intent intent = new Intent();
        try {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //跳自启动管理
            intent.setAction("com.letv.android.permissionautoboot");
            Utils.getApp().startActivity(intent);
        } catch (Exception e) {
            String[] compns = new String[]{
                    "com.letv.android.letvsafe/.AutobootManageActivity",
                    "com.letv.android.letvsafe/.BackgroundAppManageActivity",//应用保护
                    "com.letv.android.letvsafe"
            };
            startActivityByComponentNames(compns);
        }

    }

    private static void goOppo() {
        String[] compns = new String[]{
                "com.coloros.safecenter/.startupapp.StartupAppListActivity",
                "com.coloros.safecenter/.permission.startup.StartupAppListActivity",
                "com.oppo.safe/.permission.startup.StartupAppListActivity",
                "com.coloros.oppoguardelf/com.coloros.powermanager.fuelgaue.PowerUsageModelActivity",
                "com.coloros.safecenter/com.coloros.privacypermissionsentry.PermissionTopActivity",
                "com.coloros.safecenter",
                "com.oppo.safe",
                "com.coloros.oppoguardelf"
        };
        startActivityByComponentNames(compns);

    }

    /**
     * 按顺序尝试跳转,如果都失败，最后 jumpAPPInfo()
     *
     * @param componentNames
     */
    private static void startActivityByComponentNames(String... componentNames) {
        boolean success = false;
        for (String compName : componentNames) {
            try {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ComponentName componentName = ComponentName.unflattenFromString(compName);
                intent.setComponent(componentName);
                Utils.getApp().startActivity(intent);
                success = true;
                break;
            } catch (Exception ex) {
                LogUtils.w("跳转【" + compName + "】失败");
            }
        }
        if (success == false) {
            jumpAPPInfo();
        }
    }

    /**
     * 按顺序尝试跳转,如果都失败，最后 jumpAPPInfo()
     *
     * @param componentNames
     */
    private static void startActivityByComponentNames(ComponentName... componentNames) {
        boolean success = false;
        for (ComponentName compName : componentNames) {
            try {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setComponent(compName);
                Utils.getApp().startActivity(intent);
                success = true;
                break;
            } catch (Exception ex) {
                LogUtils.w("跳转【" + compName + "】失败");
            }
        }
        if (success == false) {
            jumpAPPInfo();
        }
    }


    private static void goMeizu() {
        String[] compNs = {
                "com.meizu.safe/.permission.SmartBGActivity",
                "com.meizu.safe/.permission.PermissionMainActivity",
                "com.meizu.safe/.permission.SmartBGActivity",//Flyme7.3.0(7.1.2)
                "com.meizu.safe"
        };
        startActivityByComponentNames(compNs);
    }

    private static void goSamsung() {
        ComponentName[] compNs = new ComponentName[]{
                ComponentName.unflattenFromString("com.samsung.android.sm/.app.dashboard.SmartManagerDashBoardActivity"),
                new ComponentName("com.samsung.android.sm_cn", "com.samsung.android.sm.ui.ram.AutoRunActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm_cn/com.samsung.android.sm.ui.ram.AutoRunActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm_cn/com.samsung.android.sm.ui.appmanagement.AppManagementActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm_cn/com.samsung.android.sm.ui.cstyleboard.SmartManagerDashBoardActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm_cn/.ui.ram.RamActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm_cn/.app.dashboard.SmartManagerDashBoardActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm/com.samsung.android.sm.ui.ram.AutoRunActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm/com.samsung.android.sm.ui.appmanagement.AppManagementActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm/com.samsung.android.sm.ui.cstyleboard.SmartManagerDashBoardActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm/.ui.ram.RamActivity"),
                ComponentName.unflattenFromString("com.samsung.android.lool/com.samsung.android.sm.ui.battery.BatteryActivity"),
                ComponentName.unflattenFromString("com.samsung.android.sm_cn"),
                ComponentName.unflattenFromString("com.samsung.android.sm")
        };
        startActivityByComponentNames(compNs);
    }

    private static void goXiaomi() {
        ComponentName[] compNs = {
                new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")
        };
        startActivityByComponentNames(compNs);
    }

    private static void jumpAPPInfo() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", AppUtils.getAppPackageName(), null);
        intent.setData(uri);
        Utils.getApp().startActivity(intent);
    }

    private static void goHuawei() {
        String[] compNs = new String[]{
                "com.huawei.systemmanager/.startupmgr.ui.StartupNormalAppListActivity",//EMUI9.1.0(方舟,9.0)
                "com.huawei.systemmanager/.appcontrol.activity.StartupAppControlActivity",
                "com.huawei.systemmanager/.optimize.process.ProtectActivity",
                "com.huawei.systemmanager/.optimize.bootstart.BootStartActivity",
                "com.huawei.systemmanager/com.huawei.permissionmanager.ui.MainActivity",
                "com.huawei.systemmanager"
        };
        startActivityByComponentNames(compNs);
    }
}
