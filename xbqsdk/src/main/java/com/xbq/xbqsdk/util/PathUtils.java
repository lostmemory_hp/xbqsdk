package com.xbq.xbqsdk.util;

import android.content.Context;
import android.os.Environment;

import com.blankj.utilcode.util.AppUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author: liaohaiping
 * Time: 2019-06-17
 * Description:
 */
public class PathUtils {
    public static File getPackageDir() {
        File f = new File(Environment.getExternalStorageDirectory(), AppUtils.getAppPackageName());
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }

    public static String newFileName(String fileExtension) {
        SimpleDateFormat yyyyMMddHHmmssSSS = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return yyyyMMddHHmmssSSS.format(new Date()) + String.valueOf(System.nanoTime() % 1000) + fileExtension;
    }


    public static File newPackageDirFile(String fileType, String fileExtension) {
        File f = new File(getPackageDir(), fileType + File.separator + newFileName(fileExtension));
        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        return f;
    }

    public static File getPackageDir(String fileType) {
        File f = new File(getPackageDir(), fileType);
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }



    public static File getTempDir(Context context) {
        File externalFilesDir = context.getExternalFilesDir(null);
        File download = new File(externalFilesDir, "temp");
        download.mkdirs();
        return download;
    }

    public static File newTempFile(Context context, String fileExtension) {
        File externalFilesDir = context.getExternalFilesDir(null);
        File download = new File(externalFilesDir, "temp");
        if (!download.exists()) {
            download.mkdirs();
        }
        String fileName = newFileName(fileExtension);
        return new File(download, fileName);
    }

}
