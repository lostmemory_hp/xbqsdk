package com.xbq.xbqsdk.util.coroutine

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.umeng.umcrash.UMCrash
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext


fun LifecycleOwner.launch(
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
): Job {
    val runBlock: suspend CoroutineScope.() -> Unit = {
        try {
            block()
        } catch (e: Exception) {
            LogUtils.e(e)
            UMCrash.generateCustomLog(e,"LifecycleOwner.launch");
            ToastUtils.showShort(e.message)
        }
    }
    return lifecycleScope.launch(context, start, runBlock)
}

fun <T> LifecycleOwner.async(
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> T
): Deferred<T> {
    val runBlock: suspend CoroutineScope.() -> T = {
        try {
            block()
        } catch (e: Exception) {
            LogUtils.e(e)
            UMCrash.generateCustomLog(e,"LifecycleOwner.async");
            ToastUtils.showShort(e.message)
            throw Exception(e)
        }
    }
    return lifecycleScope.async(context, start, runBlock)
}