package com.xbq.xbqsdk.util.ext

import androidx.appcompat.widget.PopupMenu
import java.lang.reflect.Method


fun PopupMenu.setForceShowIcon(){
    try {
        val fields = this::class.java.declaredFields
        for (field in fields) {
            if ("mPopup" == field.name) {
                field.isAccessible = true
                val menuPopupHelper = field.get(this)
                val classPopupHelper =
                    Class.forName(menuPopupHelper.javaClass.name)
                val setForceIcons: Method = classPopupHelper.getMethod(
                    "setForceShowIcon",
                    Boolean::class.javaPrimitiveType
                )
                setForceIcons.invoke(menuPopupHelper, true)
                break
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}