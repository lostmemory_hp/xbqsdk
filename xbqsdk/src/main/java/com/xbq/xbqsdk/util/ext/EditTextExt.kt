package com.xbq.xbqsdk.util.ext

import android.widget.EditText
import com.blankj.utilcode.util.ToastUtils
import java.util.regex.Pattern

/**
 * Author: liaohaiping
 * Time: 2021-11-26
 * Description:
 */

fun EditText.isEmpty(): Boolean {
    return text.isEmpty()
}

fun EditText.validatePhoneNumber(phone: String): Boolean {
    if (validateEmpty("请输入电话号码")) {
        val patter = Pattern.compile("^1[0-9]{10}\$")
        val isphone = patter.matcher(phone).find()
        if (!isphone) {
            ToastUtils.showShort("请输入正确的电话号码")
        }
        return isphone
    }
    return false
}

fun EditText.validateEmpty(errMsg: String): Boolean {
    if (this.value()?.isEmpty()) {
        setError(errMsg)
        return false
    }else{
        setError(null)
        return true
    }
}

fun EditText.validateSame(item2: EditText, errMsg: String): Boolean {
    val isSame = item2.value().equals(item2)
    if (!isSame) {
        ToastUtils.showShort(errMsg)
    }
    return isSame
}
