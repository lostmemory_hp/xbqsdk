//package com.xbq.xbqsdk.core.glide
//
//import android.content.Context
//import android.graphics.Bitmap
//import android.graphics.drawable.Drawable
//import android.view.View
//import android.widget.ImageView
//import com.blankj.utilcode.util.LogUtils
//import com.bumptech.glide.load.Transformation
//import com.bumptech.glide.load.engine.DiskCacheStrategy
//import com.xbq.xbqsdk.R
//import com.xbq.xbqsdk.util.ext.applyIf
//import jp.wasabeef.glide.transformations.BlurTransformation
//import java.io.IOException
//
//
//open class ImageLoadUtils() {
//    companion object{
//        private lateinit var context: Context
//        fun init(ctx: Context){
//            context = ctx
//        }
//        val blurTransformation = BlurTransformation(80)
//
//        fun loadImage(
//            url: String,
//            imageView: ImageView,
//            transformation: Transformation<Bitmap>?
//        ) {
//            if (url.isEmpty()) return
//            if (url.isBlank()) return
//            if (url.startsWith("assets://")) {
//                try {
//                    // get input stream
//                    val assetPath = url.substring("assets://".length)
//                    val fis = context.assets.open(assetPath)
//                    // load image as Drawable
//                    val drawable = Drawable.createFromStream(fis, null)
//                    GlideApp.with(context)
//                        .load(drawable)
//                        .applyIf(transformation != null) {
//                            transform(transformation)
//                        }
//                        .dontAnimate()
//                        .skipMemoryCache(false)
//                        .applyIf(imageView.drawable==null){
//                            placeholder(R.drawable.ic_image_place_holder)
//                        }
//                        .applyIf(imageView.drawable!=null){
//                            placeholder(imageView.drawable)
//                        }
//                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                        .into(imageView)
//                    fis.close()
//                } catch (ex: IOException) {
//                    LogUtils.e(ex)
//                    return
//                }
//            } else if (url.startsWith("drawable://")) {
//                var drawablePath = url.substring("drawable://".length)
//                val drawbaleId =
//                    context.resources.getIdentifier(drawablePath, "drawable", context.packageName)
//                GlideApp.with(context)
//                    .load(drawbaleId)
//                    .applyIf(transformation != null) {
//                        transform(transformation)
//                    }
//                    .dontAnimate()
//                    .skipMemoryCache(false)
//                    .applyIf(imageView.drawable==null){
//                        placeholder(R.drawable.ic_image_place_holder)
//                    }
//                    .applyIf(imageView.drawable!=null){
//                        placeholder(imageView.drawable)
//                    }
//                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .into(imageView)
//            } else if (url.startsWith("http://") || url.startsWith("https://")) {
//                GlideApp.with(context)
//                    .load(url)
//                    .applyIf(transformation != null) {
//                        transform(transformation)
//                    }
//                    .dontAnimate()
//                    .skipMemoryCache(false)
//                    .applyIf(imageView.drawable==null){
//                        placeholder(R.drawable.ic_image_place_holder)
//                    }
//                    .applyIf(imageView.drawable!=null){
//                        placeholder(imageView.drawable)
//                    }
//                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .into(imageView)
//            }else{
//                GlideApp.with(context)
//                    .load(url)
//                    .applyIf(transformation != null) {
//                        transform(transformation)
//                    }
//                    .dontAnimate()
//                    .skipMemoryCache(true)
//                    .applyIf(imageView.drawable==null){
//                        placeholder(R.drawable.ic_image_place_holder)
//                    }
//                    .applyIf(imageView.drawable!=null){
//                        placeholder(imageView.drawable)
//                    }
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .into(imageView)
//            }
//
//        }
//
//        fun loadImage(url: String, imageView: ImageView) {
//            loadImage(url,imageView, null)
//        }
//
//
//        fun loadBlurredMusicBackground(url: String,imageView: ImageView){
//            loadImage(url, imageView, blurTransformation)
//        }
//
//
//
//        fun loadImageToBackground(
//            url: String,
//            view: View,
//            transformation: Transformation<Bitmap>?
//        ) {
//            if (url.isEmpty()) return
//            if (url.isBlank()) return
//            val target = DrawableBackgroundTarget(view)
//            if (url.startsWith("assets://")) {
//                try {
//                    // get input stream
//                    val assetPath = url.substring("assets://".length)
//                    val fis = context.assets.open(assetPath)
//                    // load image as Drawable
//                    val drawable = Drawable.createFromStream(fis, null)
//                    GlideApp.with(context)
//                        .load(drawable)
//                        .applyIf(transformation != null) {
//                            transform(transformation)
//                        }
//                        .dontAnimate()
//                        .skipMemoryCache(false)
//                        .placeholder(target.currentDrawable)
//                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                        .into(target)
//                    fis.close()
//                } catch (ex: IOException) {
//                    LogUtils.e(ex)
//                    return
//                }
//            } else if (url.startsWith("drawable://")) {
//                var drawablePath = url.substring("drawable://".length)
//                val drawbaleId =
//                    context.resources.getIdentifier(drawablePath, "drawable", context.packageName)
//                GlideApp.with(context)
//                    .load(drawbaleId)
//                    .applyIf(transformation != null) {
//                        transform(transformation)
//                    }
//                    .dontAnimate()
//                    .skipMemoryCache(false)
//                    .placeholder(target.currentDrawable)
//                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .into(target)
//            } else if (url.startsWith("http://") || url.startsWith("https://")) {
//                GlideApp.with(context)
//                    .load(url)
//                    .applyIf(transformation != null) {
//                        transform(transformation)
//                    }
//                    .dontAnimate()
//                    .skipMemoryCache(false)
//                    .placeholder(target.currentDrawable)
//                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .into(target)
//            }
//        }
//
//    }
//}