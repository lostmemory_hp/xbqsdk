package com.xbq.xbqsdk.core.ui.account

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.component.immersion.ImmersionFragment
import com.xbq.xbqsdk.databinding.FragmentRegisterBinding
import com.xbq.xbqsdk.core.event.UserInfoChanged
import com.xbq.xbqsdk.core.ui.ReadPrivacyTextSetup
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.util.click.onDebouncedClick
import com.xbq.xbqsdk.util.coroutine.launch
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
@AndroidEntryPoint
class RegisterFragment : ImmersionFragment<FragmentRegisterBinding>() {
    @Inject
    lateinit var userRepository: UserRepository

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.let {
            ReadPrivacyTextSetup(it.ckbReadPrivacyAlready).setup()
            it.btnGoLogin.setOnClickListener { returnLoginView() }
            it.btnRegister.onDebouncedClick { doRegister() }
            it.etUserName.addTextChangedListener { setRegisterButtonEnable() }
            it.etPassword.addTextChangedListener { setRegisterButtonEnable() }
            it.etPasswordConfirm.addTextChangedListener { setRegisterButtonEnable() }
            it.ckbReadPrivacyAlready.setOnCheckedChangeListener { buttonView, isChecked -> setRegisterButtonEnable() }
        }
    }

    private fun returnLoginView() {
        (requireActivity() as? LoginActivity)?.showLogin()
    }


    private fun doRegister() {
        binding.let {
            val userName = it.etUserName.text.toString().trim()
            val password = it.etPassword.text.toString().trim()
            launch {
                val res = userRepository.register(userName, password)
                if (res.success()) {
                    val loginRes = userRepository.login(userName, password)
                    if (loginRes.success()) {
                        EventBus.getDefault().post(UserInfoChanged())
                        ToastUtils.showShort("注册并登录成功")
                        requireActivity().let {
                            it.setResult(Activity.RESULT_OK)
                            it.finish()
                        }
                    } else {
                        ToastUtils.showShort("注册成功，请登录。")
                        returnLoginView()
                    }
                } else {
                    ToastUtils.showShort(res.message)
                }
            }
        }
    }
    private fun setRegisterButtonEnable() {
        binding.let {
            val userName = it.etUserName.text.toString().trim()
            val password = it.etPassword.text.toString().trim()
            val password2 = it.etPasswordConfirm.text.toString().trim()
            it.btnRegister.isEnabled = userName.isNotBlank()
                    && password.isNotBlank()
                    && password == password2
                    && binding.ckbReadPrivacyAlready.isChecked
        }

    }
}