package com.xbq.xbqsdk.core.ui.account

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import com.blankj.utilcode.util.ToastUtils
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.xbq.xbqsdk.component.dialog.XbqDialogFragment
import com.xbq.xbqsdk.databinding.DlgDeleteAccountBinding
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.util.click.onDebouncedClick
import com.xbq.xbqsdk.util.coroutine.launch
import com.xbq.xbqsdk.util.ext.value
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
@AndroidEntryPoint
class DeleteAccountFragment : XbqDialogFragment<DlgDeleteAccountBinding>() {
    @Inject
    lateinit var userRepository:UserRepository
    var onDeleteAccountSuccess: (() -> Unit)? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false
        binding.btnCancel.setOnClickListener { dismiss() }
        binding.btnDeleteAccount.onDebouncedClick { deleteAccount() }
        binding.etPassword.addTextChangedListener {
            binding.btnDeleteAccount.isEnabled = it?.isNotEmpty()?:false
        }
    }

    override fun onDestroy() {
        onDeleteAccountSuccess = null
        super.onDestroy()
    }

    fun deleteAccount(){
        val pwd = binding.etPassword.value()
        launch {
            val res = userRepository.deleteSelfAccount(pwd)
            if (res.success()){
                dismiss()
                onDeleteAccountSuccess?.invoke()
            }else{
//                ToastUtils.showShort(res.message)
                Snackbar.make(binding.root,res.message, BaseTransientBottomBar.LENGTH_SHORT).show()
            }

        }
    }
}