package com.xbq.xbqsdk.core.ext

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.core.ui.account.LoginActivity
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.util.activityresult.StartActivityForResultHelper

/**
 * Author: liaohaiping
 * Time: 2021-11-19
 * Description:
 */

fun Activity.ensureLogin(userCache: UserCache, onSuccess: (() -> Unit)?) {
    if (userCache.isLogin()) {
        onSuccess?.invoke()
    }else {
        val intent = Intent(this, XbqSdk.getLoginActivity())
        StartActivityForResultHelper.startActivityForResult(this, intent) { resultCode, data ->
            if (resultCode == Activity.RESULT_OK) {
                onSuccess?.invoke()
            }
        }
    }
}

fun Fragment.ensureLogin(userCache: UserCache, onSuccess: (() -> Unit)?) {
    if (userCache.isLogin()) {
        onSuccess?.invoke()
    }else {
        val intent = Intent(requireContext(), XbqSdk.getLoginActivity())
        StartActivityForResultHelper.startActivityForResult(this, intent) { resultCode, data ->
            if (resultCode == Activity.RESULT_OK) {
                onSuccess?.invoke()
            }
        }
    }
}
