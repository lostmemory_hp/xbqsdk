package com.xbq.xbqsdk.core.pay

import androidx.activity.ComponentActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.net.common.dto.ConfirmOrderDto
import com.xbq.xbqsdk.net.common.vo.ConfirmOrderVO
import com.xbq.xbqsdk.net.common.vo.ProductVO
import com.xbq.xbqsdk.net.constants.PayTypeEnum
import com.xbq.xbqsdk.util.coroutine.launch
import javax.inject.Inject


/**
 * Author: liaohaiping
 * Time: 2021-12-10
 * Description:
 */
class XbqPayUtils @Inject constructor(val wxPayUtils: WxPayUtils, val aliPayUtils: AliPayUtils) :
    DefaultLifecycleObserver {
    var onPaySuccessCallback: PaySuccessCallback = null
        set(value) {
            field = value
            wxPayUtils.onPaySuccessCallback = value
            aliPayUtils.onPaySuccessCallback = value
        }



    //返回值表示是否可以继续后面的步骤
    var onConfirmOrderSuccess: ConfirmOrderSuccessCallback = null
        set(value) {
            field = value
            wxPayUtils.onConfirmOrderSuccess = value
            aliPayUtils.onConfirmOrderSuccess = value
        }

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        owner.lifecycle.addObserver(wxPayUtils)
    }

    override fun onDestroy(owner: LifecycleOwner) {
        onPaySuccessCallback = null
        onConfirmOrderSuccess = null
        super.onDestroy(owner)
    }

    fun confirmCommonProductOrder(
        productSku: String,
        orderAttr:String,
        payType: PayTypeEnum,
        contactPhone:String,
        activity: ComponentActivity
    ) {
        activity.launch {
            when (payType) {
                PayTypeEnum.WXPAY_APP -> wxPayUtils.confirmCommonProductOrder(productSku,orderAttr,contactPhone)
                PayTypeEnum.ALIPAY_APP -> aliPayUtils.confirmCommonProductOrder(productSku,orderAttr,contactPhone, activity)
                else -> ToastUtils.showShort("不支持的支付方法")
            }
        }

    }
}