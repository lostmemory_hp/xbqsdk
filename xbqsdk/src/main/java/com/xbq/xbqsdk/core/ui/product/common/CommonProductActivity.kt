package com.xbq.xbqsdk.core.ui.product.common

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.core.pay.ChoosePayTypeDialog
import com.xbq.xbqsdk.core.pay.XbqPayUtils
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.databinding.ActivityCommonProductBinding
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.dto.ProductListDto
import com.xbq.xbqsdk.util.coroutine.launch
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-01
 * Description:
 */
@AndroidEntryPoint
class CommonProductActivity : BaseActivity<ActivityCommonProductBinding>() {
    companion object {
        const val EXTRA_FEATURE = "feature"
        const val EXTRA_TITLE = "title"
        const val EXTRA_BUY_DESC = "buyDesc"
        fun newIntent(context: Context,feature:String, title:String, buyDesc:String):Intent{
            return Intent(context, CommonProductActivity::class.java).apply{
                putExtra(EXTRA_FEATURE, feature)
                putExtra(EXTRA_TITLE, title)
                putExtra(EXTRA_BUY_DESC, buyDesc)
            }
        }
    }

    @Inject
    lateinit var commonApi: CommonApi
    @Inject
    lateinit var xbqPayUtils: XbqPayUtils
    @Inject
    lateinit var adapter: CommonProductAdapter

    var feature: String? = null
    var title: String = ""
    var buyDesc: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent.getStringExtra(EXTRA_FEATURE)?.let { feature = it }
        intent.getStringExtra(EXTRA_TITLE)?.let { title = it }
        intent.getStringExtra(EXTRA_BUY_DESC)?.let { buyDesc = it }
        binding.tvBuyDesc.isVisible = buyDesc.isNotEmpty()
        binding.tvBuyDesc.text = buyDesc
        if (title.length > 0) {
            binding.titlebar.title = title
        }

        binding.titlebar.setOnTitleBarListener(object :OnTitleBarListener{
            override fun onLeftClick(view: View?) {
                finish()
            }
        })

        binding.let {
            it.recyclerview.adapter = adapter
            it.recyclerview.layoutManager = LinearLayoutManager(this)
            adapter.setEmptyView(com.xbq.xbqsdk.R.layout.xbq_empty_view)
            adapter.setOnItemClickListener { adt, view, position ->
                adapter.selectedIndex = position
            }

            it.btnPay.setOnClickListener {
                if(adapter.selectedItem==null){
                    ToastUtils.showShort("请选择要购买的会员")
                    return@setOnClickListener
                }
//                if(binding.etContactPhone.length()==0){
//                    ToastUtils.showShort("请选择要购买的会员")
//                    return@setOnClickListener
//                }
                val phone = binding.etContactPhone.text.trim().toString()
                adapter.selectedItem?.let { productVO ->
                    ChoosePayTypeDialog().apply {
                        setPayClickListener {
                            xbqPayUtils.confirmCommonProductOrder(productVO.sku,"",it,phone,this@CommonProductActivity)
                        }
                        show(supportFragmentManager, "payorder")
                    }
                }
            }
        }
        loadProducts()
        initPayUtils()

    }


    fun loadProducts() {
        if (feature == null) return
        launch {
            val res = commonApi.productList(ProductListDto(feature!!))
            if (res.success()) {
                adapter.setList(res.data!!.toMutableList())
                adapter.selectedIndex=0
            } else {
                ToastUtils.showShort(res.message)
            }
        }
    }

    private fun initPayUtils() {
        lifecycle.addObserver(xbqPayUtils)
        xbqPayUtils.onConfirmOrderSuccess = { dto, vo -> true }
        xbqPayUtils.onPaySuccessCallback = {  }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}