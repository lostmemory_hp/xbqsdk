package com.xbq.xbqsdk.core.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.xbq.xbqsdk.core.ui.account.LoginActivity
import com.xbq.xbqsdk.core.ui.product.common.CommonProductActivity

/**
 * Author: liaohaiping
 * Time: 2021-11-03
 * Description:
 */
class XbqActivityContracts {
    class BuyProduct: ActivityResultContract<String, Boolean>() {
        override fun createIntent(context: Context, feature: String?): Intent {
            val intent = Intent(context, CommonProductActivity::class.java)
            feature?.let {
                intent.putExtra(CommonProductActivity.EXTRA_FEATURE,feature)
            }
            return intent
        }

        override fun parseResult(resultCode: Int, intent: Intent?): Boolean {
            return  resultCode== Activity.RESULT_OK
        }

    }
    class Login: ActivityResultContract<Int, Boolean>() {
        override fun createIntent(context: Context, input: Int?): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            return intent
        }

        override fun parseResult(resultCode: Int, intent: Intent?): Boolean {
            return  resultCode== Activity.RESULT_OK
        }


    }
}