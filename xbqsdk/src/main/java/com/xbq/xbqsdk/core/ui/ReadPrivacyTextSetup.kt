package com.xbq.xbqsdk.core.ui

import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView

class ReadPrivacyTextSetup(val ckb:TextView) {
    fun setup() {
        val context = ckb.context
        val text = "已阅读并同意《用户协议》及《隐私政策》"
        val startOfUserAgreement = text.indexOf("《用户协议》")
        val endOfUserAgreement = startOfUserAgreement + "《用户协议》".length
        val userAgreementClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                WebviewActivity.startUserAgreement()
            }
        }
        val startOfPrivacy = text.indexOf("《隐私政策》")
        val endOfPrivacy = startOfPrivacy + "《隐私政策》".length
        val privacyClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                WebviewActivity.startPrivacy()
            }
        }

        SpannableString(text).apply {
            setSpan(
                userAgreementClickableSpan, startOfUserAgreement, endOfUserAgreement,
                SpannableString.SPAN_INCLUSIVE_INCLUSIVE
            )
            setSpan(
                privacyClickableSpan,
                startOfPrivacy,
                endOfPrivacy,
                SpannableString.SPAN_INCLUSIVE_INCLUSIVE
            )
            ckb.setText(this)
            ckb.movementMethod = LinkMovementMethod.getInstance()
        }
    }
}