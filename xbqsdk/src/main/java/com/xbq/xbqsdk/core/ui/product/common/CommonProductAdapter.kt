package com.xbq.xbqsdk.core.ui.product.common

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.databinding.ItemCommonProductBinding
import com.xbq.xbqsdk.net.common.vo.FeatureDescriber
import com.xbq.xbqsdk.net.common.vo.ProductVO
import com.xbq.xbqsdk.util.ext.addDeleteLine
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-03
 * Description:
 */
class CommonProductAdapter @Inject constructor(val featureDescriber: FeatureDescriber) :
    BaseQuickAdapter<ProductVO, BaseViewHolder>(R.layout.item_common_product) {
    var selectedIndex = 0
        set(value) {
            val old = field
            field = value
            selectedItem = getItem(value)
            notifyItemChanged(old)
            notifyItemChanged(value)
        }

    var selectedItem: ProductVO? = null

    var selectedViewHolder: ItemCommonProductBinding? = null


    override fun convert(holder: BaseViewHolder, item: ProductVO) {
        ItemCommonProductBinding.bind(holder.itemView).let {
            it.rbProductItem.isChecked = item == selectedItem
            it.root.isSelected = item == selectedItem
            it.tvCurrentPrice.text = "￥${item.price}"
            it.tvOldPrice.text = "￥${item.oldPrice}"
            it.tvOldPrice.addDeleteLine()
            it.tvProductName.text = item.name
            it.tvProductDesc.text = item.description
            it.tvProductFeatures.text = item.getProductFeatureStr(featureDescriber)
        }
    }


}