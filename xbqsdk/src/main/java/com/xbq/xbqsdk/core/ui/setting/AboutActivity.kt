package com.xbq.xbqsdk.core.ui.setting

import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.MetaDataUtils
import com.blankj.utilcode.util.ResourceUtils
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.databinding.ActivityAboutBinding
import com.xbq.xbqsdk.core.ui.DevInfoActivity
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.util.click.fiveClick
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-26
 * Description:
 */
@AndroidEntryPoint
class AboutActivity : BaseActivity<ActivityAboutBinding>() {
    @Inject
    lateinit var commonApi: CommonApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.titlebar.setOnTitleBarListener(object : OnTitleBarListener {
            override fun onLeftClick(view: View?) {
                finish()
            }
        })
        binding.iconApp.setImageResource(AppUtils.getAppIconId())
        binding.tvAppName.setText(AppUtils.getAppName())
        binding.tvAppVersion.setText("版本号: ${AppUtils.getAppVersionName()}")
        binding.tvUpdateTime.setText("客服QQ: ${MetaDataUtils.getMetaDataInApp("QQ")}")

        binding.iconApp.fiveClick { ActivityUtils.startActivity(DevInfoActivity::class.java) }
    }


}