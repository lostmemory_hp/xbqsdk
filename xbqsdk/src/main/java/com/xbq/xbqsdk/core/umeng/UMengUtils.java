package com.xbq.xbqsdk.core.umeng;

import android.content.Context;
import android.os.Bundle;

import com.blankj.utilcode.util.MetaDataUtils;
import com.uc.crashsdk.export.CrashApi;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.umcrash.UMCrash;
import com.umeng.umcrash.UMCrashCallback;
import com.xbq.xbqsdk.XbqSdk;

/**
 * Author: liaohaiping
 * Time: 2021-06-02
 * Description:
 */
public class UMengUtils {
//    public static String UM_APP_KEY= BuildConfig.UMENG_APP_KEY;

    public static String getUmAppKey() {
        return MetaDataUtils.getMetaDataInApp("UMENG_APPKEY");
    }

    public static String getUmengChannel() {
        String umengChannel = MetaDataUtils.getMetaDataInApp("UMENG_CHANNEL");
        if ("360".equals(umengChannel)) {
            umengChannel = "q360";  //友盟规定不要使用纯数字作为渠道ID
        }
        return umengChannel;
    }

    public static void init(Context context) {
        UMConfigure.init(context.getApplicationContext(), getUmAppKey(), getUmengChannel(), UMConfigure.DEVICE_TYPE_PHONE, "");
        UMConfigure.setLogEnabled(XbqSdk.isDEBUG());
        // 选用AUTO页面采集模式
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
        MobclickAgent.setCatchUncaughtExceptions(true);
        // 支持在子进程中统计自定义事件
        UMConfigure.setProcessEvent(true);

        /**
         * 如果您使用我们的SDK捕获native崩溃后，其他捕获工具无法捕获到native崩溃，
         * 可以使用如下方法在初始化SDK后进行设置，使其他SDK可以捕获到native崩溃
         */
        final Bundle customInfo = new Bundle();
        customInfo.putBoolean("mCallNativeDefaultHandler", true);
        CrashApi.getInstance().updateCustomInfo(customInfo);


        //崩溃回调（自定义字段）
        UMCrash.registerUMCrashCallback(new UMCrashCallback() {
            @Override
            public String onCallback() {
                return "inner version: " + XbqSdk.getInnerVersion();
            }
        });
    }

    public static void preinit(Context context) {
        UMConfigure.preInit(context.getApplicationContext(), getUmAppKey(), getUmengChannel());

    }

}
