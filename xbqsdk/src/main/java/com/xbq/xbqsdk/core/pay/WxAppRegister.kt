package com.xbq.xbqsdk.core.pay

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.annotation.Keep
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.net.constants.SysConfigEnum
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.components.ActivityComponent
import javax.inject.Inject

@AndroidEntryPoint
class WxAppRegister : BroadcastReceiver() {
    @Inject
    lateinit var userCache: UserCache
    override fun onReceive(context: Context, intent: Intent) {
        val wxappId = userCache.getConfig(SysConfigEnum.WX_APPID)
        if(wxappId.isNotEmpty()) {
            val msgApi = WXAPIFactory.createWXAPI(context, null)
            // 将该app注册到微信
            msgApi.registerApp(wxappId)
        }
    }
}