package com.xbq.xbqsdk.core.ui

import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.blankj.utilcode.util.*
import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.databinding.ActivityWebviewBinding
import com.xbq.xbqsdk.util.ext.urlEncode
import com.xbq.xbqsdk.util.format.FormatUtils
import dagger.hilt.android.AndroidEntryPoint

/**
 * Author: liaohaiping
 * Time: 2020-10-30
 * Description:
 */
@AndroidEntryPoint
class WebviewActivity :
    BaseActivity<ActivityWebviewBinding>() {

    companion object {
        fun startUserAgreement() {
            ActivityUtils.startActivity(Bundle().apply {
                putString("title", "用户协议")
                putString("url",XbqSdk.getUserAgreementUrl())
            }, WebviewActivity::class.java)
        }

        fun startPrivacy() {
            ActivityUtils.startActivity(Bundle().apply {
                putString("title", "隐私政策")
                val name = AppUtils.getAppName().urlEncode()
                val gs = MetaDataUtils.getMetaDataInApp("COMPANY").urlEncode()
                val idRes = ResourceUtils.getStringIdByName("app_build_time");
                val dt =  if(idRes>0) {
                   Utils.getApp().getString(idRes).urlEncode()
                }else{
                    FormatUtils.formatYMD(System.currentTimeMillis())
                }
                val qq = MetaDataUtils.getMetaDataInApp("QQ")
                val qs = "?n=${name}&gs=${gs}&dt=${dt}&qq=${qq}"
                putString("url","${XbqSdk.getPrivacyUrl()}${qs}")
            }, WebviewActivity::class.java)
        }

        /**
         *
         * @param title String
         * @param url String
         * @param shouldOverrideUrlLoading Boolean true表示由webview处理跳转, false表示webview不处理跳转, 由程序代码处理
         */
        fun startWebUrl(title: String, url: String,shouldOverrideUrlLoading:Boolean) {
            ActivityUtils.startActivity(Bundle().apply {
                putString("title", title)
                putString("url", url)
                putBoolean("shouldOverrideUrlLoading", shouldOverrideUrlLoading)
            }, WebviewActivity::class.java)
        }
    }

    // 1, 若没有设置 WebViewClient 则在点击链接之后由系统处理该 url，通常是使用浏览器打开或弹出浏览器选择对话框。
    // 2, 若设置 WebViewClient 且该方法返回 true ，则说明由应用的代码处理该 url，WebView 不处理。
    // 3, 若设置 WebViewClient 且该方法返回 false，则说明由 WebView处理该 url，即用 WebView 加载该 url。
    var shouldOverrideUrlLoading:Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.let {
            intent.getStringExtra("url")?.apply {
                it.webview.getSettings().setJavaScriptEnabled(true)
                it.webview.webViewClient = object : WebViewClient() {
                    override fun shouldOverrideUrlLoading(
                        view: WebView?,
                        request: WebResourceRequest?
                    ): Boolean {
                        // 1, 若没有设置 WebViewClient 则在点击链接之后由系统处理该 url，通常是使用浏览器打开或弹出浏览器选择对话框。
                        // 2, 若设置 WebViewClient 且该方法返回 true ，则说明由应用的代码处理该 url，WebView 不处理。
                        // 3, 若设置 WebViewClient 且该方法返回 false，则说明由 WebView处理该 url，即用 WebView 加载该 url。
//                        return super.shouldOverrideUrlLoading(view, request)
                        return shouldOverrideUrlLoading
                    }
                }
                it.webview.webChromeClient = object : WebChromeClient() {
                    override fun onReceivedTitle(view: WebView?, title: String?) {
                        super.onReceivedTitle(view, title)
                        if(title!=null && !title.startsWith("http")) {
                            binding.titlebar.title = title
                        }
                    }
                }
                it.webview.loadUrl(this)
            }
            shouldOverrideUrlLoading = intent.getBooleanExtra("shouldOverrideUrlLoading",false)
            intent.getStringExtra("title")?.apply {it.titlebar.title = this }
            it.titlebar.setOnTitleBarListener(object : OnTitleBarListener {
                override fun onLeftClick(view: View?) {
                    finish()
                }
            })
        }
    }
}