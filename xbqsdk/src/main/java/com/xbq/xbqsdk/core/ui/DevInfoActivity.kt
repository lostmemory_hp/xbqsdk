package com.xbq.xbqsdk.core.ui

import android.content.res.Resources
import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.ResourceUtils
import com.blankj.utilcode.util.ScreenUtils
import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.core.pay.WxEnvChecker
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.databinding.ActivityDevInfoBinding
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.base.BaseDto
import com.xbq.xbqsdk.net.constants.SysConfigEnum
import com.xbq.xbqsdk.util.DpiUtils
import com.xbq.xbqsdk.util.ShareFileUtils
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2020-11-06
 * Description:设备环境信息
 */
@AndroidEntryPoint
class DevInfoActivity : BaseActivity<ActivityDevInfoBinding>() {
    @Inject
    lateinit var wxEnvChecker: WxEnvChecker

    @Inject
    lateinit var userCache: UserCache

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.tvInfo.text = getDevEnvirontment()
        binding.titlebar.setOnTitleBarListener(object : OnTitleBarListener {
            override fun onLeftClick(view: View?) {
                finish()
            }

            override fun onRightClick(view: View?) {
                ShareFileUtils.shareText(this@DevInfoActivity, binding.tvInfo.text.toString())
            }
        })
    }

    fun getDevEnvirontment(): String {
        val info = StringBuilder()
        val dto = BaseDto()
        ScreenUtils.getScreenDensityDpi()
        ScreenUtils.getScreenDensity()
        info.append("\n手机DPI：${ScreenUtils.getScreenDensityDpi()},xdpi=${Resources.getSystem().displayMetrics.xdpi},ydpi=${Resources.getSystem().displayMetrics.ydpi}")
        info.append("\n手机分辨率：${DpiUtils.getScreenResolution(this)}")
        info.append("\n应用名称：${AppUtils.getAppName()}")
        info.append("\n包名：${AppUtils.getAppPackageName()}")
        info.append("\n版本名：${AppUtils.getAppVersionName()}")
        info.append("\n版本号：${AppUtils.getAppVersionCode()}")
        info.append("\n内部版本号：${XbqSdk.getInnerVersion()}")
        info.append("\n市场：${dto.appMarket}")
        info.append("\n渠道：${dto.agencyChannel}")
        val buildTimeResId = ResourceUtils.getStringIdByName("app_build_time")
        if(buildTimeResId>0) {
            info.append("\n发布时间：${resources.getText(buildTimeResId)}")
        }
        info.append("\nMD5签名：${AppUtils.getAppSignatureMD5().replace(":", "").toLowerCase()}")
        info.append("\nSHA1签名：${AppUtils.getAppSignatureSHA1()}")
        info.append("\nSHA256签名：${AppUtils.getAppSignatureSHA256()}")
        info.append("\n微信开发环境：${wxEnvChecker.checkWxEnv()}")
        info.append("\n微信支付环境：${wxEnvChecker.checkWxPayEnv()}")
        info.append("\n微信AppID：${userCache.getConfig(SysConfigEnum.WX_APPID)}")
        info.append("\nConfig：\n${GsonUtils.toJson(userCache.getConfigs())}")

        return info.toString()
    }


}