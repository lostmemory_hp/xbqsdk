package com.xbq.xbqsdk.core.ui.product.vip


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.Keep
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.ToastUtils
import com.gyf.immersionbar.ktx.immersionBar
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.component.recyleview.LinearSpaceItemDecoration
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.core.pay.ChoosePayTypeDialog
import com.xbq.xbqsdk.core.pay.XbqPayUtils
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.databinding.ActivityVipBinding
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.dto.ProductListDto
import com.xbq.xbqsdk.util.coroutine.launch
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class VipActivity : BaseActivity<ActivityVipBinding>() {
    companion object {
        const val EXTRA_VIPUIDATA = "vipUiData"
        fun newIntent(context: Context, data: VipUIData): Intent {
            return Intent(context, VipActivity::class.java).apply {
                putExtra(EXTRA_VIPUIDATA, data)
            }
        }
    }


    @Inject
    lateinit var commonApi: CommonApi

    @Inject
    lateinit var xbqPayUtils: XbqPayUtils

    val adapter by lazy { VipAdapter() }

    var uiData: VipUIData? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        immersionBar {
            statusBarDarkFont(true)
        }
        intent.getParcelableExtra<VipUIData>(EXTRA_VIPUIDATA)?.let {
            uiData = it
            bindUIData(it)
        }

        binding.titlebar.setOnTitleBarListener(object : OnTitleBarListener {
            override fun onLeftClick(view: View?) {
//                finish()
                showVipExitTip()
            }
        })

        binding.let {
            it.recyclerview.adapter = adapter
            it.recyclerview.layoutManager =
                LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
            it.recyclerview.addItemDecoration(
                LinearSpaceItemDecoration(
                    12,
                    RecyclerView.HORIZONTAL
                )
            )
            adapter.setEmptyView(R.layout.xbq_empty_view)
            adapter.setOnItemClickListener { adt, view, position ->
                adapter.select(position)
            }

            it.btnPay.setOnClickListener {
                if (adapter.selectedItem == null) {
                    ToastUtils.showShort("请选择要购买的会员")
                    return@setOnClickListener
                }
                adapter.selectedItem?.let { productVO ->
                    ChoosePayTypeDialog().apply {
                        setPayClickListener {
                            xbqPayUtils.confirmCommonProductOrder(
                                productVO.sku,
                                "",
                                it,
                                "",
                                this@VipActivity
                            )
                        }
                        show(supportFragmentManager, "payorder")
                    }
                }
            }
        }
        loadProducts()
        initPayUtils()

    }

    private fun
            bindUIData(it: VipUIData) {
        it.title?.let { binding.titlebar.title = it }
        it.vipDesc1?.let { binding.vipDesc1.text = it }
        it.vipDesc2?.let { binding.vipDesc2.text = it }

        it.vipIntro1?.let { binding.vipIntro1.text = it }
        it.vipIntroImg1?.let {
            val drawable = ContextCompat.getDrawable(this, it)
            binding.vipIntro1.setCompoundDrawables(null, drawable, null, null)
        }

        it.vipIntro2?.let { binding.vipIntro2.text = it }
        it.vipIntroImg2?.let {
            val drawable = ContextCompat.getDrawable(this, it)
            binding.vipIntro2.setCompoundDrawables(null, drawable, null, null)
        }

        it.vipIntro3?.let { binding.vipIntro3.text = it }
        it.vipIntroImg3?.let {
            val drawable = ContextCompat.getDrawable(this, it)
            binding.vipIntro3.setCompoundDrawables(null, drawable, null, null)
        }

        it.vipIntro4?.let { binding.vipIntro4.text = it }
        it.vipIntroImg4?.let {
            val drawable = ContextCompat.getDrawable(this, it)
            binding.vipIntro4.setCompoundDrawables(null, drawable, null, null)
        }

        it.vipIntro5?.let { binding.vipIntro5.text = it }
        it.vipIntroImg5?.let {
            val drawable = ContextCompat.getDrawable(this, it)
            binding.vipIntro5.setCompoundDrawables(null, drawable, null, null)
        }

        it.vipIntro6?.let { binding.vipIntro6.text = it }
        it.vipIntroImg6?.let {
            val drawable = ContextCompat.getDrawable(this, it)
            binding.vipIntro6.setCompoundDrawables(null, drawable, null, null)
        }
    }


    fun loadProducts() {
        if (uiData?.feature.isNullOrEmpty()) return
        launch {
            val res = commonApi.productList(ProductListDto(uiData!!.feature))
            if (res.success()) {
                adapter.setList(res.data!!.toMutableList())
                adapter.select(0)
            } else {
                ToastUtils.showShort(res.message)
            }
        }
    }

    private fun initPayUtils() {
        lifecycle.addObserver(xbqPayUtils)
        xbqPayUtils.onConfirmOrderSuccess = { dto, vo -> true }
        xbqPayUtils.onPaySuccessCallback = {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onBackPressed() {
        showVipExitTip()
    }

    private fun showVipExitTip() {
        VipExitDialog().apply {
            marginHorizontal = 64
            tranparentBackground = true
            onExitClick = { finish() }
            onContinueClick = {}
            show(supportFragmentManager, "vip_exit")
        }
    }


    override fun onDestroy() {
        super.onDestroy()
    }

    @Keep
    class VipUIData() : Parcelable {
        var feature: String = ""
        var title:String? = null
        var vipDesc1: String? = null
        var vipDesc2: String? = null
        var vipIntro1: String? = null
        var vipIntro2: String? = null
        var vipIntro3: String? = null
        var vipIntro4: String? = null
        var vipIntro5: String? = null
        var vipIntro6: String? = null

        @DrawableRes
        var vipDescIcon: Int? = null

        @DrawableRes
        var vipIntroImg1: Int? = null

        @DrawableRes
        var vipIntroImg2: Int? = null

        @DrawableRes
        var vipIntroImg3: Int? = null

        @DrawableRes
        var vipIntroImg4: Int? = null

        @DrawableRes
        var vipIntroImg5: Int? = null

        @DrawableRes
        var vipIntroImg6: Int? = null

        constructor(parcel: Parcel) : this() {
            feature = parcel.readString()!!
            title = parcel.readString()
            vipDesc1 = parcel.readString()
            vipDesc2 = parcel.readString()
            vipIntro1 = parcel.readString()
            vipIntro2 = parcel.readString()
            vipIntro3 = parcel.readString()
            vipIntro4 = parcel.readString()
            vipIntro5 = parcel.readString()
            vipIntro6 = parcel.readString()
            vipDescIcon = parcel.readValue(Int::class.java.classLoader) as? Int
            vipIntroImg1 = parcel.readValue(Int::class.java.classLoader) as? Int
            vipIntroImg2 = parcel.readValue(Int::class.java.classLoader) as? Int
            vipIntroImg3 = parcel.readValue(Int::class.java.classLoader) as? Int
            vipIntroImg4 = parcel.readValue(Int::class.java.classLoader) as? Int
            vipIntroImg5 = parcel.readValue(Int::class.java.classLoader) as? Int
            vipIntroImg6 = parcel.readValue(Int::class.java.classLoader) as? Int
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(feature)
            parcel.writeString(title)
            parcel.writeString(vipDesc1)
            parcel.writeString(vipDesc2)
            parcel.writeString(vipIntro1)
            parcel.writeString(vipIntro2)
            parcel.writeString(vipIntro3)
            parcel.writeString(vipIntro4)
            parcel.writeString(vipIntro5)
            parcel.writeString(vipIntro6)
            parcel.writeValue(vipDescIcon)
            parcel.writeValue(vipIntroImg1)
            parcel.writeValue(vipIntroImg2)
            parcel.writeValue(vipIntroImg3)
            parcel.writeValue(vipIntroImg4)
            parcel.writeValue(vipIntroImg5)
            parcel.writeValue(vipIntroImg6)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<VipUIData> {
            override fun createFromParcel(parcel: Parcel): VipUIData {
                return VipUIData(parcel)
            }

            override fun newArray(size: Int): Array<VipUIData?> {
                return arrayOfNulls(size)
            }
        }


    }
}