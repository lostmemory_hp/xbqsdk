package com.xbq.xbqsdk.core.pay

import android.app.Activity
import com.alipay.sdk.app.PayTask
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.dto.ConfirmOrderDto
import com.xbq.xbqsdk.net.common.vo.ConfirmOrderVO
import com.xbq.xbqsdk.net.common.vo.ProductVO
import com.xbq.xbqsdk.net.constants.PayTypeEnum
import kotlinx.coroutines.*
import java.math.BigDecimal
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2020-11-11
 * Description:
 */
class AliPayUtils @Inject constructor(val commonApi: CommonApi,
                                      val userCache: UserCache,
                                      val orderStatusFetcher: OrderStatusFetcher) {
    var onPaySuccessCallback: PaySuccessCallback = null

    //返回值表示是否可以继续后面的步骤
    var onConfirmOrderSuccess:ConfirmOrderSuccessCallback = null

    suspend fun confirmCommonProductOrder(productSku: String,orderAttr:String,contactPhone:String, activity: Activity) {
        val dto = ConfirmOrderDto(
            productSku,
            PayTypeEnum.ALIPAY_APP,
            contactPhone,
            getPaydesc(),
            BigDecimal.ZERO,
            orderAttr
        )
        val res = commonApi.confirmOrder(dto)
        if (res.success()) {
            val confirmOrderVO = res.data!!
            val canConitune = onConfirmOrderSuccess?.invoke(dto, confirmOrderVO)?:true
            if(!canConitune){
                return
            }
            val alipay = PayTask(activity)
            val payResult = withContext(Dispatchers.IO){
                val result = alipay.payV2(confirmOrderVO.getPaymentData(), true)
                AliPayResult(result)
            }
            /**
             * 对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
             */
            val resultInfo: String = payResult.getResult() // 同步返回需要验证的信息
            val resultStatus: String = payResult.getResultStatus()
            // 判断resultStatus 为9000则代表支付成功
            when (resultStatus) {
                "4000" -> ToastUtils.showShort("订单支付失败")
                "5000" -> ToastUtils.showShort("重复请求")
                "6001" -> ToastUtils.showShort("用户取消")
                "6002" -> ToastUtils.showShort("网络错误")
                "其它" -> ToastUtils.showShort("其它支付错误")
                else -> {
                    // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                    orderStatusFetcher.fetchOrderStatus(
                        confirmOrderVO.orderNo,
                        onPaySuccessCallback
                    )
                }
            }
        } else {
            ToastUtils.showShort(res.message)
        }

    }

    private fun getPaydesc(): String {
        val buffer = StringBuffer()
        if (AppUtils.isAppInstalled("com.eg.android.AlipayGphone")) {
            buffer.append("有支付宝；")
        } else {
            buffer.append("无支付宝；")
        }
        if (AppUtils.isAppInstalled("com.tencent.mm")) {
            buffer.append("有微信。")
        } else {
            buffer.append("无微信。")
        }
        return buffer.toString()
    }
}