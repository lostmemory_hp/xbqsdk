package com.xbq.xbqsdk.core.glide

import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.view.View
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.transition.Transition

/**
 * Author: liaohaiping
 * Time: 2020-04-17
 * Description:
 */
class DrawableBackgroundTarget<T : View>(view: T) : CustomViewTarget<T, Drawable>(view),
    Transition.ViewAdapter {
    var animatable: Animatable? = null
    override fun onLoadFailed(errorDrawable: Drawable?) {
        view.background = errorDrawable
    }

    override fun onResourceCleared(placeholder: Drawable?) {
        view.background = placeholder
    }

    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
        if (transition == null || !transition.transition(resource, this)) {
            setResourceInternal(resource)
        } else {
            maybeUpdateAnimatable(resource)
        }
    }

    override fun getCurrentDrawable(): Drawable? {
        return view.background
    }

    override fun setDrawable(drawable: Drawable?) {
        view.background = drawable
    }


    private fun setResourceInternal(resource: Drawable?) { // Order matters here. Set the resource first to make sure that the Drawable has a valid and
// non-null Callback before starting it.
        setResource(resource)
        maybeUpdateAnimatable(resource)
    }

    private fun maybeUpdateAnimatable(resource: Drawable?) {
        if (resource is Animatable) {
            animatable = resource as Animatable?
            animatable?.start()
        } else {
            animatable = null
        }
    }


    protected fun setResource(resource: Drawable?) {
        view.background = resource
    }

}