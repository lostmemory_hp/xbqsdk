package com.xbq.xbqsdk.core.ui.setting

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import com.blankj.utilcode.util.AppUtils
import com.xbq.xbqsdk.component.dialog.XbqDialogFragment
import com.xbq.xbqsdk.databinding.DlgPrivacyAgreementBinding
import com.xbq.xbqsdk.core.ui.WebviewActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * Author: liaohaiping
 * Time: 2021-11-19
 * Description:
 */
@AndroidEntryPoint
class PrivacyDialogFragment : XbqDialogFragment<DlgPrivacyAgreementBinding>() {
    var onAgree: (() -> Unit)? = null
    var onReject: (() -> Unit)? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var text = "感谢您使用${AppUtils.getAppName()}软件，在使用我们的软件前请您仔细阅读《隐私政策》及《用户协议》、为保证您可以正常使用本软件所有功能，请同意我们的协议。"
        val spannableString = SpannableString(text)
        val userAgreeClick: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                WebviewActivity.startUserAgreement()
            }
        }
        val privacyClick: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                WebviewActivity.startPrivacy()
            }
        }
        val startOfPrivacy: Int = text.indexOf("《隐私政策》")
        val endOfPrivacy = startOfPrivacy + "《隐私政策》".length
        val startOfUserAgreement: Int = text.indexOf("《用户协议》")
        val endOfUserAgreement = startOfUserAgreement + "《用户协议》".length
        spannableString.setSpan(
            privacyClick,
            startOfPrivacy,
            endOfPrivacy,
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )
        spannableString.setSpan(
            userAgreeClick,
            startOfUserAgreement,
            endOfUserAgreement,
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )

        binding.tvContent.setText(spannableString)
        binding.tvContent.setMovementMethod(LinkMovementMethod.getInstance())
        binding.btnAgree.setOnClickListener {
            dismiss()
            onAgree?.invoke()
        }
        binding.btnReject.setOnClickListener {
            dismiss()
            onReject?.invoke()
        }
    }

    override fun onDestroy() {
        onAgree = null
        onReject = null
        super.onDestroy()
    }

}