package com.xbq.xbqsdk.core.pay

import com.xbq.xbqsdk.net.common.dto.ConfirmOrderDto
import com.xbq.xbqsdk.net.common.vo.ConfirmOrderVO

/**
 * Author: liaohaiping
 * Time: 2021-12-10
 * Description:
 */

typealias  ConfirmOrderSuccessCallback =  (suspend (ConfirmOrderDto, ConfirmOrderVO) -> Boolean)?
typealias  PaySuccessCallback = (()->Unit)?