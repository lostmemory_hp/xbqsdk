package com.xbq.xbqsdk.core.pay

import android.content.Context
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.core.event.UserInfoChanged
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.net.base.BaseDto
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.dto.OrderStatusDto
import com.xbq.xbqsdk.net.constants.PayStatusEnum
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2020-11-11
 * Description:
 */
class OrderStatusFetcher @Inject constructor(
    val commonApi: CommonApi,
    val userCache: UserCache,
    @ApplicationContext val context: Context
) {
    var canceled = AtomicBoolean(false)
    var repeatCount = 0 //重试次数
    suspend fun fetchOrderStatus(orderNo: String, onSuccess: (() -> Unit)?) = MainScope().launch {
        while (canceled.get() == false) {
            if (repeatCount > 20) {
                ToastUtils.showShort("获取订单状态超时，请重新打开应用。")
                break
            }
            repeatCount++
            val res = commonApi.orderStatus(OrderStatusDto(orderNo))
            if (res.success()) {
                when (res.data!!.payStatus) {
                    PayStatusEnum.PAID -> {
                        val featureRes = commonApi.userFeatures(BaseDto())
                        if (featureRes.success()) {
                            featureRes.data?.let { userCache.saveUserFeatures(it) }
                            EventBus.getDefault().post(UserInfoChanged())
                            ToastUtils.showShort("恭喜您，支付成功！")
                        } else {
                            ToastUtils.showLong("支付成功，但是获取会员信息失败，请重新打开应用即可看到会员信息。")
                        }
                        onSuccess?.let { it() }
                        break
                    }
                    PayStatusEnum.CLOSED -> {
                        ToastUtils.showShort("订单已关闭，请重新支付")
                        break
                    }
                    PayStatusEnum.REFUNDED -> {
                        ToastUtils.showShort("订单已退款")
                        break
                    }
                    else -> {
                        delay(3000)
                    }
                }
            } else {
                delay(3000)
            }
        }
    }.join()

    fun cancel() {
        canceled.set(true)
    }


}