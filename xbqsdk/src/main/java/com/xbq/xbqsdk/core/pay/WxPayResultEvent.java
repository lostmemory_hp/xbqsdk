package com.xbq.xbqsdk.core.pay;

import androidx.annotation.Keep;

@Keep
public class WxPayResultEvent {
    private boolean success;
    private String errorMessage;
    private String orderNo;

    public WxPayResultEvent(boolean success, String errorMessage, String orderNo) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.orderNo = orderNo;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
