package com.xbq.xbqsdk.core.pay

import android.content.Context
import android.content.pm.PackageManager
import com.blankj.utilcode.util.LogUtils
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2020-10-30
 * Description:
 */
class WxEnvChecker @Inject constructor(@ActivityContext val context: Context) {

    companion object {
        val TAG = WxEnvChecker::class.simpleName
        const val ENV_RESULT_PAY_OK = "微信支付环境已配置好"
        const val ENV_RESULT_OPEN_0K = "微信开放平台环境已配置好"
    }

    /**
     * 检查微信开发环境
     */
    fun checkWxEnv(): String {
        val wxentryActivityClassName = "${context.packageName}.wxapi.WXEntryActivity"
        try {
            val forName = Class.forName(wxentryActivityClassName);
            if (forName == null) {
                val msg = "没有添加WXEntryActivity类";
                LogUtils.d(TAG, msg)
                return msg
            } else {
                val resolveActivity = context.packageManager.getPackageInfo(
                    context.packageName,
                    PackageManager.GET_ACTIVITIES
                )
                    .activities
                    .firstOrNull { it.name.equals(wxentryActivityClassName) }
                if (resolveActivity == null) {
                    val msg = "Manifest中没有配置WXEntryActivity"
                    LogUtils.d(TAG, msg)
                    return msg
                }
            }
        } catch (e: Exception) {
            val msg = "没有添加WXEntryActivity类"
            LogUtils.d(TAG, msg)
            return msg
        }

        return ENV_RESULT_OPEN_0K
    }

    fun isWxOpenEnvOk() = ENV_RESULT_OPEN_0K.equals(checkWxEnv())


    /**
     * 检查微信支付环境
     */
    fun checkWxPayEnv(): String {
        val wxpayEntryActivityClassName = "${context.packageName}.wxapi.WXPayEntryActivity"
        try {
            //这一句用于判断类是否存在
            val forName = Class.forName(wxpayEntryActivityClassName)
            val resolveActivity = context.packageManager.getPackageInfo(
                context.packageName,
                PackageManager.GET_ACTIVITIES
            ).activities.firstOrNull { it.name.equals(wxpayEntryActivityClassName) }
            if (resolveActivity == null) {
                val msg = "Manifest中没有配置WXPayEntryActivity"
                LogUtils.d(TAG, msg)
                return msg
            }
        } catch (e: Exception) {
            val msg = "没有添加WXPayEntryActivity类"
            LogUtils.d(TAG, msg)
            return msg
        }

        return ENV_RESULT_PAY_OK
    }

    fun isWxPayEnvOk() = ENV_RESULT_PAY_OK.equals(checkWxPayEnv())
}