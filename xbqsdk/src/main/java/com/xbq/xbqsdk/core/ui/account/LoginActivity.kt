package com.xbq.xbqsdk.core.ui.account

import android.os.Bundle
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.databinding.ActivityLoginBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
@AndroidEntryPoint
class LoginActivity: BaseActivity<ActivityLoginBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showLogin()
    }

    fun showLogin(){
        showFragment(R.id.fragment, LoginFragment::class.java)
    }
    fun showRegister(){
        showFragment(R.id.fragment, RegisterFragment::class.java)
    }

    override fun onBackPressed() {
    }
}