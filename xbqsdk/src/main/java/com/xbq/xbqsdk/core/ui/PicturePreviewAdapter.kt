package com.xbq.xbqsdk.core.ui

import android.net.Uri
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.core.glide.GlideApp
import com.xbq.xbqsdk.databinding.ItemImageBinding

class PicturePreviewAdapter:BaseQuickAdapter<Uri,BaseViewHolder>(R.layout.item_image) {
    override fun convert(holder: BaseViewHolder, item: Uri) {
        ItemImageBinding.bind(holder.itemView).let {
            GlideApp.with(context)
                .load(item)
                .into(it.photoview)
        }
    }
}