package com.xbq.xbqsdk.core.ui.base

import androidx.viewbinding.ViewBinding
import com.xbq.xbqsdk.component.activity.VBActivity

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
open class BaseActivity<T : ViewBinding> : VBActivity<T>() {

}