package com.xbq.xbqsdk.core.ui.account

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.component.immersion.ImmersionFragment
import com.xbq.xbqsdk.databinding.FragmentLoginBinding
import com.xbq.xbqsdk.core.event.UserInfoChanged
import com.xbq.xbqsdk.core.ui.ReadPrivacyTextSetup
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.util.coroutine.launch
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
@AndroidEntryPoint
class LoginFragment : ImmersionFragment<FragmentLoginBinding>() {

    @Inject
    lateinit var userRepository: UserRepository

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.let {
            ReadPrivacyTextSetup(it.ckbReadPrivacyAlready).setup()
            it.btnLogin.setOnClickListener { doLogin() }
            it.btnGoRegister.setOnClickListener { goRegister() }
            it.ckbReadPrivacyAlready.setOnCheckedChangeListener { buttonView, isChecked ->
                setLoginButtonEnable()
            }
            it.etUserName.addTextChangedListener { setLoginButtonEnable() }
            it.etPassword.addTextChangedListener { setLoginButtonEnable() }
        }
    }


    private fun doLogin() {
        val userName = binding.etUserName.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()
        launch {
            val res = userRepository.login(userName, password)
            if (res.success()) {
                EventBus.getDefault().post(UserInfoChanged())
                ToastUtils.showShort("登录成功")
                requireActivity().let {
                    it.setResult(Activity.RESULT_OK)
                    it.finish()
                }
            } else {
                ToastUtils.showShort(res.message)
            }
        }
    }

    private fun setLoginButtonEnable() {
        val userName = binding.etUserName.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()
        binding.btnLogin.isEnabled = userName.isNotBlank()
                && password.isNotBlank()
                && binding.ckbReadPrivacyAlready.isChecked
    }


    private fun goRegister() {
        (requireActivity() as? LoginActivity)?.showRegister()
    }

}