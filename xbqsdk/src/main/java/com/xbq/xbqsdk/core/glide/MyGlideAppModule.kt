package com.xbq.xbqsdk.core.glide

import android.content.Context
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.cache.DiskCache
import com.bumptech.glide.load.engine.cache.ExternalPreferredCacheDiskCacheFactory
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MyGlideAppModule : AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        //1G缓存
        builder.setDiskCache(ExternalPreferredCacheDiskCacheFactory(context,DiskCache.Factory.DEFAULT_DISK_CACHE_DIR,1024*1024*1024))
        super.applyOptions(context, builder)
    }

}