package com.xbq.xbqsdk.core.pay

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.component.dialog.XbqDialogFragment
import com.xbq.xbqsdk.databinding.DlgChoosePayTypeBinding
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.constants.PayTypeEnum
import com.xbq.xbqsdk.net.constants.SysConfigEnum
import com.xbq.xbqsdk.util.click.onDebouncedClick
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2020-11-11
 * Description:
 */
@AndroidEntryPoint
class ChoosePayTypeDialog : XbqDialogFragment<DlgChoosePayTypeBinding>() {
    private var paytype: PayTypeEnum? = PayTypeEnum.ALIPAY_APP
    private var payClickListener: ((PayTypeEnum) -> Unit)? = null
    private var showAlipay = true
    private var showWxPay = true

    @Inject
    lateinit var userCache: UserCache

    @Inject
    lateinit var wxEnvChecker: WxEnvChecker


    fun isWxInstall() = AppUtils.isAppInstalled("com.tencent.mm")
    fun isAlipayInstall() = AppUtils.isAppInstalled("com.eg.android.AlipayGphone")

    /**
     * 自动根据配置设置显示支付方式，优先支付宝支付
     */
    fun autoSetPayButtonVisibility(): ChoosePayTypeDialog {
        showAlipay = !userCache.getConfigBoolean(SysConfigEnum.DISABLE_ALIPAY)
        val hasWxAppId = !TextUtils.isEmpty(userCache.getConfig(SysConfigEnum.WX_APPID))
        showWxPay = hasWxAppId && (!showAlipay || isWxInstall() && !isAlipayInstall())
        if (showAlipay) {
            paytype = PayTypeEnum.ALIPAY_APP
        } else {
            if (showWxPay) {
                paytype = PayTypeEnum.WXPAY_APP
            }
        }
        return this
    }

    /**
     * 自动根据配置设置显示支付方式，
     * 支付宝支付,微信可以同时显示
     */
    fun autoShowDoublePayButtonVisibility(): ChoosePayTypeDialog {
        showAlipay = !userCache.getConfigBoolean(SysConfigEnum.DISABLE_ALIPAY)
        val hasWxAppId = !TextUtils.isEmpty(userCache.getConfig(SysConfigEnum.WX_APPID))
        showWxPay = hasWxAppId && isWxInstall()


        paytype = if (showAlipay) PayTypeEnum.ALIPAY_APP
        else if (showWxPay) PayTypeEnum.WXPAY_APP
        else PayTypeEnum.ALIPAY_APP

        return this
    }

    fun hideAlipay(): ChoosePayTypeDialog {
        showAlipay = false
        if (paytype == PayTypeEnum.ALIPAY_APP) {
            paytype = null
        }
        return this
    }

    fun hideWxPay(): ChoosePayTypeDialog {
        showWxPay = false
        if (paytype == PayTypeEnum.WXPAY_APP) {
            paytype = null
        }
        return this
    }

    fun chooseWxPay(): ChoosePayTypeDialog {
        this.paytype = PayTypeEnum.WXPAY_APP
        return this
    }

    fun chooseAliPay(): ChoosePayTypeDialog {
        this.paytype = PayTypeEnum.ALIPAY_APP
        return this
    }

    fun setPayClickListener(listener: ((PayTypeEnum) -> Unit)): ChoosePayTypeDialog {
        this.payClickListener = listener
        return this
    }

    private fun checkWxPay() {
        paytype = PayTypeEnum.WXPAY_APP
        binding.alipayCheckIcon.visibility = View.INVISIBLE
        binding.alipayCheckIcon.setImageResource(R.drawable.ic_checkbox_unchecked)

        binding.wxpayCheckIcon.visibility = View.VISIBLE
        binding.wxpayCheckIcon.setImageResource(R.drawable.ic_checkbox_checked)
    }

    private fun checkAlipay() {
        paytype = PayTypeEnum.ALIPAY_APP
        binding.alipayCheckIcon.visibility = View.VISIBLE
        binding.alipayCheckIcon.setImageResource(R.drawable.ic_checkbox_checked)

        binding.wxpayCheckIcon.visibility = View.INVISIBLE
        binding.wxpayCheckIcon.setImageResource(R.drawable.ic_checkbox_unchecked)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.btnAlipay.visibility = if (showAlipay) View.VISIBLE else View.GONE
//        binding.btnWxPay.visibility = if (showWxPay) View.VISIBLE else View.GONE
        autoShowDoublePayButtonVisibility()
        binding.btnAlipay.onDebouncedClick { checkAlipay() }
        binding.btnWxPay.onDebouncedClick { checkWxPay() }
        binding.btnPayConfirm.onDebouncedClick {
            if (paytype == null) {
                ToastUtils.showShort("请选择支付方式")
            } else {
                if (paytype == PayTypeEnum.WXPAY_APP) {
                    val envResult = wxEnvChecker.checkWxPayEnv()
                    if (WxEnvChecker.ENV_RESULT_PAY_OK.equals(envResult)) {
                        if (isWxInstall()) {
                            dismiss()
                            payClickListener?.let { it(paytype!!) }
                        } else {
                            ToastUtils.showShort("您的设备上没有安装微信，无法使用微信支付。")
                        }
                    } else {
                        ToastUtils.showShort("${envResult},无法使用微信支付")
                    }
                } else {
                    dismiss()
                    payClickListener?.let { it(paytype!!) }
                }
            }
        }

        when (paytype) {
            PayTypeEnum.WXPAY_APP -> checkWxPay()
            PayTypeEnum.ALIPAY_APP -> checkAlipay()
        }

        if (!(showAlipay && showWxPay)) {
            dismissAllowingStateLoss()
            if(paytype!=null) {
                payClickListener?.invoke(paytype!!)
            }
        }
    }

    override fun onDestroy() {
        payClickListener = null
        super.onDestroy()
    }


}