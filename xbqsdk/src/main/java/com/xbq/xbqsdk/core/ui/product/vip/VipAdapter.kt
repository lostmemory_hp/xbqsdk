package com.xbq.xbqsdk.core.ui.product.vip

import androidx.core.view.isVisible
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.databinding.ItemVipBinding
import com.xbq.xbqsdk.net.common.vo.ProductVO
import com.xbq.xbqsdk.util.ext.addDeleteLine

/**
 * Author: liaohaiping
 * Time: 2021-01-20
 * Description:
 */
class VipAdapter :
    BaseQuickAdapter<ProductVO, BaseViewHolder>(R.layout.item_vip) {
    var selectedItem: ProductVO? = null


    override fun convert(holder: BaseViewHolder, item: ProductVO) {
        ItemVipBinding.bind(holder.itemView).let {
            val position = getItemPosition(item)
            it.tagXianshiyouhui.isVisible = position == 0
            it.root.isSelected = (selectedItem?.id == item.id)
            it.title.text = item.name
            it.tvOldPrice.text = "原价: ${item.oldPrice}"
            it.tvOldPrice.addDeleteLine()
            it.tvCurrentPrice.text = "￥${item.price}"
            when (position) {
                0 -> it.vipLogo.setImageResource(R.drawable.ic_vip_logo_1)
                1 -> it.vipLogo.setImageResource(R.drawable.ic_vip_logo_2)
                else -> it.vipLogo.setImageResource(R.drawable.ic_vip_logo_3)
            }
        }
    }


    fun select(video: ProductVO) {
        val old = selectedItem
        selectedItem = video
        if (old != null) {
            notifyData(old)
        }
        notifyData(video)
    }


    fun select(index: Int) {
        val old = selectedItem
        val item = getItem(index)
        selectedItem = item
        old?.let { notifyData(it) }
        notifyData(item)
    }

    private fun notifyData(old: ProductVO) {
        var index = data.indexOfFirst { old.id == it.id }
        if (index != -1) {
            notifyItemChanged(index)
        }
    }

    fun selectedIndex(): Int {
        return data.indexOfFirst { it.id == selectedItem?.id }
    }


}