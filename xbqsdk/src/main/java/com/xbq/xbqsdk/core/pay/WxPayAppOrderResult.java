package com.xbq.xbqsdk.core.pay;

import androidx.annotation.Keep;

@Keep
public class WxPayAppOrderResult {
  private String sign;
  private String prepayId;
  private String partnerId;
  private String appId;
  /**
   * 由于package为java保留关键字，因此改为packageValue. 前端使用时记得要更改为package
   */
  private String packageValue;
  private String timeStamp;
  private String nonceStr;

  public String getSign() {
    return sign;
  }

  public WxPayAppOrderResult setSign(String sign) {
    this.sign = sign;
    return this;
  }

  public String getPrepayId() {
    return prepayId;
  }

  public WxPayAppOrderResult setPrepayId(String prepayId) {
    this.prepayId = prepayId;
    return this;
  }

  public String getPartnerId() {
    return partnerId;
  }

  public WxPayAppOrderResult setPartnerId(String partnerId) {
    this.partnerId = partnerId;
    return this;
  }

  public String getAppId() {
    return appId;
  }

  public WxPayAppOrderResult setAppId(String appId) {
    this.appId = appId;
    return this;
  }

  public String getPackageValue() {
    return packageValue;
  }

  public WxPayAppOrderResult setPackageValue(String packageValue) {
    this.packageValue = packageValue;
    return this;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public WxPayAppOrderResult setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
    return this;
  }

  public String getNonceStr() {
    return nonceStr;
  }

  public WxPayAppOrderResult setNonceStr(String nonceStr) {
    this.nonceStr = nonceStr;
    return this;
  }
}
