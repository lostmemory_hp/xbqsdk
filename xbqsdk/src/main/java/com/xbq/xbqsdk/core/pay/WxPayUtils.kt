package com.xbq.xbqsdk.core.pay

import android.content.Context
import androidx.lifecycle.*
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.tencent.mm.opensdk.modelpay.PayReq
import com.tencent.mm.opensdk.openapi.IWXAPI
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.xbq.xbqsdk.R
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.dto.ConfirmOrderDto
import com.xbq.xbqsdk.net.common.vo.ConfirmOrderVO
import com.xbq.xbqsdk.net.common.vo.ProductVO
import com.xbq.xbqsdk.net.constants.PayTypeEnum
import com.xbq.xbqsdk.net.constants.SysConfigEnum
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.math.BigDecimal
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2020-11-10
 * Description:
 */
class WxPayUtils @Inject constructor(
    @ActivityContext
    val context: Context,
    val commonApi: CommonApi,
    val userCache: UserCache,
    val orderStatusFetcher: OrderStatusFetcher
) : DefaultLifecycleObserver {

    private lateinit var api: IWXAPI

    //返回值表示是否可以继续后面的步骤
    var onConfirmOrderSuccess:ConfirmOrderSuccessCallback = null

    var onPaySuccessCallback: PaySuccessCallback = null



    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        registToWx()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        LogUtils.d("WxPayUtils, on destory")
        EventBus.getDefault().unregister(this)
        super.onDestroy(owner)

    }

    fun registToWx() {
        LogUtils.d("registToWx")
        val appId = userCache.getConfig(SysConfigEnum.WX_APPID)
        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(context, appId, false)
        // 将应用的appId注册到微信
        api.registerApp(appId)
        EventBus.getDefault().register(this)
    }


    private fun getPaydesc(): String {
        val buffer = StringBuffer()
        if (AppUtils.isAppInstalled("com.eg.android.AlipayGphone")) {
            buffer.append("有支付宝；")
        } else {
            buffer.append("无支付宝；")
        }
        if (AppUtils.isAppInstalled("com.tencent.mm")) {
            buffer.append("有微信。")
        } else {
            buffer.append("无微信。")
        }
        return buffer.toString()
    }


    suspend fun confirmCommonProductOrder(productSku: String,orderAttr:String,contactPhone:String) {
        val dto = ConfirmOrderDto(
            productSku,
            PayTypeEnum.WXPAY_APP,
            contactPhone,
            getPaydesc(),
            BigDecimal.ZERO,
            orderAttr
        )
        val res = commonApi.confirmOrder(dto)
        if (res.success()) {
            val confirmOrderVO = res.data!!
            val canConitune = onConfirmOrderSuccess?.invoke(dto, confirmOrderVO)?:true
            if(!canConitune){
                return
            }
            val wxPayAppOrderResult: WxPayAppOrderResult = GsonUtils.fromJson(
                confirmOrderVO.paymentData,
                WxPayAppOrderResult::class.java
            )
            val request = PayReq()
            request.appId = wxPayAppOrderResult.appId
            request.partnerId = wxPayAppOrderResult.partnerId
            request.prepayId = wxPayAppOrderResult.prepayId
            request.packageValue = wxPayAppOrderResult.packageValue
            request.nonceStr = wxPayAppOrderResult.nonceStr
            request.timeStamp = wxPayAppOrderResult.timeStamp
            request.sign = wxPayAppOrderResult.sign
            request.extData = confirmOrderVO.getOrderNo()
            if (api.sendReq(request)) {

            } else {
                ToastUtils.showShort("支付失败。")
            }
        } else {
            ToastUtils.showShort(res.message)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onPayResult(event: WxPayResultEvent) {
        LogUtils.d("onPayResult:${GsonUtils.toJson(event)}")
        if (event.isSuccess) {
            MainScope().launch {
                orderStatusFetcher.fetchOrderStatus(event.orderNo,onPaySuccessCallback)
            }
        }
    }
}