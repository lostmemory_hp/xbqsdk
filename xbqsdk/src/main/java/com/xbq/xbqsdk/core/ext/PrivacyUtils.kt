package com.xbq.xbqsdk.core.ext

import androidx.fragment.app.FragmentActivity
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.SPUtils
import com.xbq.xbqsdk.core.ui.setting.PrivacyDialogFragment

/**
 * Author: liaohaiping
 * Time: 2021-11-19
 * Description:
 */
fun FragmentActivity.showPrivacy(onAgree: () -> Unit) {
    val key = "HAS_SHOW_PRIVACY"
    if(SPUtils.getInstance().getBoolean(key, false)){
        onAgree?.invoke()
        return
    }

    PrivacyDialogFragment().apply {
        this.isCancelable = false
        this.onAgree = {
            SPUtils.getInstance().put(key,true)
            onAgree?.invoke()
        }
        this.onReject = {
            finish()
            AppUtils.exitApp()
        }
        this.show(supportFragmentManager, "privacy_dialog")
    }
}