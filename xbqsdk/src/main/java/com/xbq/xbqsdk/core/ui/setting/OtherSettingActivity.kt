package com.xbq.xbqsdk.core.ui.setting

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.AppUtils
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.databinding.ActivityOtherSettingBinding
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.util.permissions.utils.PermissionAutoStartUtils
import com.xbq.xbqsdk.util.permissions.utils.PermissionUtils
import dagger.hilt.android.AndroidEntryPoint

/**
 * Author: liaohaiping
 * Time: 2021-11-27
 * Description:
 */
@AndroidEntryPoint
class OtherSettingActivity : BaseActivity<ActivityOtherSettingBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.titlebar.setOnTitleBarListener(object:OnTitleBarListener{
            override fun onLeftClick(view: View?) {
                finish()
            }
        })

        binding.btnSetBatteryWhiteList.setOnClickListener { PermissionUtils.ignoreBatteryOptimization(this,true) }
        binding.btnSetBackgroundPermisson.setOnClickListener {  PermissionAutoStartUtils.goAutoRunPage()  }
        binding.tvBatteryOptimizeDesc.text="系统为了省电会在定位过程中误杀【${AppUtils.getAppName()}】，需要您将【${AppUtils.getAppName()}】加入保护白名单。"
    }
}