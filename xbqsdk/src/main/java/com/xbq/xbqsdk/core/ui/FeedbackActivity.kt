package com.xbq.xbqsdk.core.ui

import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.databinding.ActivityFeedbackBinding
import com.xbq.xbqsdk.core.ext.ensureLogin
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.net.common.dto.AddFeedbackDto
import com.xbq.xbqsdk.util.click.onDebouncedClick
import com.xbq.xbqsdk.util.coroutine.launch
import com.xbq.xbqsdk.util.ext.isEmpty
import com.xbq.xbqsdk.util.ext.validateEmpty
import com.xbq.xbqsdk.util.ext.value
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-26
 * Description:
 */
@AndroidEntryPoint
class FeedbackActivity : BaseActivity<ActivityFeedbackBinding>() {
    @Inject
    lateinit var userCache: UserCache
    @Inject
    lateinit var commonApi: CommonApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.titlebar.setOnTitleBarListener(object : OnTitleBarListener {
            override fun onLeftClick(view: View?) {
                finish()
            }

            override fun onRightClick(view: View?) {
                submitFeedback()
            }
        })
        binding.btnSubmit.onDebouncedClick { submitFeedback() }
    }

    fun submitFeedback() {
        if (validate()) {
            ensureLogin(userCache) {
                launch {
                    val dto = AddFeedbackDto(binding.etTitle.value(),binding.etContent.value(),binding.etContactPhone.value())
                    val res = commonApi.addFeedback(dto)
                    if(res.success()){
                        ToastUtils.showShort("已成功提交，感谢您的反馈。")
                        finish()
                    }else{
                        ToastUtils.showShort(res.message)
                    }
                }
            }

        }
    }

    private fun validate(): Boolean {
        var isvalid = true
        if(binding.etTitle.isEmpty()){
            isvalid=false
            binding.etTitle.setError("标题不能为空")
        }else{
            binding.etTitle.setError(null)
        }
        if(binding.etContent.isEmpty()){
            isvalid=false
            binding.etContent.setError("内容不能为空")
        }else{
            binding.etContent.setError(null)
        }
        if(binding.etContactPhone.isEmpty()){
            isvalid=false
            binding.etContactPhone.setError("联系方式不能为空")
        }else{
            binding.etContactPhone.setError(null)
        }
        return  isvalid
    }
}