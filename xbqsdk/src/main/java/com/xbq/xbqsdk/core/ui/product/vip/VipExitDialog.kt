package com.xbq.xbqsdk.core.ui.product.vip

import android.os.Bundle
import android.view.View
import com.xbq.xbqsdk.component.dialog.XbqDialogFragment
import com.xbq.xbqsdk.databinding.DlgVipExitBinding
import com.xbq.xbqsdk.util.click.onDebouncedClick

/**
 * Author: liaohaiping
 * Time: 2022-02-26
 * Description:
 */
class VipExitDialog : XbqDialogFragment<DlgVipExitBinding>() {
    var onExitClick: (() -> Unit)? = null
    var onContinueClick: (() -> Unit)? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnContinue.onDebouncedClick {
            onContinueClick?.invoke()
            dismissAllowingStateLoss()
        }
        binding.btnExit.onDebouncedClick {
            onExitClick?.invoke()
            dismissAllowingStateLoss()
        }
    }

    override fun onDestroy() {
        onExitClick = null
        onContinueClick = null
        super.onDestroy()
    }
}