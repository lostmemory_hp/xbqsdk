package com.xbq.xbqsdk.core.ui

import android.net.Uri
import android.os.Bundle
import com.blankj.utilcode.util.ActivityUtils
import com.xbq.xbqsdk.databinding.ActivityPicturePreviewBinding
import com.xbq.xbqsdk.core.ui.base.BaseActivity

class PicturePreviewActivity : BaseActivity<ActivityPicturePreviewBinding>() {

    var imageList = mutableListOf<Uri>()
    var currentIndex = 0
    val adapter = PicturePreviewAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parseArgs()
        binding.viewPager.adapter = adapter
    }

    fun parseArgs() {
        intent.getParcelableArrayListExtra<Uri>(EXTRA_IMAGE_LIST)?.let {
            imageList.clear()
            imageList.addAll(it)
        }
        currentIndex = intent.getIntExtra(EXTRA_CURRENT_INDEX, 0)
    }

    companion object {
        const val EXTRA_IMAGE_LIST = "imageList"
        const val EXTRA_CURRENT_INDEX = "currentIndex"
        fun startPreview(imageList: Collection<Uri>, currentIndex: Int) {
            var extras = Bundle().apply {
                val images = arrayListOf<Uri>()
                images.addAll(imageList)
                putParcelableArrayList(EXTRA_IMAGE_LIST,images)
                putInt(EXTRA_CURRENT_INDEX, currentIndex)
            }
            ActivityUtils.startActivity(extras, PicturePreviewActivity::class.java)
        }

        fun startPreview(imageList: ArrayList<Uri>) {
            startPreview(imageList, 0)
        }

        fun startPreview(uri: Uri) {
            startPreview(arrayListOf(uri), 0)
        }

    }
}