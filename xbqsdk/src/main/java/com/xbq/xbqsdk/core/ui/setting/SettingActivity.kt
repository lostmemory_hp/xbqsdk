package com.xbq.xbqsdk.core.ui.setting

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.core.ui.base.BaseActivity
import com.xbq.xbqsdk.component.titlebar.OnTitleBarListener
import com.xbq.xbqsdk.databinding.ActivitySettingBinding
import com.xbq.xbqsdk.core.event.UserInfoChanged
import com.xbq.xbqsdk.core.ext.ensureLogin
import com.xbq.xbqsdk.core.ui.account.DeleteAccountFragment
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.net.UserRepository
import com.xbq.xbqsdk.net.common.CommonApi
import com.xbq.xbqsdk.util.click.onDebouncedClick
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Author: liaohaiping
 * Time: 2021-11-26
 * Description:
 */
@AndroidEntryPoint
class SettingActivity : BaseActivity<ActivitySettingBinding>() {
    @Inject
    lateinit var commonApi: CommonApi
    @Inject
    lateinit var userCache: UserCache
    @Inject
    lateinit var userRepository: UserRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.titlebar.setOnTitleBarListener(object : OnTitleBarListener {
            override fun onLeftClick(view: View?) {
                finish()
            }
        })
        binding.btnOtherSetting.onDebouncedClick { ActivityUtils.startActivity(OtherSettingActivity::class.java) }
        binding.btnLoginOut.onDebouncedClick {
            exitLogin()
        }
        binding.btnDeleteAccount.onDebouncedClick {
            DeleteAccountFragment().apply {
                onDeleteAccountSuccess={
                    exitLogin()
                    ToastUtils.showShort("帐号注销成功")
                }
                show(supportFragmentManager,"deleteAccount")
            }
        }
        binding.btnLoginOut.isVisible = userCache.isLogin()
        binding.btnDeleteAccount.isVisible = userCache.isLogin()
    }



    fun exitLogin(){
        userCache.exitLogin()
        EventBus.getDefault().post(UserInfoChanged())
        finish()
    }



}