package com.xbq.xbqsdk;

import androidx.activity.ComponentActivity;

import com.xbq.xbqsdk.core.ui.account.LoginActivity;
import com.xbq.xbqsdk.core.ui.product.vip.VipActivity;
import com.xbq.xbqsdk.net.common.vo.FeatureDescriber;

/**
 * Author: liaohaiping
 * Time: 2022-03-04
 * Description:
 */
public final class XbqSdk {
    public static final String API_PREFIX_PLACEHOLDER = "$$API_PREFIX_PLACEHOLDER$$";
    private static String BASE_URL = "";
    private static String APPLICATION = "";
    private static String INNER_VERSION = "1";
    private static String API_PREFIX = "";
    private static String PRIVACY_URL = "";
    private static String USER_AGREEMENT_URL = "";
    private static boolean DEBUG = true;
    private static Class<?> mainActivity;
    private static Class<?> loginActivity = LoginActivity.class;
    private static FeatureDescriber featureDescriber;


    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getApplication() {
        return APPLICATION;
    }

    public static String getInnerVersion() {
        return INNER_VERSION;
    }

    public static String getApiPrefix() {
        return API_PREFIX;
    }

    public static boolean isDEBUG() {
        return DEBUG;
    }

    public static String getPrivacyUrl() {
        return PRIVACY_URL;
    }

    public static String getUserAgreementUrl() {
        return USER_AGREEMENT_URL;
    }

    public static Class<?> getMainActivity() {
        return mainActivity;
    }

    public static Class<?> getLoginActivity() {
        return loginActivity;
    }


    public static FeatureDescriber getFeatureDescriber() {
        return featureDescriber;
    }

    public static void setBaseUrl(String baseUrl) {
        BASE_URL = baseUrl;
    }

    public static void setApplication(String APPLICATION) {
        XbqSdk.APPLICATION = APPLICATION;
    }

    public static void setInnerVersion(String innerVersion) {
        INNER_VERSION = innerVersion;
    }

    public static void setApiPrefix(String apiPrefix) {
        API_PREFIX = apiPrefix;
    }

    public static void setPrivacyUrl(String privacyUrl) {
        PRIVACY_URL = privacyUrl;
    }

    public static void setUserAgreementUrl(String userAgreementUrl) {
        USER_AGREEMENT_URL = userAgreementUrl;
    }

    public static void setDEBUG(boolean DEBUG) {
        XbqSdk.DEBUG = DEBUG;
    }


    public static void setMainActivity(Class<?> mainActivity) {
        XbqSdk.mainActivity = mainActivity;
    }

    public static void setLoginActivity(Class<?> loginActivity) {
        XbqSdk.loginActivity = loginActivity;
    }


    public static void setFeatureDescriber(FeatureDescriber featureDescriber) {
        XbqSdk.featureDescriber = featureDescriber;
    }
}
