package com.xiaobaqi.xbqdemo

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.util.activityresult.ActivityContractHelper
import com.xbq.xbqsdk.util.click.onDebouncedClick
import com.xbq.xbqsdk.util.coroutine.launch
import com.xiaobaqi.xbqdemo.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var userCache: UserCache

    lateinit var helper: ActivityContractHelper<Array<String>, Uri?>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater, null, false)
        setContentView(binding.root)
        helper = ActivityContractHelper()
        lifecycle.addObserver(helper)
        helper.register(this, ActivityResultContracts.OpenDocument())
        binding.button.setOnClickListener {
            launch {
                helper.startForResult(arrayOf("image/*")) {
                    Log.d("lhp", "open document, uri: ${it.toString()}")
                }
            }
        }
        binding.button2.onDebouncedClick {
            ensureVip(userCache){
                launch {
                    helper.startForResult(arrayOf("image/*")) {
                        Log.d("lhp", "open document, uri: ${it.toString()}")
                    }
                }
            }
        }

    }
}