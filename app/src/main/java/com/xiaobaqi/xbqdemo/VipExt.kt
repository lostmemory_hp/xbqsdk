package com.xiaobaqi.xbqdemo

import android.app.Activity
import androidx.fragment.app.FragmentActivity
import com.blankj.utilcode.util.ToastUtils
import com.xbq.xbqsdk.core.ext.ensureLogin
import com.xbq.xbqsdk.core.ui.product.vip.VipActivity
import com.xbq.xbqsdk.net.UserCache
import com.xbq.xbqsdk.util.activityresult.StartActivityForResultHelper

/**
 * Author: liaohaiping
 * Time: 2022-03-09
 * Description:
 */

fun FragmentActivity.ensureFeature(feature: String, userCache: UserCache, callback: () -> Unit) =
    ensureLogin(userCache) {
        if (userCache.isFreeOrCanUse(feature)) {
            callback.invoke()
        } else {
            val uidata = VipActivity.VipUIData().apply {
                this.feature = feature
                this.title = "购买会员"
                this.vipDesc1 = "哈哈哈"
                this.vipDesc2 = "hehhe"
                this.vipIntro1 = "我的"
                this.vipIntroImg1 = R.mipmap.icon
            }
            val intent = VipActivity.newIntent(this, uidata)
            StartActivityForResultHelper.startActivityForResult(this, intent) { resultCode, data ->
                if (resultCode == Activity.RESULT_OK) {
                    callback.invoke()
                } else {
                    ToastUtils.showShort("购买会员失败!")
                }
            }
        }
    }


fun FragmentActivity.ensureVip(userCache: UserCache,callback: () -> Unit) = ensureFeature(
    FeatureEnum.WORD_EDITOR.name,userCache,callback)