package com.xiaobaqi.xbqdemo

import com.xbq.xbqsdk.net.common.vo.FeatureDescriber

enum class FeatureEnum(var desc: String) : FeatureDescriber {
    WORD_EDITOR("word文档会员");

    fun setDesc(desc: String): FeatureEnum {
        this.desc = desc
        return this
    }

    override fun getDescription(feature: String?): String {
        return values().firstOrNull { it.name == feature }?.name ?: feature ?: ""
    }
}