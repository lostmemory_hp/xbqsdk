package com.xiaobaqi.xbqdemo

import com.xbq.xbqsdk.XbqApplication
import com.xbq.xbqsdk.XbqSdk
import com.xbq.xbqsdk.core.ui.account.LoginActivity
import dagger.hilt.android.HiltAndroidApp

/**
 * Author: liaohaiping
 * Time: 2022-03-09
 * Description:
 */
@HiltAndroidApp
class MyApp: XbqApplication() {
    override fun onCreate() {
        super.onCreate()
        XbqSdk.setApplication("WORD_EDITOR")
        XbqSdk.setBaseUrl("https://api.dzxiaoshipin.com")
        XbqSdk.setApiPrefix("xbq/api/")
        XbqSdk.setPrivacyUrl("")
        XbqSdk.setUserAgreementUrl("")
        XbqSdk.setLoginActivity(LoginActivity::class.java)
        XbqSdk.setFeatureDescriber(FeatureEnum.WORD_EDITOR)


    }
}